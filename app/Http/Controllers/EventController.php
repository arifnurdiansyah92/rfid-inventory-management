<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use Validator;
use Carbon\Carbon;
class EventController extends Controller
{
    function index(){
        $data = Event::orderBy('created_at','desc')->get();
        return view('event.index', ['data' => $data]);
    }
    function create(){
        return view('event.create');
    }
    function store(Request $req){
        $validate = Validator::make($req->all(), [
            'judul' => 'required',
            'tempat' => 'required',
            'konten' => 'required',
            'tgl_event' => 'required',
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
            'status' => 'required|integer|between:0,1',
            'thumbnail' => 'image'
        ]);
        if($validate->fails()){
            return redirect()->back()->withErrors($validate->errors())->withInput();
        }

        $tgl_event = explode(" - ",$req->tgl_event);
        $event = new Event;
        $event->judul = $req->judul;
        $event->konten = $req->konten;
        $event->tempat = $req->tempat;
        $event->tgl_mulai = Carbon::createFromFormat('d/m/Y',$tgl_event[0]);
        $event->tgl_selesai = Carbon::createFromFormat('d/m/Y',$tgl_event[1]);
        $event->jam_mulai = $req->jam_mulai;
        $event->jam_selesai = $req->jam_selesai;
        $event->status = $req->status;
        if($req->thumbnail){
            $filename = date('ymdHis').".".$req->thumbnail->getClientOriginalExtension();
            $req->file('thumbnail')->move('uploads',$filename);
            $event->thumbnail = $filename;
        }
        if($event->save()){
            return redirect()->route('event.index')->with('success','Berhasil menambah data event!');
        }
    }
    function edit($id){
        $data = Event::findOrFail($id);
        return view('event.edit',['data' => $data]);
    }
    function update($id,Request $req){
        $validate = Validator::make($req->all(), [
            'judul' => 'required',
            'tempat' => 'required',
            'konten' => 'required',
            'tgl_event' => 'required',
            'jam_mulai' => 'required',
            'jam_selesai' => 'required',
            'status' => 'required|integer|between:0,1',
            'thumbnail' => 'image'
        ]);
        if($validate->fails()){
            return redirect()->back()->withErrors($validate->errors())->withInput();
        }

        $tgl_event = explode(" - ",$req->tgl_event);
        $event = Event::findOrFail($id);
        $event->judul = $req->judul;
        $event->konten = $req->konten;
        $event->tempat = $req->tempat;
        $event->jam_mulai = $req->jam_mulai;
        $event->jam_selesai = $req->jam_selesai;
        $event->tgl_mulai = Carbon::createFromFormat('d/m/Y',$tgl_event[0]);
        $event->tgl_selesai = Carbon::createFromFormat('d/m/Y',$tgl_event[1]);
        $event->status = $req->status;
        if($req->thumbnail){
            $filename = date('ymdHis').".".$req->thumbnail->getClientOriginalExtension();
            $req->file('thumbnail')->move('uploads',$filename);
            $event->thumbnail = $filename;
        }
        if($event->save()){
            return redirect()->route('event.index')->with('success','Berhasil menambah data event!');
        }
    }
    function delete($id){
        $data = Event::findOrFail($id);
        $data->delete();
        return redirect()->back()->with('success','Berhasil menghapus data!');
    }
    function event(){
        $upcoming = Event::whereDate('tgl_selesai','>=',Carbon::now())->where('status',1)->orderBy('created_at','desc')->get();
        $past = Event::whereDate('tgl_selesai', '<', Carbon::now())->where('status',1)->orderBy('created_at','desc')->get();
        $allevent = Event::where('status',1)->orderBy('created_at','desc')->get();
        $kalender = collect();
        foreach($allevent as $event){
            $item = [];
            $item['title'] = $event->judul;
            $item['allDay'] = true;
            $item['url'] = route('main.event.detail',$event->id);
            $item['start'] = Carbon::createFromDate($event->tgl_mulai)->toDateTimeLocalString();
            $item['end'] = Carbon::createFromDate($event->tgl_selesai)->addDays(1)->toDateTimeLocalString();
            $kalender->push($item);
        }
        return view('mainpage.event',['kalender' => $kalender, 'upcoming' => $upcoming, 'past' => $past]);
    }
    function detail($id){
        $data = Event::findOrFail($id);
        return view('mainpage.event_detail',['data' => $data]);
    }
}
