<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Berita;
use Validator;

class BeritaController extends Controller
{
    function index(){
        $data = Berita::orderBy('created_at','desc')->get();
        return view('berita.index', ['data' => $data]);
    }
    function create(){
        return view('berita.create');
    }
    function store(Request $req){
        $validate = Validator::make($req->all(), [
            'judul' => 'required',
            'konten' => 'required',
            'status' => 'required|integer|between:0,1',
            'thumbnail' => 'image'
        ]);
        if($validate->fails()){
            return redirect()->back()->withErrors($validate->errors())->withInput();
        }
        $berita = new Berita;
        $berita->judul = $req->judul;
        $berita->konten = $req->konten;
        $berita->status = $req->status;
        if($req->thumbnail){
            $filename = date('ymdHis').".".$req->thumbnail->getClientOriginalExtension();
            $req->file('thumbnail')->move('uploads',$filename);
            $berita->thumbnail = $filename;
        }
        if($berita->save()){
            return redirect()->route('berita.index')->with('success','Berhasil menambah data berita!');
        }
    }
    function edit($id){
        $data = Berita::findOrFail($id);
        return view('berita.edit',['data' => $data]);
    }
    function update($id, Request $req){
        $validate = Validator::make($req->all(), [
            'judul' => 'required',
            'konten' => 'required',
            'status' => 'required|integer|between:0,1',
        ]);
        if($validate->fails()){
            return redirect()->back()->withErrors($validate->errors())->withInput();
        }
        $berita = Berita::findOrFail($id);
        $berita->judul = $req->judul;
        $berita->konten = $req->konten;
        $berita->status = $req->status;
        if($req->thumbnail){
            $filename = date('ymdHis').".".$req->thumbnail->getClientOriginalExtension();
            $req->file('thumbnail')->move('uploads',$filename);
            $berita->thumbnail = $filename;
        }
        if($berita->save()){
            return redirect()->route('berita.index')->with('success','Berhasil merubah data berita!');
        }
    }
    function delete($id){
        $data = Berita::findOrFail($id);
        $data->delete();
        return redirect()->back()->with('success','Berhasil menghapus data!');
    }
    function berita(){
        $data = Berita::where('status',1)->orderBy('created_at','desc')->get();
        foreach($data as $berita){
            $konten = explode('<div class="page-break"></div>',$berita->konten);
            $berita->konten = $konten[0];
        }
        return view('mainpage.berita',['data' => $data]);
    }
    function detail($id){
        $data = Berita::findOrFail($id);
        $berita = Berita::where('status',1)->where('id','<>',$id)->orderBy('created_at','desc')->get();
        return view('mainpage.berita_detail',['data' => $data, 'berita' => $berita]);
    }
}
