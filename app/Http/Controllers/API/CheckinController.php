<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CheckinController extends Controller
{
    function check_sbg($sbg){
        if($sbg == "5555519016666662"){
            return response()->json(['msg' => 'Sukses', 'status' => 1],200);

        }else if($sbg == "5555519016666663"){
            return response()->json(['msg' => 'Sukses', 'status' => 2],200);
        }else{
            return response()->json(['msg' => 'Data Tidak Ditemukan', 'status' => 0],404);
        }
    }
}
