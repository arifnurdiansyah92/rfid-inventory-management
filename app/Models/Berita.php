<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Berita extends Model
{
    public function getThumbnailImgAttribute(){
        return Storage::disk('public')->url($this->thumbnail);
    }
}
