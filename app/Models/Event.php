<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Storage;

class Event extends Model
{
    public function getTglEventAttribute(){
        return Carbon::parse($this->tgl_mulai)->format('d/m/Y') . " - " . Carbon::parse($this->tgl_selesai)->format('d/m/Y');
    }
    public function getThumbnailImgAttribute(){
        return Storage::disk('public')->url($this->thumbnail);
    }
}
