(function ($) {
    $.fn.asycWidget = function() {
        return this.each(function () {
            doit($(this));
        })
    }

    function setloading(el) {
        if(el.find('img').length == 0){
            el.html('<div align="center"><img src="https://cdn.detik.net.id/assets/images/load.gif"/></div>');
        }
    }

    function doit(el) {
        var base_url    = ( typeof baseurl !== 'undefined' ) ? baseurl : base_url;
        var urlParams   = new URLSearchParams(window.location.search);
        var url         = base_url+'/ajax/'+el.attr('d-widget')+(urlParams.has('flush')?'?flush':'');
        var params      = (el.attr('d-params'))?el.attr('d-params'):{};

        $.ajax({
            url: url,
            type: "POST",
            data: {param : params},
            beforeSend: function(){
                setloading(el);
            },
            success:function(result){
                el.html(result.html);
                try {
                    $(".lqd").imgLiquid();
                } catch (e) {}

                if (el.find('img').length>0) {
                    el.find('img').each(function() {
                        if( this.complete ){
                            setTimeout(function(){
                                $(document.body).trigger("sticky_kit:recalc");
                            },3000);
                        }
                    });
                }else{
                    $(document.body).trigger("sticky_kit:recalc");
                }
            }
        })
    }
    $('[d-widget]').asycWidget();

})(jQuery);
