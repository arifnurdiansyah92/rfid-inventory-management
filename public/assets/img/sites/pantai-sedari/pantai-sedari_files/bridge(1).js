
window.DtkXComponent = xcomponent.create({

    // The html tag used to render my component

    tag: 'thecomment2',
    dimensions: {
        width: '100%',
        height: '100%'
    },
    autoResize: { width: true,height:true},
    // The url that will be loaded in the iframe or popup, when someone includes my component on their page
    defaultLogLevel: 'error',
    scrolling: false,
    // Devel
    url: 'https://newcomment.detik.com/static/index.htm?v=1',
    // Production
    // url: 'https://newcomment.detik.com/frontend/static/

    props: {

        url_dtk: {
            type: 'string',
            required: false
        },
        identifier: {
            type: 'number',
            required: false
        },
        group: {
            type: 'number',
            required: false
        },
        date: {
            type: 'string',
            required: false
        },
        title: {
            type: 'string',
            required: false
        },
        appId: {
            type: 'number',
            required: false
        },
        url_share: {
            type: 'string',
            required: false
        },
        prefix: {
            type: 'string',
            required: false
        },
        prokontra: {
            type: 'number',
            required: false
        },
        showhide: {
            type: 'number',
            required: false
        },
        label_1: {
            type: 'string',
            required: false
        },
        label_2: {
            type: 'string',
            required: false
        },
        kanalAds: {
            type: 'string',
            required: false
        },
        envAds: {
            type: 'string',
            required: false
        },
        onLogin: {
            type: 'function'
        },
        onResize: {
            type: 'function'
        },
        onScroll: {
            type: 'function'
        },
        onAlert: {
            type: 'function'
        },
        data_oa: {
            type: 'function',
            required: false
        }
    }


});
$(function() {
    // console.log( "ready!" );
});
function onLogin (){
    if ($('.to_login').html() != 'undefined' && $('.to_login').html() != null) {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            $('.to_login')[0].click();
        }
        else{
            $('.to_login').trigger('click');
        }
    }
    else{
        alert("Maaf terjadi kesalahan, harap logout dan login kembali");
    }
}
function onResize (height){
    $('#thecomment2').attr({style: "height: "+height+"px"}).css({height:height+'px'});
    $('.xcomponent-outlet').attr({style: "height: "+height+"px"}).css({height:height+'px'});
}
function onScroll (toComment) {
    var toBox = $('#thecomment2').offset().top
    var scrollHeight = toBox+toComment
    $('html, body').animate({ scrollTop: scrollHeight }, 800);
}

function onAlert (msg) {
    alert(msg)
}

function data_oa(msg) {
    if(typeof OA_output !== 'undefined'){
        return OA_output['nativekomentar'+msg+''];
    }else{
        return '';
    }
}