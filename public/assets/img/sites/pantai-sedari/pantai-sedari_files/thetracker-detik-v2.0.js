var _dSUBTYPE_WEB="web";var _dSUBTYPE_MOBILE="mobile";var _dacct="acc-default";var _dacct_sub="-";var _dwv="0.1";var _dgifpath="//analytic.detik.com/detikanalytic/__dtm.gif";var _dhash="on";var _ddn="maindomain";var _dtimeout="1800";var _dtcp="/";var _dtitle=1;var _dsendtimeout=4000;var _ddomainhash,_dvisitorid,_dinittime,_ddoc=document,_dloc=_ddoc.location,_dcookie=_ddoc.cookie;var _dcreateddate,_darticleid,_dkanalid,_ddetikid,_dcustomparams;function detikTracker(params){if(params.acctype===undefined||params.subacctype===undefined||params.createddate===undefined||params.articleid===undefined||params.kanalid===undefined){console.log("[DetikAnalytic] These fields are required: acctype, subacctype, createddate, articleid, kanalid!");return}else{_dacct=params.acctype;_dacct_sub=params.subacctype;_dcreateddate=params.createddate;_darticleid=params.articleid;_dkanalid=params.kanalid;_dcustomparams=_dGetCustomParams(params)}
collectData();processData();setTimeout(sendData(),_dsendtimeout)}
function collectData(){_ddomainhash=_dDomainHash();_dvisitorid=Math.round(Math.random()*2147483647);_dinittime=Math.round((new Date()).getTime()/1000);if((_ddetikid=_dGC(_dcookie,"D_ISS",";"))!="-"){_ddetikid=_ddetikid.split("=")[1]}else{_ddetikid=""}}
function processData(){var _dtm_domain,_dtma,_dtmb,_dtmc,_dtma_exp,_dtmb_exp="";_dtma_exp=" expires="+(new Date((new Date()).getTime()+63072000000)).toGMTString()+";";if(_dtimeout&&_dtimeout!="")_dtmb_exp=" expires="+(new Date((new Date()).getTime()+(_dtimeout*1000))).toGMTString()+";";if(_ddn&&_ddn!="")_dtm_domain=" domain= ."+_dDomainPath()+";";_dtma=_dcookie.indexOf("__dtma="+_ddomainhash+".");_dtmb=_dcookie.indexOf("__dtmb="+_ddomainhash);_dtmc=_dcookie.indexOf("__dtmc="+_ddomainhash);if(_dtma>=0&&_dtmb>=0&&_dtmc>=0){_dtmb=_dFixB(_ddoc.cookie,";",_dinittime);_ddoc.cookie="__dtmb="+_dtmb+"; path="+_dtcp+";"+_dtmb_exp+_dtm_domain}else{if(_dtma>=0){_dtma=_dFixA(_ddoc.cookie,";",_dinittime)}else{_dtma=_ddomainhash+"."+_dvisitorid+"."+_dinittime+"."+_dinittime+"."+_dinittime+".1"}
if(_dtmb>=0){_dtmb=_dFixB(_ddoc.cookie,";",_dinittime)}else{_dtmb=_ddomainhash+".1.10."+_dinittime}
_ddoc.cookie="__dtma="+_dtma+"; path="+_dtcp+";"+_dtma_exp+_dtm_domain;_ddoc.cookie="__dtmb="+_dtmb+"; path="+_dtcp+";"+_dtmb_exp+_dtm_domain;_ddoc.cookie="__dtmc="+_ddomainhash+"; path="+_dtcp+";"+_dtm_domain}}
function sendData(){var p,s,t="",dm="",pg=_dloc.pathname+_dloc.search;s="";s+="&dtmn="+_dvisitorid;if(_dtitle&&_ddoc.title&&_ddoc.title!="")s+="&dtmdt="+_dES(_ddoc.title);if(_dloc.hostname&&_dloc.hostname!="")s+="&dtmhn="+_dES(_dloc.hostname);s+="&dtmp="+pg;if((t=_dGC(_ddoc.cookie,"__dtma="+_ddomainhash+".",";"))!="-")s+="&dtma="+t;if((t=_dGC(_ddoc.cookie,"__dtmb="+_ddomainhash+".",";"))!="-")s+="&dtmb="+t;s+="&dtmr="+_ddoc.referrer;s+=_dConstructMP();var i2=new Image(1,1);i2.src=_dgifpath+"?"+"dtmwv="+_dwv+s+"&dtmac="+_dacct+"&dtmacsub="+_dacct_sub;i2.onload=function(){return};return}
function _dGetCustomParams(params){var s="";for(var i in params){if(i.indexOf("custom-")!=-1){s+="&"+i.replace("-","_")+"="+params[i]}};return s}
function _dConstructMP(){var s="";if(_dcreateddate!=""){s+="&createddate="+_dcreateddate}
if(_darticleid!=""){s+="&articleid="+_darticleid}
if(_dkanalid!=""){s+="&kanalid="+_dkanalid}
if(_ddetikid!=""){s+="&detik_id="+_ddetikid}
if(_dcustomparams!=""){s+=_dcustomparams}
return s}
function _dFixA(c,s,t){if(!c||c==""||!s||s==""||!t||t=="")return "-";var a=_dGC(c,"__dtma="+_ddomainhash+".",s);var lt=0,i=0;if((i=a.lastIndexOf("."))>9){_uns=a.substring(i+1,a.length);_uns=(_uns*1)+1;a=a.substring(0,i);if((i=a.lastIndexOf("."))>7){lt=a.substring(i+1,a.length);a=a.substring(0,i)}
if((i=a.lastIndexOf("."))>5){a=a.substring(0,i)}
a+="."+lt+"."+t+"."+_uns}
return a}
function _dFixB(c,s,t){if(!c||c==""||!s||s==""||!t||t=="")return "-";var b=_dGC(c,"__dtmb="+_ddomainhash,s);var lt=0,i=0,pv_count,x=0,y=0;var dom_hash="";x=b.indexOf(".");y=b.indexOf(".",x+1);dom_hash=b.substring(0,x);pv_count=b.substring(x+1,y);pv_count=(pv_count*1)+1;b=dom_hash+"."+pv_count+".10."+t;return b}
function _dDomainHash(){var domain=_dDomainPath();if(domain=="")return 1
else return _dHash(domain)}
function _dDomainPath(){if(_ddn=="maindomain"){var TLDs=["com"].join();var parts=(window.location.hostname).split('.'),ln=parts.length,i=ln,minLength=parts[parts.length-1].length,part
while(part=parts[--i]){if(TLDs.indexOf(part)<0||part.length<minLength||i<ln-2||i===0){return part+'.'+parts[ln-1]}}}else if(_ddn=="auto"){var d=_ddoc.domain;if(d.substring(0,4)=="www."){d=d.substring(4,d.length)}
return d}else{return ""}}
function _dHash(d){if(!d||d=="")return 1;var h=0,g=0;for(var i=d.length-1;i>=0;i--){var c=parseInt(d.charCodeAt(i));h=((h<<6)&0xfffffff)+c+(c<<14);if((g=h&0xfe00000)!=0)h=(h^(g>>21))}
return h}
function _dGC(l,n,s){if(!l||l==""||!n||n==""||!s||s=="")return "-";var i,i2,i3,c="-";i=l.indexOf(n);i3=n.indexOf("=")+1;if(i>-1){i2=l.indexOf(s,i);if(i2<0){i2=l.length}
c=l.substring((i+i3),i2)}
return c}
function _dES(s,u){if(typeof(encodeURIComponent)=='function'){if(u)return encodeURI(s);else return encodeURIComponent(s)}else{return escape(s)}}
function getCookieData(name){var pairs=document.cookie.split("; "),count=pairs.length,parts;while(count--){parts=pairs[count].split("=");if(parts[0]===name)return parts[1]}
return!1}