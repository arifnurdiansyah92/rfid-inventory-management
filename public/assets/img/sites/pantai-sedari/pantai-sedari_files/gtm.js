
// Copyright 2012 Google Inc. All rights reserved.
(function(w,g){w[g]=w[g]||{};w[g].e=function(s){return eval(s);};})(window,'google_tag_manager');(function(){

var data = {
"resource": {
  "version":"514",
  "macros":[{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return $(",["escape",["macro",0],8,16],").parent().parent().hasClass(\"list_berita\")})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return $(",["escape",["macro",0],8,16],").hasClass(\"gtm_beritaterkait_artikel\")||$(",["escape",["macro",0],8,16],").parents(\"a\").hasClass(\"gtm_beritaterkait_artikel\")})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return $(",["escape",["macro",0],8,16],").hasClass(\"gtm_bacajuga_artikel\")||$(",["escape",["macro",0],8,16],").parents(\"a\").hasClass(\"gtm_bacajuga_artikel\")})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var a=document.getElementsByTagName(\"iframe\"),b=a.length;b--;)if(\/tv.detik.com\\\/20detik\\\/embed\/.test(a[b].src))return!0;return!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var a=document.getElementsByTagName(\"iframe\"),b=a.length;b--;)if(\/devbase.detik.com:8090\\\/20detik\\\/embed\/.test(a[b].src))return!0;return!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return $(",["escape",["macro",0],8,16],").parents().hasClass(\"headline\")})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){(new Date).getTime();return(new Date-performance.timing.navigationStart)\/1E3})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){for(var a={},d=document.location.href,b=0;b\u003Cd.length;b++){var c=d.charAt(b);a[c]?a[c]++:a[c]=1}return a[\"\/\"]})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){(function(c,e){var a=function(b){this._options={loopCheckTime:50,loopMaxNumber:5,baitClass:\"pub_300x250 pub_300x250m pub_728x90 text-ad textAd text_ad text_ads text-ads text-ad-links\",baitStyle:\"width: 1px !important; height: 1px !important; position: absolute !important; left: -10000px !important; top: -1000px !important;\"};this._var={bait:null,checking:!1,loop:null,loopNumber:0,event:{detected:[],notDetected:[]}}};a.prototype._options=null;a.prototype._var=null;a.prototype._bait=null;\na.prototype._creatBait=function(){var b=document.createElement(\"div\");b.setAttribute(\"class\",this._options.baitClass);b.setAttribute(\"style\",this._options.baitStyle);this._var.bait=c.document.body.appendChild(b);this._var.bait.offsetParent;this._var.bait.offsetHeight;this._var.bait.offsetLeft;this._var.bait.offsetTop;this._var.bait.offsetWidth;this._var.bait.clientHeight;this._var.bait.clientWidth};a.prototype._destroyBait=function(){c.document.body.removeChild(this._var.bait);this._var.bait=null};\na.prototype.check=function(b){b===e\u0026\u0026(b=!0);if(this._var.checking)return!1;this._var.checking=!0;null===this._var.bait\u0026\u0026this._creatBait();var a=this;this._var.loopNumber=0;!0===b\u0026\u0026(this._var.loop=setInterval(function(){a._checkBait(b)},this._options.loopCheckTime));setTimeout(function(){a._checkBait(b)},1);return!0};a.prototype._checkBait=function(b){var a=!1;null===this._var.bait\u0026\u0026this._creatBait();if(null!==c.document.body.getAttribute(\"abp\")||null===this._var.bait.offsetParent||0==this._var.bait.offsetHeight||\n0==this._var.bait.offsetLeft||0==this._var.bait.offsetTop||0==this._var.bait.offsetWidth||0==this._var.bait.clientHeight||0==this._var.bait.clientWidth)a=!0;if(c.getComputedStyle!==e){var d=c.getComputedStyle(this._var.bait,null);!d||\"none\"!=d.getPropertyValue(\"display\")\u0026\u0026\"hidden\"!=d.getPropertyValue(\"visibility\")||(a=!0)}!0===b\u0026\u0026(this._var.loopNumber++,this._var.loopNumber\u003E=this._options.loopMaxNumber\u0026\u0026this._stopLoop());if(!0===a)this._stopLoop(),this._destroyBait(),this.emitEvent(!0),!0===b\u0026\u0026(this._var.checking=\n!1);else if(null===this._var.loop||!1===b)this._destroyBait(),this.emitEvent(!1),!0===b\u0026\u0026(this._var.checking=!1)};a.prototype._stopLoop=function(b){clearInterval(this._var.loop);this._var.loop=null;this._var.loopNumber=0};a.prototype.emitEvent=function(b){b=this._var.event[!0===b?\"detected\":\"notDetected\"];for(var a in b)if(b.hasOwnProperty(a))b[a]();this.clearEvent();return this};a.prototype.clearEvent=function(){this._var.event.detected=[];this._var.event.notDetected=[]};a.prototype.on=function(a,\nc){this._var.event[a?\"detected\":\"notDetected\"].push(c);return this};a=new a;a.on(!0,function(){return!0});a.on(!1,function(){return!1});a.check()})(window);return addBlocker})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){var a=",["escape",["macro",0],8,16],".pathname.split(\".\");return 1\u003Ca.length?a.pop():\"html\"})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(a){if(13===a.which)return $(\"#autocompletegadget\").val(),!1})();"]
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){if(\"undefined\"===typeof window._gtm_scroll_set){for(var b=[50,75,100],a=document.querySelector(\"article\"),d=document.querySelector(\"div.terkait.div-top.pt45\"),e=document.querySelector(\"div#komentar\"),f=document.querySelector(\"div.terbaru\"),g=a.offsetHeight+d.offsetHeight+e.offsetHeight+f.offsetHeight,c=0;a;)c+=a.offsetTop-a.scrollTop+a.clientTop,a=a.offsetParent;window._gtm_scroll_set={thresholds:b.map(function(a){return parseInt(.01*g*a+c)}).join(),percentages:b}}return window._gtm_scroll_set.thresholds})();"]
    },{
      "function":"__v",
      "vtp_name":"gtm.scrollThreshold",
      "vtp_dataLayerVersion":1
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){if(\"undefined\"!==typeof window._gtm_scroll_set){var b=window._gtm_scroll_set.percentages,a=window._gtm_scroll_set.thresholds.split(\",\").map(function(a){return parseInt(a)});a=a.indexOf(",["escape",["macro",13],8,16],");return b[a]}return ",["escape",["macro",13],8,16],"})();"]
    },{
      "function":"__e"
    },{
      "function":"__c",
      "vtp_value":"www.detik.com, news.detik.com, sport.detik.com, hot.detik.com, inet.detik.com, m.detik.com, blogdetik.com, food.detik.com, travel.detik.com, oto.detik.com, finance.detik.com, health.detik.com, wolipop.detik.com, readerchoice.detik.com, foto.detik.com, forum.detik.com, m.forum.detik.com"
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"ngtmdc"
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"__dtmc"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"kanalid"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"articleid"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"articletype"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"createdate"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"publishdate"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"keyword"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"idfokus"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"namafokus"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"idprogram"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"namaprogram"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"pagesize"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"pagenumber"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"articledewasa"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"articlehoax"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"videopresent"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"contenttype"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"platform"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"author"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"video_id"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"adBlocker"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"hl_nhl_wp"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"hl_nhl_kanal"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"playfrom_article"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"playfrom_url"
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"banner_campaign",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"banner_platform",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"banner_medium",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"banner_kanal",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"banner_term",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"banner_position",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"tag_from",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__c",
      "vtp_value":"UA-891770-59"
    },{
      "function":"__v",
      "vtp_name":"gtm.elementId",
      "vtp_dataLayerVersion":1
    },{
      "function":"__u",
      "vtp_component":"HOST",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"URL",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__c",
      "vtp_value":"UA-891770-59"
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementClasses",
      "vtp_dataLayerVersion":1
    },{
      "function":"__e"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"gaEventCategory"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"gaEventAction"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"gaEventLabel"
    },{
      "function":"__u",
      "vtp_component":"PATH",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"gaEventCategoryVPV"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"gaEventActionVPV"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"gaEventLabelVPV"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"baca_juga_aid"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"bc_domain"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"bc_nav_url"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"bc_title"
    },{
      "function":"__c",
      "vtp_value":"UA-891770-245"
    },{
      "function":"__v",
      "vtp_name":"gtm.triggers",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":""
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"adBlocked"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__aev",
      "vtp_setDefaultValue":false,
      "vtp_varType":"ID"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"eventCategory"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"eventAction"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"eventLabel"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"customSearchInput"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"virtualPageURL"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"virtualPageTitle"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",80]],["map","fieldName","title","value",["macro",81]]],
      "vtp_useHashAutoLink":false,
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":false,
      "vtp_name":"userid"
    },{
      "function":"__k",
      "vtp_decodeCookie":false,
      "vtp_name":"__dtmc"
    },{
      "function":"__c",
      "vtp_value":"www.detik.com, news.detik.com, sport.detik.com, hot.detik.com, inet.detik.com, m.detik.com, blogdetik.com, food.detik.com, travel.detik.com, oto.detik.com, finance.detik.com, health.detik.com, wolipop.detik.com, readerchoice.detik.com, foto.detik.com, forum.detik.com, m.forum.detik.com"
    },{
      "function":"__gas",
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","userId","value",["macro",83]],["map","fieldName","DAtest","value",["macro",84]]],
      "vtp_useHashAutoLink":false,
      "vtp_autoLinkDomains":["macro",85],
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-891770-189",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"sessionactivity"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"sessionmedium"
    },{
      "function":"__gas",
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_decorateFormsAutoLink":false,
      "vtp_autoLinkDomains":["macro",16],
      "vtp_cookieDomain":"auto",
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","userId","value",["macro",17]],["map","fieldName","DAtest","value",["macro",18]]],
      "vtp_metric":["list",["map","index","3","metric",["macro",7]]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","42","dimension",["macro",19]],["map","index","43","dimension",["macro",20]],["map","index","44","dimension",["macro",21]],["map","index","45","dimension",["macro",22]],["map","index","46","dimension",["macro",23]],["map","index","47","dimension",["macro",24]],["map","index","48","dimension",["macro",25]],["map","index","49","dimension",["macro",26]],["map","index","50","dimension",["macro",27]],["map","index","51","dimension",["macro",28]],["map","index","53","dimension",["macro",29]],["map","index","54","dimension",["macro",30]],["map","index","55","dimension",["macro",31]],["map","index","56","dimension",["macro",32]],["map","index","57","dimension",["macro",33]],["map","index","58","dimension",["macro",34]],["map","index","59","dimension",["macro",35]],["map","index","60","dimension",["macro",36]],["map","index","68","dimension",["macro",37]],["map","index","62","dimension",["macro",17]],["map","index","63","dimension",["macro",38]],["map","index","69","dimension",["macro",39]],["map","index","70","dimension",["macro",40]],["map","index","71","dimension",["macro",41]],["map","index","72","dimension",["macro",42]],["map","index","73","dimension",["macro",43]],["map","index","74","dimension",["macro",44]],["map","index","75","dimension",["macro",45]],["map","index","76","dimension",["macro",46]],["map","index","77","dimension",["macro",47]],["map","index","78","dimension",["macro",48]],["map","index","81","dimension",["macro",49]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":"UA-891770-189",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"action"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"panelname"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"pt_from_type"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"pt_from_kanal"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"pt_platform"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"pt_to_url"
    },{
      "function":"__v",
      "vtp_dataLayerVersion":2,
      "vtp_setDefaultValue":true,
      "vtp_defaultValue":"null",
      "vtp_name":"pt_to_page"
    },{
      "function":"__d",
      "vtp_elementSelector":"input#autogadgetcomplete",
      "vtp_selectorType":"CSS"
    },{
      "function":"__c",
      "vtp_value":"GTM-TW3QW5K"
    },{
      "function":"__jsm",
      "vtp_javascript":["template","(function(){return function(a){a.set(\"dimension21\",",["escape",["macro",9],8,16],")}})();"]
    },{
      "function":"__aev",
      "vtp_setDefaultValue":false,
      "vtp_varType":"URL",
      "vtp_component":"PATH",
      "vtp_defaultPages":["list","https:\/\/detik.com\/"]
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_queryKey":"zoneid",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__u",
      "vtp_component":"QUERY",
      "vtp_enableMultiQueryKeys":false,
      "vtp_enableIgnoreEmptyQueryParam":false
    },{
      "function":"__remm",
      "vtp_setDefaultValue":false,
      "vtp_input":["macro",19],
      "vtp_fullMatch":true,
      "vtp_replaceAfterMatch":true,
      "vtp_ignoreCase":true,
      "vtp_map":["list",["map","key","2-605.*","value","detiknews"],["map","key","2-69.*","value","detiksport"],["map","key","2-3.*","value","detikfinance"],["map","key","2-105.*","value","detikinet"],["map","key","2-638.*","value","detikoto"],["map","key","2-207.*","value","detikhot"],["map","key","2-835.*","value","wolipop"],["map","key","2-755 .*","value","detikhealth"],["map","key","2-1024.*","value","detiktravel"],["map","key","2-284.*","value","detikfood"],["map","key","2-1678.*","value","detikX"]]
    },{
      "function":"__f",
      "vtp_component":"URL"
    },{
      "function":"__v",
      "vtp_name":"gtm.elementTarget",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.element",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementId",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementTarget",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.elementUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__aev",
      "vtp_varType":"TEXT"
    },{
      "function":"__v",
      "vtp_name":"gtm.errorMessage",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.errorUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.errorLineNumber",
      "vtp_dataLayerVersion":1
    },{
      "function":"__dbg"
    },{
      "function":"__v",
      "vtp_name":"gtm.videoUrl",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoTitle",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.videoDuration",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.scrollUnits",
      "vtp_dataLayerVersion":1
    },{
      "function":"__v",
      "vtp_name":"gtm.scrollDirection",
      "vtp_dataLayerVersion":1
    }],
  "tags":[{
      "function":"__ua",
      "setup_tags":["list",["tag",658,0]],
      "teardown_tags":["list",["tag",659,0]],
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_autoLinkDomains":["macro",16],
      "vtp_decorateFormsAutoLink":false,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","cookieDomain","value","auto"],["map","fieldName","userId","value",["macro",17]],["map","fieldName","DAtest","value",["macro",18]]],
      "vtp_metric":["list",["map","index","2","metric",["macro",7]]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",19]],["map","index","2","dimension",["macro",20]],["map","index","3","dimension",["macro",21]],["map","index","4","dimension",["macro",22]],["map","index","5","dimension",["macro",23]],["map","index","6","dimension",["macro",24]],["map","index","7","dimension",["macro",25]],["map","index","8","dimension",["macro",26]],["map","index","9","dimension",["macro",27]],["map","index","10","dimension",["macro",28]],["map","index","11","dimension",["macro",29]],["map","index","12","dimension",["macro",30]],["map","index","13","dimension",["macro",31]],["map","index","14","dimension",["macro",32]],["map","index","15","dimension",["macro",33]],["map","index","16","dimension",["macro",34]],["map","index","17","dimension",["macro",35]],["map","index","18","dimension",["macro",36]],["map","index","26","dimension",["macro",37]],["map","index","20","dimension",["macro",17]],["map","index","21","dimension",["macro",38]],["map","index","27","dimension",["macro",39]],["map","index","28","dimension",["macro",40]],["map","index","29","dimension",["macro",41]],["map","index","30","dimension",["macro",42]],["map","index","31","dimension",["macro",43]],["map","index","32","dimension",["macro",44]],["map","index","33","dimension",["macro",45]],["map","index","34","dimension",["macro",46]],["map","index","35","dimension",["macro",47]],["map","index","36","dimension",["macro",48]],["map","index","39","dimension",["macro",49]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":2
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":6
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":7
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiknews",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":8
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfinance",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":9
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link motogp",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":10
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikhot",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":11
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikinet",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":12
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiksport",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":13
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link sepakbola",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":14
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":15
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfood",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":16
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikhealth",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":17
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link wolipop",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":18
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiktravel",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":19
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiktv",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":20
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":21
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link pasangmata",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":22
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikforum",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":23
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link blogdetik",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":24
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link morning friends",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":25
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikrewards",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":26
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link solusi ukm",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":27
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link dnewgeneration",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":28
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button search",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":29
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link daftar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":30
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link masuk",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":31
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link profile saya",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":32
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link dashboard",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":33
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link keluar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":34
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":35
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link home",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":36
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link basket",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":37
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link raket",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":38
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link formula 1",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":39
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link sepakbola",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":40
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link sport lain",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":41
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link foto sport",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":42
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link video sport",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":43
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link about the game",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":44
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link indeks",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":45
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link most popular",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":46
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link siaran langsung",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":47
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link jadwal pertandingan",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":48
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link klasemen",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":49
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link statistik center",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":50
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link live match center",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":51
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik slider",
      "vtp_eventLabel":"berita utama",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":52
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"berita utama",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":57
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"headline",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":58
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel terkait",
      "vtp_eventLabel":"headline",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":59
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik notifikasi",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":60
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":61
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button more news",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":63
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most popular",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":64
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detiksport",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":66
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":67
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link redaksi",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":68
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link pedoman media siber",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":69
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link karir",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":70
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link kotak pos",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":71
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link info iklan",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":72
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link privacy policy",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":73
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link disclaimer",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":74
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow facebook",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":75
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button selengkapnya",
      "vtp_eventLabel":"most popular",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":77
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most commented",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":78
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"infografis",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":84
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow instagram",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":85
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow google plus",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":86
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow twitter",
      "vtp_eventLabel":"footer",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":87
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"about the game",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":89
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"tematik",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":90
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"fokus",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":92
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"gsport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":93
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"detik forum",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":94
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"blogdetik",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":95
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"siaran langsung",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":96
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"fokus",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":97
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":98
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most popular sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":99
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most popular sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":100
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button selengkapnya",
      "vtp_eventLabel":"most popular sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":101
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most commented sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":102
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most commented sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":103
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"fokus sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":104
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"fokus sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":105
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"fokus sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":106
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":107
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"berita sepakbola",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":108
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"klasemen",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":109
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"jadwal pertandingan",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":110
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"foto sport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":111
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"video sport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":112
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"about the game",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":113
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik tab formula 1",
      "vtp_eventLabel":"klasemen",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":114
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik tab motogp",
      "vtp_eventLabel":"klasemen",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":115
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button selengkapnya",
      "vtp_eventLabel":"klasemen",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":116
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik tab formula 1",
      "vtp_eventLabel":"jadwal pertandingan",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":117
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik tab motogp",
      "vtp_eventLabel":"jadwal pertandingan",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":118
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button selengkapnya",
      "vtp_eventLabel":"jadwal pertandingan",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":119
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"foto sport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":120
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik slider",
      "vtp_eventLabel":"foto sport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":121
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"video sport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":122
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik slider",
      "vtp_eventLabel":"video sport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":123
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"about the game",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":124
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"gsport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":125
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"gsport",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":126
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"detik forum",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":127
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"detik forum",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":128
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"blogdetik",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":129
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"blogdetik",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":130
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"berita sepakbola",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":131
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"berita sepakbola",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":132
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link pesta bola eropa 2016",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":134
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":135
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link breadcrumb",
      "vtp_eventLabel":"breadcrumb",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":136
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":137
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":138
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link home",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":139
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow facebook",
      "vtp_eventLabel":"breadcrumb",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":140
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow twitter",
      "vtp_eventLabel":"breadcrumb",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":141
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow google plus",
      "vtp_eventLabel":"breadcrumb",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":142
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik follow instagram",
      "vtp_eventLabel":"breadcrumb",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":143
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":144
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":145
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiksport",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":146
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":147
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik share facebook",
      "vtp_eventLabel":"share atas",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":148
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik share twitter",
      "vtp_eventLabel":"share atas",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":149
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik share google plus",
      "vtp_eventLabel":"share atas",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":150
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button komentar",
      "vtp_eventLabel":"share atas",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":151
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik share facebook",
      "vtp_eventLabel":"share floating",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":152
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik share twitter",
      "vtp_eventLabel":"share floating",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":153
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik share google plus",
      "vtp_eventLabel":"share floating",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":154
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button komentar",
      "vtp_eventLabel":"share floating",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":155
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"baca juga",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":157
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik balas",
      "vtp_eventLabel":"komentar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":158
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik share facebook",
      "vtp_eventLabel":"komentar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":159
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":160
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":161
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"berita terbaru",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":163
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"berita terbaru",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":164
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"detik forum",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":165
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button search",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":166
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link blogdetik",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":167
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link daftar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":168
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link dashboard",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":169
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfinance",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":170
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button search",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":171
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link blogdetik",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":172
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link daftar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":173
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link dashboard",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":174
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiknews",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":175
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfinance",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":176
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfood",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":177
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikforum",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":178
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":179
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikhealth",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":180
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikhot",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":181
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikinet",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":182
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":183
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikrewards",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":184
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiktravel",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":185
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiktv",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":186
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link dnewgeneration",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":187
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link keluar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":188
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link masuk",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":189
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link morning friends",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":190
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link pasangmata",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":191
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link profile saya",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":192
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link sepakbola",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":193
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link solusi ukm",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":194
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link wolipop",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":195
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikhot desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":196
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiknews desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":197
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikinet desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":198
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikfinance desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":199
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikoto desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":200
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiktravel desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":201
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikfood desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":202
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikhealth desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":203
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp wolipop desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":204
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom mobile",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":205
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom mobile",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":206
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikfinance desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":207
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiknews mobile",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"news feed",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":208
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detikcom mobile",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiknews",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":209
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"detik forum",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":210
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik thumbnail",
      "vtp_eventLabel":"detik forum",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":211
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"blogdetik",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":212
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button indeks panah",
      "vtp_eventLabel":"blogdetik",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":213
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most popular sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":214
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik button selengkapnya",
      "vtp_eventLabel":"most popular sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":215
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most commented sticky",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":216
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiknews",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":217
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikhot",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":219
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikinet",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":220
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiksport",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":221
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link sepakbola",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":222
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":223
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfood",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":224
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikhealth",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":225
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link wolipop",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":226
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiktravel",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":227
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detiktv",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":228
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikfoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":229
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link pasangmata",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":230
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikforum",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":231
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link morning friends",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":232
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link detikrewards",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":233
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link solusi ukm",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":234
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link dnewgeneration",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":235
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link masuk",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":236
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link profile saya",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":237
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link keluar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":238
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detiksport",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":239
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":240
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link basket",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":241
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link raket",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":242
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link formula 1",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":243
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link motogp",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":244
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link sepakbola",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":245
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link sport lain",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":246
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link foto sport",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":247
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link video sport",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":248
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link about the game",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":249
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link indeks",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":250
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link most popular",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":251
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link siaran langsung",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":252
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link jadwal pertandingan",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":253
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link klasemen",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":254
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"detail artikel reguler detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik link statistik center",
      "vtp_eventLabel":"navigasi bar",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":255
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",64],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",65],
      "vtp_eventLabel":["macro",66],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":256
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"wp detiksport desktop",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":"klik mostpop",
      "vtp_eventLabel":"most popular",
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",54],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":260
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":262
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"artikel",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"berita terkait",
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_eventValue":"1",
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","cookieDomain","value","auto"]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",19]],["map","index","2","dimension",["macro",20]],["map","index","17","dimension",["macro",35]],["map","index","22","dimension",["macro",67]],["map","index","23","dimension",["macro",68]],["map","index","24","dimension",["macro",69]],["map","index","25","dimension",["macro",70]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",71],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":289
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":"artikel",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"baca juga",
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_eventValue":"1",
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","cookieDomain","value","auto"]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",19]],["map","index","2","dimension",["macro",20]],["map","index","3","dimension",["macro",35]],["map","index","4","dimension",["macro",67]],["map","index","5","dimension",["macro",68]],["map","index","6","dimension",["macro",69]],["map","index","7","dimension",["macro",70]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",71],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":290
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":293
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_enableLinkId":false,
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":296
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":297
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"home",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik logo detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":300
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"home",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikcom",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":301
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detiknews",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":302
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikfinance",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":303
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikhot",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":304
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikinet",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":305
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detiksport",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":306
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link sepakbola",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":307
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":308
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikfood",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":309
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikhealth",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":310
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link wolipop",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":311
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detiktravel",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":312
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detiktv",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":313
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikfoto",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":314
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link pasangmata",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":315
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikforum",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":316
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link blogdetik",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":317
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link morning friends",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":318
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link detikrewards",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":319
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik link solusiukm",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":320
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"detiksearch",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik search bar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":321
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"detiksearch",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button search",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":322
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"login detikconnect",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button daftar",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":323
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"login detikconnect",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button masuk",
      "vtp_eventLabel":"frame bar detikconnect",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":324
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"home",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik logo detikoto",
      "vtp_eventLabel":"header",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":325
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"kanal",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik icon detikcom",
      "vtp_eventLabel":"navigasi bar",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":326
    },{
      "function":"__ua",
      "setup_tags":["list",["tag",659,0]],
      "once_per_event":true,
      "vtp_useDebugVersion":false,
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_autoLinkDomains":["macro",16],
      "vtp_decorateFormsAutoLink":false,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":true,
      "vtp_setTrackerName":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","cookieDomain","value","auto"],["map","fieldName","userId","value",["macro",17]],["map","fieldName","DAtest","value",["macro",18]]],
      "vtp_metric":["list",["map","index","2","metric",["macro",7]]],
      "vtp_enableLinkId":false,
      "vtp_dimension":["list",["map","index","1","dimension",["macro",19]],["map","index","2","dimension",["macro",20]],["map","index","3","dimension",["macro",21]],["map","index","4","dimension",["macro",22]],["map","index","5","dimension",["macro",23]],["map","index","6","dimension",["macro",24]],["map","index","7","dimension",["macro",25]],["map","index","8","dimension",["macro",26]],["map","index","9","dimension",["macro",27]],["map","index","10","dimension",["macro",28]],["map","index","11","dimension",["macro",29]],["map","index","12","dimension",["macro",30]],["map","index","13","dimension",["macro",31]],["map","index","14","dimension",["macro",32]],["map","index","15","dimension",["macro",33]],["map","index","16","dimension",["macro",34]],["map","index","17","dimension",["macro",35]],["map","index","18","dimension",["macro",36]],["map","index","19","dimension",["macro",7]],["map","index","26","dimension",["macro",37]],["map","index","30","dimension",["macro",42]],["map","index","20","dimension",["macro",17]],["map","index","21","dimension",["macro",73]]],
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",71],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":333
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"scroll tracking",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",74],
      "vtp_eventAction":["macro",61],
      "vtp_trackingId":"UA-891770-245",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":335
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"Document",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",74],
      "vtp_eventAction":["macro",10],
      "vtp_eventLabel":["macro",57],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":336
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":337
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":338
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_fieldsToSet":["list",["map","fieldName",["macro",75],"value","true"]],
      "vtp_eventCategory":["macro",76],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",77],
      "vtp_eventLabel":["macro",78],
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":340
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_fieldsToSet":["list",["map","fieldName","cookieDomain","value","auto"],["map","fieldName","page","value",["template","\/searchall\/?query=",["macro",79]]]],
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":342
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":["macro",76],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",77],
      "vtp_eventLabel":["macro",78],
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":343
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button homepage",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button detikcom",
      "vtp_eventLabel":"button selengkapnya",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":344
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button homepage",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button 20detik",
      "vtp_eventLabel":"button selengkapnya",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":345
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button homepage",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button pasangmata",
      "vtp_eventLabel":"button selengkapnya",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":346
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button detikcom",
      "vtp_eventLabel":"download play store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":347
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button detikcom",
      "vtp_eventLabel":"download app store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":348
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button detikcom",
      "vtp_eventLabel":"download mobile",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":349
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button 20detik",
      "vtp_eventLabel":"download play store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":350
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button 20detik",
      "vtp_eventLabel":"download app store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":351
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button 20detik",
      "vtp_eventLabel":"download mobile",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":352
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button pasangmata",
      "vtp_eventLabel":"download play store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":353
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button pasangmata",
      "vtp_eventLabel":"download mobile",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":354
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button pasangmata",
      "vtp_eventLabel":"download app store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":355
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button cnn indonesia",
      "vtp_eventLabel":"download play store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":356
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button cnn indonesia",
      "vtp_eventLabel":"download app store",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":357
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"button detail",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button cnn indonesia",
      "vtp_eventLabel":"download mobile",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":358
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"history point trivia quiz",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button piala",
      "vtp_eventLabel":"button piala",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":360
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"trivia quiz",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button trivia",
      "vtp_eventLabel":"button trivia",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":361
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"trivia quiz",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button mulai",
      "vtp_eventLabel":"button mulai",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":362
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"trivia quiz",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button menu game",
      "vtp_eventLabel":"button menu game",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":363
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"dribble ball",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button dribble",
      "vtp_eventLabel":"button dribble",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":364
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"penalty",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button penalty",
      "vtp_eventLabel":"button penalty",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":365
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"petunjuk permainan",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button petunjuk permainan",
      "vtp_eventLabel":"button petunjuk permainan",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":366
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventValue":"1",
      "vtp_eventCategory":"petunjuk permainan",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik button menu game",
      "vtp_eventLabel":"button menu game",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":369
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_dimension":["list",["map","index","31","dimension",["macro",43]],["map","index","32","dimension",["macro",44]],["map","index","33","dimension",["macro",45]],["map","index","34","dimension",["macro",46]],["map","index","35","dimension",["macro",47]],["map","index","36","dimension",["macro",48]]],
      "vtp_trackingId":"UA-891770-223",
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":374
    },{
      "function":"__paused",
      "vtp_originalTagType":"html",
      "tag_id":379
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":381
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":["macro",60],
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":["macro",61],
      "vtp_eventLabel":["macro",62],
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":382
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"Streaming Transmedia",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik trans tv streaming",
      "vtp_eventLabel":"trans tv streaming",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":384
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"Streaming Transmedia",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik trans 7 streaming",
      "vtp_eventLabel":"trans 7 streaming",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":385
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"Streaming Transmedia",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik cnn tv streaming",
      "vtp_eventLabel":"cnn tv streaming",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":386
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"Streaming Transmedia",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik cnbc tv streaming",
      "vtp_eventLabel":"cnbc tv streaming",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":387
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"button live tv",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik live tv",
      "vtp_eventLabel":"button live tv",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":388
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_doubleClick":false,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","page","value",["macro",80]],["map","fieldName","title","value",["macro",81]]],
      "vtp_useHashAutoLink":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_decorateFormsAutoLink":false,
      "vtp_enableLinkId":false,
      "vtp_enableEcommerce":false,
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":390
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_overrideGaSettings":false,
      "vtp_trackType":"TRACK_PAGEVIEW",
      "vtp_gaSettings":["macro",82],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "tag_id":391
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":false,
      "vtp_eventCategory":"User Session",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",86],
      "vtp_eventAction":["macro",87],
      "vtp_eventLabel":["macro",88],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":393
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most popular",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":397
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik channel box",
      "vtp_eventLabel":"most popular",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":398
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"most commented",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":399
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"lifestyle",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":400
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"lifestyle",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":401
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"widget cnn",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":402
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"widget cnbc",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":403
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik selengkapnya",
      "vtp_eventLabel":"detikx",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":404
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik channel box",
      "vtp_eventLabel":"detikx",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":405
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik selengkapnya",
      "vtp_eventLabel":"video terpopuler",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":406
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik hl",
      "vtp_eventLabel":"video terpopuler",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":407
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik artikel",
      "vtp_eventLabel":"video terpopuler",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":408
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik channel box",
      "vtp_eventLabel":"video rekomendasi",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":409
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik lainnya",
      "vtp_eventLabel":"artikel lainnya",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":410
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik channel box",
      "vtp_eventLabel":"fokus berita",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":411
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"redesign wp",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik channel box",
      "vtp_eventLabel":"video",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":412
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"Panel Tracking",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_gaSettings":["macro",89],
      "vtp_eventAction":["macro",90],
      "vtp_eventLabel":["macro",91],
      "vtp_dimension":["list",["map","index","84","dimension",["macro",92]],["map","index","85","dimension",["macro",93]],["map","index","86","dimension",["macro",94]],["map","index","82","dimension",["macro",95]],["map","index","83","dimension",["macro",96]]],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":413
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"detik ramadan",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik submit kalkulator zakat",
      "vtp_eventLabel":"submit kalkulator zakat",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":414
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"detik ramadan",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik preview kartu lebaran",
      "vtp_eventLabel":"preview kartu lebaran",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":415
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"detik ramadan",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"klik bayar zakat",
      "vtp_eventLabel":"klik di sini",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":416
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"live tv",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"share facebook",
      "vtp_eventLabel":"button share",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":417
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"live tv",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"share twitter",
      "vtp_eventLabel":"button share",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":418
    },{
      "function":"__ua",
      "once_per_event":true,
      "vtp_nonInteraction":false,
      "vtp_overrideGaSettings":true,
      "vtp_eventCategory":"live tv",
      "vtp_trackType":"TRACK_EVENT",
      "vtp_eventAction":"share linked in",
      "vtp_eventLabel":"button share",
      "vtp_trackingId":["macro",50],
      "vtp_enableRecaptchaOption":false,
      "vtp_enableUaRlsa":false,
      "vtp_enableUseInternalVersion":false,
      "vtp_enableFirebaseCampaignData":true,
      "vtp_trackTypeIsEvent":true,
      "tag_id":419
    },{
      "function":"__cl",
      "tag_id":420
    },{
      "function":"__cl",
      "tag_id":421
    },{
      "function":"__cl",
      "tag_id":422
    },{
      "function":"__cl",
      "tag_id":423
    },{
      "function":"__cl",
      "tag_id":424
    },{
      "function":"__cl",
      "tag_id":425
    },{
      "function":"__cl",
      "tag_id":426
    },{
      "function":"__cl",
      "tag_id":427
    },{
      "function":"__cl",
      "tag_id":428
    },{
      "function":"__cl",
      "tag_id":429
    },{
      "function":"__cl",
      "tag_id":430
    },{
      "function":"__cl",
      "tag_id":431
    },{
      "function":"__cl",
      "tag_id":432
    },{
      "function":"__cl",
      "tag_id":433
    },{
      "function":"__cl",
      "tag_id":434
    },{
      "function":"__cl",
      "tag_id":435
    },{
      "function":"__cl",
      "tag_id":436
    },{
      "function":"__cl",
      "tag_id":437
    },{
      "function":"__cl",
      "tag_id":438
    },{
      "function":"__cl",
      "tag_id":439
    },{
      "function":"__cl",
      "tag_id":440
    },{
      "function":"__cl",
      "tag_id":441
    },{
      "function":"__cl",
      "tag_id":442
    },{
      "function":"__cl",
      "tag_id":443
    },{
      "function":"__cl",
      "tag_id":444
    },{
      "function":"__cl",
      "tag_id":445
    },{
      "function":"__cl",
      "tag_id":446
    },{
      "function":"__cl",
      "tag_id":447
    },{
      "function":"__cl",
      "tag_id":448
    },{
      "function":"__cl",
      "tag_id":449
    },{
      "function":"__cl",
      "tag_id":450
    },{
      "function":"__cl",
      "tag_id":451
    },{
      "function":"__cl",
      "tag_id":452
    },{
      "function":"__cl",
      "tag_id":453
    },{
      "function":"__cl",
      "tag_id":454
    },{
      "function":"__cl",
      "tag_id":455
    },{
      "function":"__cl",
      "tag_id":456
    },{
      "function":"__cl",
      "tag_id":457
    },{
      "function":"__cl",
      "tag_id":458
    },{
      "function":"__cl",
      "tag_id":459
    },{
      "function":"__cl",
      "tag_id":460
    },{
      "function":"__cl",
      "tag_id":461
    },{
      "function":"__cl",
      "tag_id":462
    },{
      "function":"__cl",
      "tag_id":463
    },{
      "function":"__cl",
      "tag_id":464
    },{
      "function":"__cl",
      "tag_id":465
    },{
      "function":"__cl",
      "tag_id":466
    },{
      "function":"__cl",
      "tag_id":467
    },{
      "function":"__cl",
      "tag_id":468
    },{
      "function":"__cl",
      "tag_id":469
    },{
      "function":"__cl",
      "tag_id":470
    },{
      "function":"__cl",
      "tag_id":471
    },{
      "function":"__cl",
      "tag_id":472
    },{
      "function":"__cl",
      "tag_id":473
    },{
      "function":"__cl",
      "tag_id":474
    },{
      "function":"__cl",
      "tag_id":475
    },{
      "function":"__cl",
      "tag_id":476
    },{
      "function":"__cl",
      "tag_id":477
    },{
      "function":"__cl",
      "tag_id":478
    },{
      "function":"__cl",
      "tag_id":479
    },{
      "function":"__cl",
      "tag_id":480
    },{
      "function":"__cl",
      "tag_id":481
    },{
      "function":"__cl",
      "tag_id":482
    },{
      "function":"__cl",
      "tag_id":483
    },{
      "function":"__cl",
      "tag_id":484
    },{
      "function":"__cl",
      "tag_id":485
    },{
      "function":"__cl",
      "tag_id":486
    },{
      "function":"__cl",
      "tag_id":487
    },{
      "function":"__cl",
      "tag_id":488
    },{
      "function":"__cl",
      "tag_id":489
    },{
      "function":"__cl",
      "tag_id":490
    },{
      "function":"__cl",
      "tag_id":491
    },{
      "function":"__cl",
      "tag_id":492
    },{
      "function":"__cl",
      "tag_id":493
    },{
      "function":"__cl",
      "tag_id":494
    },{
      "function":"__cl",
      "tag_id":495
    },{
      "function":"__cl",
      "tag_id":496
    },{
      "function":"__cl",
      "tag_id":497
    },{
      "function":"__cl",
      "tag_id":498
    },{
      "function":"__cl",
      "tag_id":499
    },{
      "function":"__cl",
      "tag_id":500
    },{
      "function":"__cl",
      "tag_id":501
    },{
      "function":"__cl",
      "tag_id":502
    },{
      "function":"__cl",
      "tag_id":503
    },{
      "function":"__cl",
      "tag_id":504
    },{
      "function":"__cl",
      "tag_id":505
    },{
      "function":"__cl",
      "tag_id":506
    },{
      "function":"__cl",
      "tag_id":507
    },{
      "function":"__cl",
      "tag_id":508
    },{
      "function":"__cl",
      "tag_id":509
    },{
      "function":"__cl",
      "tag_id":510
    },{
      "function":"__cl",
      "tag_id":511
    },{
      "function":"__cl",
      "tag_id":512
    },{
      "function":"__cl",
      "tag_id":513
    },{
      "function":"__cl",
      "tag_id":514
    },{
      "function":"__cl",
      "tag_id":515
    },{
      "function":"__cl",
      "tag_id":516
    },{
      "function":"__cl",
      "tag_id":517
    },{
      "function":"__cl",
      "tag_id":518
    },{
      "function":"__cl",
      "tag_id":519
    },{
      "function":"__cl",
      "tag_id":520
    },{
      "function":"__cl",
      "tag_id":521
    },{
      "function":"__cl",
      "tag_id":522
    },{
      "function":"__cl",
      "tag_id":523
    },{
      "function":"__cl",
      "tag_id":524
    },{
      "function":"__cl",
      "tag_id":525
    },{
      "function":"__cl",
      "tag_id":526
    },{
      "function":"__cl",
      "tag_id":527
    },{
      "function":"__cl",
      "tag_id":528
    },{
      "function":"__cl",
      "tag_id":529
    },{
      "function":"__cl",
      "tag_id":530
    },{
      "function":"__cl",
      "tag_id":531
    },{
      "function":"__cl",
      "tag_id":532
    },{
      "function":"__cl",
      "tag_id":533
    },{
      "function":"__cl",
      "tag_id":534
    },{
      "function":"__cl",
      "tag_id":535
    },{
      "function":"__cl",
      "tag_id":536
    },{
      "function":"__cl",
      "tag_id":537
    },{
      "function":"__cl",
      "tag_id":538
    },{
      "function":"__cl",
      "tag_id":539
    },{
      "function":"__cl",
      "tag_id":540
    },{
      "function":"__cl",
      "tag_id":541
    },{
      "function":"__cl",
      "tag_id":542
    },{
      "function":"__cl",
      "tag_id":543
    },{
      "function":"__cl",
      "tag_id":544
    },{
      "function":"__cl",
      "tag_id":545
    },{
      "function":"__cl",
      "tag_id":546
    },{
      "function":"__cl",
      "tag_id":547
    },{
      "function":"__cl",
      "tag_id":548
    },{
      "function":"__cl",
      "tag_id":549
    },{
      "function":"__cl",
      "tag_id":550
    },{
      "function":"__cl",
      "tag_id":551
    },{
      "function":"__cl",
      "tag_id":552
    },{
      "function":"__cl",
      "tag_id":553
    },{
      "function":"__cl",
      "tag_id":554
    },{
      "function":"__cl",
      "tag_id":555
    },{
      "function":"__cl",
      "tag_id":556
    },{
      "function":"__cl",
      "tag_id":557
    },{
      "function":"__cl",
      "tag_id":558
    },{
      "function":"__cl",
      "tag_id":559
    },{
      "function":"__cl",
      "tag_id":560
    },{
      "function":"__cl",
      "tag_id":561
    },{
      "function":"__cl",
      "tag_id":562
    },{
      "function":"__cl",
      "tag_id":563
    },{
      "function":"__cl",
      "tag_id":564
    },{
      "function":"__cl",
      "tag_id":565
    },{
      "function":"__cl",
      "tag_id":566
    },{
      "function":"__cl",
      "tag_id":567
    },{
      "function":"__cl",
      "tag_id":568
    },{
      "function":"__cl",
      "tag_id":569
    },{
      "function":"__cl",
      "tag_id":570
    },{
      "function":"__cl",
      "tag_id":571
    },{
      "function":"__cl",
      "tag_id":572
    },{
      "function":"__cl",
      "tag_id":573
    },{
      "function":"__cl",
      "tag_id":574
    },{
      "function":"__cl",
      "tag_id":575
    },{
      "function":"__cl",
      "tag_id":576
    },{
      "function":"__cl",
      "tag_id":577
    },{
      "function":"__cl",
      "tag_id":578
    },{
      "function":"__cl",
      "tag_id":579
    },{
      "function":"__cl",
      "tag_id":580
    },{
      "function":"__cl",
      "tag_id":581
    },{
      "function":"__cl",
      "tag_id":582
    },{
      "function":"__cl",
      "tag_id":583
    },{
      "function":"__cl",
      "tag_id":584
    },{
      "function":"__cl",
      "tag_id":585
    },{
      "function":"__cl",
      "tag_id":586
    },{
      "function":"__cl",
      "tag_id":587
    },{
      "function":"__cl",
      "tag_id":588
    },{
      "function":"__cl",
      "tag_id":589
    },{
      "function":"__cl",
      "tag_id":590
    },{
      "function":"__cl",
      "tag_id":591
    },{
      "function":"__cl",
      "tag_id":592
    },{
      "function":"__cl",
      "tag_id":593
    },{
      "function":"__cl",
      "tag_id":594
    },{
      "function":"__cl",
      "tag_id":595
    },{
      "function":"__cl",
      "tag_id":596
    },{
      "function":"__cl",
      "tag_id":597
    },{
      "function":"__cl",
      "tag_id":598
    },{
      "function":"__cl",
      "tag_id":599
    },{
      "function":"__cl",
      "tag_id":600
    },{
      "function":"__cl",
      "tag_id":601
    },{
      "function":"__cl",
      "tag_id":602
    },{
      "function":"__cl",
      "tag_id":603
    },{
      "function":"__cl",
      "tag_id":604
    },{
      "function":"__cl",
      "tag_id":605
    },{
      "function":"__cl",
      "tag_id":606
    },{
      "function":"__cl",
      "tag_id":607
    },{
      "function":"__cl",
      "tag_id":608
    },{
      "function":"__cl",
      "tag_id":609
    },{
      "function":"__cl",
      "tag_id":610
    },{
      "function":"__cl",
      "tag_id":611
    },{
      "function":"__cl",
      "tag_id":612
    },{
      "function":"__cl",
      "tag_id":613
    },{
      "function":"__cl",
      "tag_id":614
    },{
      "function":"__cl",
      "tag_id":615
    },{
      "function":"__cl",
      "tag_id":616
    },{
      "function":"__cl",
      "tag_id":617
    },{
      "function":"__cl",
      "tag_id":618
    },{
      "function":"__cl",
      "tag_id":619
    },{
      "function":"__cl",
      "tag_id":620
    },{
      "function":"__cl",
      "tag_id":621
    },{
      "function":"__cl",
      "tag_id":622
    },{
      "function":"__cl",
      "tag_id":623
    },{
      "function":"__cl",
      "tag_id":624
    },{
      "function":"__cl",
      "tag_id":625
    },{
      "function":"__cl",
      "tag_id":626
    },{
      "function":"__cl",
      "tag_id":627
    },{
      "function":"__cl",
      "tag_id":628
    },{
      "function":"__cl",
      "tag_id":629
    },{
      "function":"__cl",
      "tag_id":630
    },{
      "function":"__cl",
      "tag_id":631
    },{
      "function":"__cl",
      "tag_id":632
    },{
      "function":"__cl",
      "tag_id":633
    },{
      "function":"__cl",
      "tag_id":634
    },{
      "function":"__cl",
      "tag_id":635
    },{
      "function":"__cl",
      "tag_id":636
    },{
      "function":"__cl",
      "tag_id":637
    },{
      "function":"__cl",
      "tag_id":638
    },{
      "function":"__cl",
      "tag_id":639
    },{
      "function":"__cl",
      "tag_id":640
    },{
      "function":"__cl",
      "tag_id":641
    },{
      "function":"__cl",
      "tag_id":642
    },{
      "function":"__cl",
      "tag_id":643
    },{
      "function":"__cl",
      "tag_id":644
    },{
      "function":"__cl",
      "tag_id":645
    },{
      "function":"__cl",
      "tag_id":646
    },{
      "function":"__cl",
      "tag_id":647
    },{
      "function":"__cl",
      "tag_id":648
    },{
      "function":"__cl",
      "tag_id":649
    },{
      "function":"__cl",
      "tag_id":650
    },{
      "function":"__cl",
      "tag_id":651
    },{
      "function":"__cl",
      "tag_id":652
    },{
      "function":"__cl",
      "tag_id":653
    },{
      "function":"__cl",
      "tag_id":654
    },{
      "function":"__cl",
      "tag_id":655
    },{
      "function":"__cl",
      "tag_id":656
    },{
      "function":"__cl",
      "tag_id":657
    },{
      "function":"__cl",
      "tag_id":658
    },{
      "function":"__cl",
      "tag_id":659
    },{
      "function":"__cl",
      "tag_id":660
    },{
      "function":"__cl",
      "tag_id":661
    },{
      "function":"__cl",
      "tag_id":662
    },{
      "function":"__cl",
      "tag_id":663
    },{
      "function":"__cl",
      "tag_id":664
    },{
      "function":"__cl",
      "tag_id":665
    },{
      "function":"__cl",
      "tag_id":666
    },{
      "function":"__cl",
      "tag_id":667
    },{
      "function":"__cl",
      "tag_id":668
    },{
      "function":"__cl",
      "tag_id":669
    },{
      "function":"__cl",
      "tag_id":670
    },{
      "function":"__cl",
      "tag_id":671
    },{
      "function":"__cl",
      "tag_id":672
    },{
      "function":"__cl",
      "tag_id":673
    },{
      "function":"__cl",
      "tag_id":674
    },{
      "function":"__cl",
      "tag_id":675
    },{
      "function":"__cl",
      "tag_id":676
    },{
      "function":"__cl",
      "tag_id":677
    },{
      "function":"__cl",
      "tag_id":678
    },{
      "function":"__cl",
      "tag_id":679
    },{
      "function":"__cl",
      "tag_id":680
    },{
      "function":"__cl",
      "tag_id":681
    },{
      "function":"__cl",
      "tag_id":682
    },{
      "function":"__cl",
      "tag_id":683
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_442",
      "tag_id":684
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_485",
      "tag_id":685
    },{
      "function":"__lcl",
      "vtp_waitForTags":true,
      "vtp_checkValidation":true,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_490",
      "tag_id":686
    },{
      "function":"__cl",
      "tag_id":687
    },{
      "function":"__cl",
      "tag_id":688
    },{
      "function":"__cl",
      "tag_id":689
    },{
      "function":"__cl",
      "tag_id":690
    },{
      "function":"__cl",
      "tag_id":691
    },{
      "function":"__cl",
      "tag_id":692
    },{
      "function":"__cl",
      "tag_id":693
    },{
      "function":"__cl",
      "tag_id":694
    },{
      "function":"__cl",
      "tag_id":695
    },{
      "function":"__cl",
      "tag_id":696
    },{
      "function":"__cl",
      "tag_id":697
    },{
      "function":"__cl",
      "tag_id":698
    },{
      "function":"__cl",
      "tag_id":699
    },{
      "function":"__cl",
      "tag_id":700
    },{
      "function":"__cl",
      "tag_id":701
    },{
      "function":"__cl",
      "tag_id":702
    },{
      "function":"__sdl",
      "vtp_verticalThresholdUnits":"PIXELS",
      "vtp_verticalThresholdOn":true,
      "vtp_verticalThresholdsPixels":["macro",12],
      "vtp_horizontalThresholdOn":false,
      "vtp_uniqueTriggerId":"2264312_565",
      "vtp_enableTriggerStartOption":true,
      "tag_id":703
    },{
      "function":"__cl",
      "tag_id":704
    },{
      "function":"__cl",
      "tag_id":705
    },{
      "function":"__cl",
      "tag_id":706
    },{
      "function":"__cl",
      "tag_id":707
    },{
      "function":"__cl",
      "tag_id":708
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_597",
      "tag_id":709
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_598",
      "tag_id":710
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_599",
      "tag_id":711
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_600",
      "tag_id":712
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_601",
      "tag_id":713
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_602",
      "tag_id":714
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_603",
      "tag_id":715
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_604",
      "tag_id":716
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_605",
      "tag_id":717
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_606",
      "tag_id":718
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_607",
      "tag_id":719
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_608",
      "tag_id":720
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_612",
      "tag_id":721
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_613",
      "tag_id":722
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_614",
      "tag_id":723
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_615",
      "tag_id":724
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_616",
      "tag_id":725
    },{
      "function":"__cl",
      "tag_id":726
    },{
      "function":"__cl",
      "tag_id":727
    },{
      "function":"__lcl",
      "vtp_waitForTags":false,
      "vtp_checkValidation":false,
      "vtp_waitForTagsTimeout":"2000",
      "vtp_uniqueTriggerId":"2264312_630",
      "tag_id":728
    },{
      "function":"__cl",
      "tag_id":729
    },{
      "function":"__cl",
      "tag_id":730
    },{
      "function":"__cl",
      "tag_id":731
    },{
      "function":"__cl",
      "tag_id":732
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\n\u003Cscript type=\"text\/gtmscript\"\u003E_atrk_opts={atrk_acct:\"jpQMs1FYxz20cv\",domain:\"detik.com\",dynamic:!0};(function(){var a=document.createElement(\"script\");a.type=\"text\/javascript\";a.async=!0;a.src=\"https:\/\/certify-js.alexametrics.com\/atrk.js\";var b=document.getElementsByTagName(\"script\")[0];b.parentNode.insertBefore(a,b)})();\u003C\/script\u003E\n\n\u003Cnoscript\u003E\u003Cimg src=\"https:\/\/certify.alexametrics.com\/atrk.gif?account=jpQMs1FYxz20cv\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"\"\u003E\u003C\/noscript\u003E\n\n ",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":258
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003E!function(b,e,f,g,a,c,d){b.fbq||(a=b.fbq=function(){a.callMethod?a.callMethod.apply(a,arguments):a.queue.push(arguments)},b._fbq||(b._fbq=a),a.push=a,a.loaded=!0,a.version=\"2.0\",a.queue=[],c=e.createElement(f),c.async=!0,c.src=g,d=e.getElementsByTagName(f)[0],d.parentNode.insertBefore(c,d))}(window,document,\"script\",\"https:\/\/connect.facebook.net\/en_US\/fbevents.js\");fbq(\"init\",\"304159643041671\");fbq(\"track\",\"PageView\");\u003C\/script\u003E\n\u003Cnoscript\u003E\n \u003Cimg height=\"1\" width=\"1\" src=\"https:\/\/www.facebook.com\/tr?id=304159643041671\u0026amp;ev=PageView\n\u0026amp;noscript=1\"\u003E\n\u003C\/noscript\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":259
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003Evar _comscore=_comscore||[];_comscore.push({c1:\"2\",c2:\"8443234\"});(function(){var a=document.createElement(\"script\"),b=document.getElementsByTagName(\"script\")[0];a.async=!0;a.src=(\"https:\"==document.location.protocol?\"https:\/\/sb\":\"https:\/\/b\")+\".scorecardresearch.com\/beacon.js\";b.parentNode.insertBefore(a,b)})();\u003C\/script\u003E\n\u003Cnoscript\u003E\n  \u003Cimg src=\"https:\/\/b.scorecardresearch.com\/p?c1=2\u0026amp;c2=8443234\u0026amp;cv=2.0\u0026amp;cj=1\"\u003E\n\u003C\/noscript\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":298
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E$(function(){pushEvent=function(b,a,c){dataLayer.push({event:\"\"+b+\"\",bc_nav_url:\"\"+a+\"\",bc_domain:\"\"+baseurl+\"\",bc_title:\"\"+c+\"\",baca_juga_aid:\"\"+getArticleId(b,a)+\"\"});$(this).stopPropagation()};getArticleId=function(b,a){arrUrl=a.split(\"\/\");arID=9\u003CarrUrl.length?-1\u003CarrUrl[2].indexOf(\"m.detik\")||-1\u003CarrUrl[2].indexOf(\"mdev.detik\")?-1\u003Ca.indexOf(\"\/~\")?arrUrl[10]:arrUrl[9]:-1\u003CarrUrl[3].indexOf(\"sepakbola\")?arrUrl[9]:arrUrl[8]:-1\u003CarrUrl[2].indexOf(\"garuda.detik\")?-1\u003Ca.indexOf(\"\/~\")?arrUrl[6]:arrUrl[5]:\n-1\u003Ca.indexOf(\"\/~\")?\"sepakbola\"==arrUrl[4]?arrUrl[6]:arrUrl[5]:\"sepakbola\"==arrUrl[3]?arrUrl[5]:arrUrl[4];-1\u003CarID.indexOf(\"d-\")\u0026\u0026(arID=arID.split(\"-\")[1]);return arID};$(\"#relatedgtm a\").click(function(){pushEvent(\"beritaterkait\",$(this).attr(\"href\"),$(this).text().trim())});$(\"#readtoogtm a\").click(function(){var b=$(this).find(\"h2\").length?$(this).find(\"h2\").text().trim():$(this).find(\"h3\").text().trim();pushEvent(\"bacajuga\",$(this).attr(\"href\"),b)})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":327
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E$(function(){pushEvent=function(b,a,c){dataLayer.push({event:\"\"+b+\"\",bc_nav_url:\"\"+a+\"\",bc_domain:\"\"+baseurl+\"\",bc_title:\"\"+c+\"\",baca_juga_aid:\"\"+getArticleId(b,a)+\"\"})};getArticleId=function(b,a){arrUrl=a.split(\"\/\");arID=9\u003CarrUrl.length?-1\u003CarrUrl[2].indexOf(\"m.detik\")||-1\u003CarrUrl[2].indexOf(\"mdev.detik\")?-1\u003Ca.indexOf(\"\/~\")?arrUrl[10]:arrUrl[9]:-1\u003CarrUrl[3].indexOf(\"sepakbola\")?arrUrl[9]:arrUrl[8]:-1\u003CarrUrl[2].indexOf(\"garuda.detik\")?-1\u003Ca.indexOf(\"\/~\")?arrUrl[6]:arrUrl[5]:-1\u003CarrUrl[2].indexOf(\"m.detik\")?\narrUrl[5]:-1\u003Ca.indexOf(\"\/~\")?\"sepakbola\"==arrUrl[4]?arrUrl[6]:arrUrl[5]:\"sepakbola\"==arrUrl[3]?arrUrl[5]:arrUrl[4];-1\u003CarID.indexOf(\"d-\")\u0026\u0026(arID=arID.split(\"-\")[1]);return arID};$(\"#relatedgtm a\").click(function(){pushEvent(\"beritaterkait\",$(this).attr(\"href\"),$(this).text().trim())});$(\"#readtoogtm a\").click(function(){var b=$(this).find(\"h2\").length?$(this).find(\"h2\").text().trim():$(this).find(\"h3\").text().trim();pushEvent(\"bacajuga\",$(this).attr(\"href\"),b)})});\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":328
    },{
      "function":"__html",
      "setup_tags":["list",["tag",648,0]],
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003Efbq(\"init\",\"304159643041671\");fbq(\"track\",\"ViewContent\");\u003C\/script\u003E\n\u003Cnoscript\u003E\n \u003Cimg height=\"1\" width=\"1\" src=\"https:\/\/www.facebook.com\/tr?id=304159643041671\u0026amp;ev=PageView\n\u0026amp;noscript=1\"\u003E\n\u003C\/noscript\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":329
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\" id=\"gtm-scroll-tracking\"\u003E(function(c){function d(a){if(!(this instanceof d))return new d(a);a=a||{};var b=a.context||\"body\";\"string\"===typeof b\u0026\u0026(b=h.querySelector(b));if(!b)throw Error(\"Unable to find context \"+b);this._context=b;this.minHeight=a.minHeight||0;this._marks={};this._tracked={};this._config={percentages:{each:{},every:{}},pixels:{each:{},every:{}},elements:{each:{},every:{}}};a=n(this._checkDepth.bind(this),500);b=this._update.bind(this);var g=n(b,500);c.addEventListener(\"scroll\",a,!0);c.addEventListener(\"resize\",\ng);this._artifacts={timer:q(b),resize:g,scroll:a}}function r(a){return a.handlers.map(function(b){return b.bind(this,{data:{depth:a.depth,label:a.label}})})}function p(a){var b=Math.floor(a.numerator\/a.n),g;for(g=1;g\u003C=b;g++)a.callback(g*a.n)}function q(a){var b=m();return setInterval(function(){m()!==b\u0026\u0026(a(),b=m())},500)}function m(){var a=h.body,b=h.documentElement;return Math.max(a.scrollHeight,a.offsetHeight,b.clientHeight,b.scrollHeight,b.offsetHeight)}function t(a){a=a.getBoundingClientRect().top;\nvar b=void 0!==c.pageYOffset?c.pageYOffset:(h.documentElement||h.body.parentNode||h.body).scrollTop;return a+b}function u(){}function n(a,b){var g,e,d,l=null,c=0,f=function(){c=new Date;l=null;d=a.apply(g,e)};return function(){var k=new Date;c||(c=k);var h=b-(k-c);g=this;e=arguments;0\u003E=h?(clearTimeout(l),l=null,c=k,d=a.apply(g,e)):l||(l=setTimeout(f,h));return d}}function v(){var a={},b;for(b in d)a[b]=u;c.ScrollTracker=a}if(c.navigator.userAgent.match(\/MSIE [678]\/gi))return v();var h=c.document;\nd.prototype.destroy=function(){clearInterval(this._artifacts._timer);c.removeEventListener(\"resize\",this._artifacts.resize);c.removeEventListener(\"scroll\",this._artifacts.scroll,!0)};d.prototype.on=function(a,b){var g=this._config;[\"percentages\",\"pixels\",\"elements\"].forEach(function(e){a[e]\u0026\u0026[\"each\",\"every\"].forEach(function(c){a[e][c]\u0026\u0026a[e][c].forEach(function(a){g[e][c][a]=g[e][c][a]||[];g[e][c][a].push(b)})})});this._update()};d.prototype._update=function(){this._calculateMarks();this._checkDepth()};\nd.prototype._calculateMarks=function(){function a(a,b){return function(b,c){var g=b.getBoundingClientRect().top-h._context.getBoundingClientRect().top;d({label:a+\"[\"+c+\"]\",depth:g,handlers:e.elements.every[a]})}}function b(a){return function(a){var b=Math.floor(a*c\/100);d({label:String(a)+\"%\",depth:b,handlers:e.percentages.every[f]})}}function g(a){return function(b){d({label:String(b)+\"px\",depth:b,handlers:a})}}delete this._marks;this._fromTop=t(this._context);this._marks={};var e=this._config,c=\nthis._contextHeight(),d=this._addMark.bind(this),h=this,f;if(!(c\u003Cthis.minHeight)){for(f in e.percentages.every)p({n:Number(f),numerator:100,callback:b(e.percentages.every[f])});for(f in e.pixels.every)p({n:Number(f),numerator:c,callback:g(e.pixels.every[f])});for(f in e.percentages.each){var k=Math.floor(c*Number(f)\/100);d({label:f+\"%\",depth:k,handlers:e.percentages.each[f]})}for(f in e.pixels.each)k=Number(f),d({label:f+\"px\",depth:k,handlers:e.pixels.each[f]});for(f in e.elements.every)k=[].slice.call(this._context.querySelectorAll(f)),\nk.length\u0026\u0026k.forEach(a(f,e.elements.every[f]));for(f in e.elements.each)if(k=this._context.querySelector(f))k=k.getBoundingClientRect().top-h._context.getBoundingClientRect().top,d({label:f,depth:k,handlers:e.elements.each[f]})}};d.prototype._checkDepth=function(){var a=this._marks,b=this._currentDepth(),c;for(c in a)b\u003E=c\u0026\u0026!this._tracked[c]\u0026\u0026(a[c].forEach(function(a){a()}),this._tracked[c]=!0)};d.prototype.reset=function(){this._tracked={};delete this._marks;this.marks={}};d.prototype._contextHeight=\nfunction(){return this._context!==h.body?this._context.scrollHeight-5:this._context.clientHeight-5};d.prototype._currentDepth=function(){var a=this._context;var b=a.offsetHeight;var d=\"CSS1Compat\"===h.compatMode?h.documentElement:h.body;d=d.clientHeight;a=a.getBoundingClientRect();b=Math.max(0,0\u003Ca.top?Math.min(b,d-a.top):a.bottom\u003Cd?a.bottom:d);this._context.scrollTop?a=this._context.scrollTop+b:(this._context.scrollTop=1,this._context.scrollTop?(this._context.scrollTop=0,a=this._context.scrollTop+\nb):a=c.pageYOffset||h.documentElement.scrollTop||h.body.scrollTop||0);return b?a+b:a\u003E=this._fromTop?a:-1};d.prototype._addMark=function(a){var b=a.depth;this._marks[b]=(this._marks[b]||[]).concat(r(a))};c.ScrollTracker=d})(this);\n(function(c){function d(){var d=c.ScrollTracker();d.on({percentages:{each:[16,26,33]}},function(c){c=\"16%\"==c.data.label?\"0%\":\"26%\"==c.data.label?\"50%\":\"100%\";dataLayer.push({event:\"scrollTracking\",gaEventAction:c})});delete c.ScrollTracker}\"loading\"!==document.readyState?d():document.addEventListener(\"DOMContentLoaded\",d)})(window);\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":334
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var b=document.querySelector(\"#input_search_gadget input\");b.addEventListener(\"keyup\",function(a){13===a.keyCode\u0026\u0026window.dataLayer.push({event:\"addressInputEnter\",enterElement:a.target})},!0)})();\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":339
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(){var a=document.querySelector(\"input#autocompletegadget\"),f=2E3,g=3,b=!1,c,d,e=function(){d=a?a.value:\"\";d.length\u003Cg||(window.dataLayer.push({event:\"customSearch\",customSearchInput:d}),b=!1)},h=function(a){b=!0;window.clearTimeout(c);13===a.keyCode?e():c=setTimeout(e,f)};null!==a\u0026\u0026(a.addEventListener(\"keydown\",h,!0),a.addEventListener(\"blur\",function(){b\u0026\u0026(window.clearTimeout(c),e())},!0))})();\u003C\/script\u003E\n",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":341
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":"\n\u003Cscript type=\"text\/gtmscript\"\u003E(function(a,c,e,f,d,b){a.hj=a.hj||function(){(a.hj.q=a.hj.q||[]).push(arguments)};a._hjSettings={hjid:909149,hjsv:6};d=c.getElementsByTagName(\"head\")[0];b=c.createElement(\"script\");b.async=1;b.src=e+a._hjSettings.hjid+f+a._hjSettings.hjsv;d.appendChild(b)})(window,document,\"https:\/\/static.hotjar.com\/c\/hotjar-\",\".js?sv\\x3d\");\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":370
    },{
      "function":"__html",
      "once_per_event":true,
      "vtp_html":["template","\u003Cscript type=\"text\/gtmscript\"\u003EdataLayer.push({event:\"scrolldepthArticle\",depth:",["escape",["macro",14],8,16],"+\"%\"});\u003C\/script\u003E"],
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":380
    },{
      "function":"__html",
      "once_per_load":true,
      "vtp_html":"\u003Cscript type=\"text\/gtmscript\"\u003E(function(c,e){var a=function(b){this._options={loopCheckTime:50,loopMaxNumber:5,baitClass:\"pub_300x250 pub_300x250m pub_728x90 text-ad textAd text_ad text_ads text-ads text-ad-links\",baitStyle:\"width: 1px !important; height: 1px !important; position: absolute !important; left: -10000px !important; top: -1000px !important;\"};this._var={bait:null,checking:!1,loop:null,loopNumber:0,event:{detected:[],notDetected:[]}}};a.prototype._options=null;a.prototype._var=null;a.prototype._bait=null;a.prototype._creatBait=\nfunction(){var b=document.createElement(\"div\");b.setAttribute(\"class\",this._options.baitClass);b.setAttribute(\"style\",this._options.baitStyle);this._var.bait=c.document.body.appendChild(b);this._var.bait.offsetParent;this._var.bait.offsetHeight;this._var.bait.offsetLeft;this._var.bait.offsetTop;this._var.bait.offsetWidth;this._var.bait.clientHeight;this._var.bait.clientWidth};a.prototype._destroyBait=function(){c.document.body.removeChild(this._var.bait);this._var.bait=null};a.prototype.check=function(b){b===\ne\u0026\u0026(b=!0);if(this._var.checking)return!1;this._var.checking=!0;null===this._var.bait\u0026\u0026this._creatBait();var a=this;this._var.loopNumber=0;!0===b\u0026\u0026(this._var.loop=setInterval(function(){a._checkBait(b)},this._options.loopCheckTime));setTimeout(function(){a._checkBait(b)},1);return!0};a.prototype._checkBait=function(b){var a=!1;null===this._var.bait\u0026\u0026this._creatBait();if(null!==c.document.body.getAttribute(\"abp\")||null===this._var.bait.offsetParent||0==this._var.bait.offsetHeight||0==this._var.bait.offsetLeft||\n0==this._var.bait.offsetTop||0==this._var.bait.offsetWidth||0==this._var.bait.clientHeight||0==this._var.bait.clientWidth)a=!0;if(c.getComputedStyle!==e){var d=c.getComputedStyle(this._var.bait,null);!d||\"none\"!=d.getPropertyValue(\"display\")\u0026\u0026\"hidden\"!=d.getPropertyValue(\"visibility\")||(a=!0)}!0===b\u0026\u0026(this._var.loopNumber++,this._var.loopNumber\u003E=this._options.loopMaxNumber\u0026\u0026this._stopLoop());if(!0===a)this._stopLoop(),this._destroyBait(),this.emitEvent(!0),!0===b\u0026\u0026(this._var.checking=!1);else if(null===\nthis._var.loop||!1===b)this._destroyBait(),this.emitEvent(!1),!0===b\u0026\u0026(this._var.checking=!1)};a.prototype._stopLoop=function(b){clearInterval(this._var.loop);this._var.loop=null;this._var.loopNumber=0};a.prototype.emitEvent=function(b){b=this._var.event[!0===b?\"detected\":\"notDetected\"];for(var a in b)if(b.hasOwnProperty(a))b[a]();this.clearEvent();return this};a.prototype.clearEvent=function(){this._var.event.detected=[];this._var.event.notDetected=[]};a.prototype.on=function(a,c){this._var.event[a?\n\"detected\":\"notDetected\"].push(c);return this};a=new a;a.on(!0,function(){dataLayer.push({adBlocker:!0})});a.on(!1,function(){dataLayer.push({adBlocker:!1})});a.check()})(window);\u003C\/script\u003E",
      "vtp_supportDocumentWrite":false,
      "vtp_enableIframeMode":false,
      "vtp_enableEditJsMacroBehavior":false,
      "tag_id":330
    },{
      "function":"__opt",
      "setup_tags":["list",["tag",658,0]],
      "once_per_event":true,
      "vtp_overrideGaSettings":true,
      "vtp_setTrackerName":false,
      "vtp_useDebugVersion":false,
      "vtp_fieldsToSet":["list",["map","fieldName","allowLinker","value","true"],["map","fieldName","cookieDomain","value","auto"],["map","fieldName","userId","value",["macro",17]],["map","fieldName","DAtest","value",["macro",18]]],
      "vtp_optimizeContainerId":["macro",98],
      "vtp_trackingId":["macro",50],
      "tag_id":332
    }],
  "predicates":[{
      "function":"_re",
      "arg0":["macro",15],
      "arg1":"contenttype"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"gtm.js"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"ico_detik"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"sport.detik.com"
    },{
      "function":"_re",
      "arg0":["macro",53],
      "arg1":"\/(.*)\/[0-9]{1,7}\/(.*)"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"gtm.click"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikcom"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikNews"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikFinance"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Moto GP"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikHot"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikInet"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikSport"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div#framebar_new \u003E div.new_container \u003E div#drop_kanal \u003E div \u003E a:nth-child(7) \u003E span"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikOto"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikFood"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikHealth"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Wolipop"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikTravel"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detiktv"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikFoto"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Pasangmata"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikForum"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Blogdetik"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Morning Friends"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"detikRewards"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Solusi UKM"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"dNewGeneration"
    },{
      "function":"_eq",
      "arg0":["macro",56],
      "arg1":"btn_s"
    },{
      "function":"_eq",
      "arg0":["macro",56],
      "arg1":"daftar to_reg"
    },{
      "function":"_eq",
      "arg0":["macro",56],
      "arg1":"masuk to_login"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Profil Saya"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Dashboard"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Keluar"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/sport.detik.com\/images\/favicon_detikcom.png"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Home"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Basket"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Raket"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.header_top \u003E div.menu \u003E ul \u003E li:nth-child(8) \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.header_top \u003E div.menu \u003E ul \u003E li:nth-child(12) \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Sport Lain"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Foto Sport"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Video Sport"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"About The Game"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Indeks"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Most Popular"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Siaran Langsung"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Jadwal Pertandingan"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Klasemen"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Statistik Center"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Live Match Center"
    },{
      "function":"_eq",
      "arg0":["macro",58],
      "arg1":"bslide"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.beritautama \u003E div.slide_bu \u003E div.caroufredsel_wrapper \u003E ul#slide_bu \u003E li \u003E article \u003E a \u003E div.ratio16_9.box_img \u003E div.img_con.lqd.imgLiquid_bgSize.imgLiquid_ready"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.headline \u003E div \u003E article \u003E div.text \u003E h1 \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.headline \u003E div \u003E article \u003E div.text \u003E div.hl_kait \u003E div.article_list \u003E article \u003E a \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"a.t_lfm"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#newsfeed-container \u003E li \u003E article \u003E div.desc_nhl \u003E a \u003E h2"
    },{
      "function":"_eq",
      "arg0":["macro",58],
      "arg1":"but_more"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"MORE NEWS"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.boxlr.mb15.mpop \u003E div#box-pop \u003E ul \u003E li \u003E article \u003E a \u003E div.title_lnf \u003E h2 \u003E span"
    },{
      "function":"_eq",
      "arg0":["macro",58],
      "arg1":"logodetik"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/sport.detik.com\/images\/logo_detik_footer.png"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/detik.com\/dapur\/redaksi"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Pedoman Media Siber"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Karir"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Kotak Pos"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Info Iklan"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Privacy Policy"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Disclaimer"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/sport.detik.com\/images\/foot_fb.png"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#box-pop div.a-center a.btn_more"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.boxlr.mb15.mpop \u003E div#box-com \u003E ul \u003E li \u003E article \u003E a \u003E div.title_lnf \u003E h2"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(6) \u003E ul \u003E li \u003E article \u003E a \u003E div.tl_vidfot \u003E h3 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(6) \u003E ul \u003E li \u003E article \u003E a \u003E div.ratio3_4.box_img.fl \u003E div \u003E span"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/sport.detik.com\/images\/foot_insta.png"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/sport.detik.com\/images\/foot_gplus.png"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/sport.detik.com\/images\/foot_tw.png"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(2) \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.boxlr.mb15.bg_white.sfeatured \u003E a \u003E div.text \u003E div \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(5) \u003E ul \u003E li:nth-child(1) \u003E article \u003E a \u003E div.ratio16_9.box_img \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(3) \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(5) \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(6) \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(6) \u003E div.title_lr \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(5) \u003E div \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",59],
      "arg1":"gaTriggerEvent"
    },{
      "function":"_re",
      "arg0":["macro",15],
      "arg1":"gaTriggerEvent"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.sticky.is_stuck \u003E  div.boxlr.mb15.mpop \u003E div#box-pop2 \u003E ul \u003E li \u003E article \u003E a \u003E div.num_fokus"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.sticky.is_stuck \u003E  div.boxlr.mb15.mpop \u003E div#box-pop2 \u003E ul \u003E li \u003E article \u003E a \u003E div.title_lnf \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.sticky.is_stuck \u003E  div.boxlr.mb15.mpop \u003E div#box-pop2 \u003E div.a-center \u003E a.btn_more"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.sticky.is_stuck \u003E  div.boxlr.mb15.mpop \u003E div#box-com2 \u003E ul \u003E li \u003E article \u003E a div.title_lnf \u003E h2"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.sticky.is_stuck \u003E  div.boxlr.mb15.mpop \u003E div#box-com2 \u003E ul \u003E li \u003E article \u003E a \u003E div.num"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.sticky.is_stuck \u003E div \u003E ul \u003E li \u003E article \u003E a \u003E div.num_fokus2"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.sticky.is_stuck \u003E div \u003E ul \u003E li \u003E article \u003E a \u003E div.title_lnf \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.sticky.is_stuck \u003E div \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#newsfeed-container \u003E li \u003E article \u003E div.ratio9_8.box_img.fl.mr10 \u003E div.img_con.lqd.imgLiquid_bgSize.imgLiquid_ready \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.boxlr.mb15.sbola \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(10) \u003E div.title_lr \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(11) \u003E div.title_lr \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.boxlr.mb15.slides \u003E div.title_lr \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.boxlr.mb15.slides2 \u003E div.title_lr \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(2) \u003E ul \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#tab_jadwal \u003E a:nth-child(2)"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#tab_jadwal \u003E a:nth-child(1)"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(10) \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#tab_klasemen \u003E a:nth-child(2)"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#tab_klasemen \u003E a:nth-child(1)"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(11) \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#slides \u003E li:nth-child(1) \u003E article \u003E a \u003E div.title_ln \u003E h3 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#slides \u003E li:nth-child(1) \u003E article \u003E a \u003E div.ratio4_3.box_img.fl \u003E div \u003E span"
    },{
      "function":"_re",
      "arg0":["macro",53],
      "arg1":"#slides \u003E li:nth-child(1) \u003E article \u003E a \u003E div.ratio4_3.box_img.fl \u003E div \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#page_slides \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#slides2 \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h3 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#slides2 \u003E li \u003E article \u003E a \u003E div.ratio4_3.box_img.fl \u003E div \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#page_slides2 \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(2) \u003E ul \u003E li \u003E article \u003E a \u003E div \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(3) \u003E ul \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(3) \u003E ul \u003E li \u003E article \u003E a \u003E div \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(5) \u003E ul \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(5) \u003E ul \u003E li \u003E article \u003E a \u003E div.ratio1_1.box_img.fl.mr10 \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(6) \u003E ul \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div:nth-child(6) \u003E ul \u003E li \u003E article \u003E a \u003E div.ratio1_1.box_img.fl.mr10 \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.boxlr.mb15.sbola \u003E ul \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.boxlr.mb15.sbola \u003E ul \u003E li \u003E article \u003E a \u003E div.ratio1_1.box_img.fl.mr10 \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.boxlr.mb15.sbola \u003E ul \u003E li \u003E article \u003E a \u003E div.ratio16_9.box_img.fl.mr10 \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.header_top \u003E div.menu2 \u003E a.new"
    },{
      "function":"_eq",
      "arg0":["macro",57],
      "arg1":"http:\/\/cdn.detik.net.id\/libs\/dc\/v1\/image\/favicon_detikcom.png"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.breadcrumb \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.btn_like \u003E a.fb \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.btn_like \u003E a.tw \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.btn_like \u003E a.gplus \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.btn_like \u003E a.insta \u003E img"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"www.detik.com"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#share_top \u003E a.fb \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#share_top \u003E a.tw \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#share_top \u003E a.gplus \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#share_top \u003E a.komentar \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E article \u003E div:nth-child(6) \u003E div.share-top-new \u003E div \u003E div \u003E a.share_detail.fb \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E article \u003E div:nth-child(6) \u003E div.share-top-new \u003E div \u003E div \u003E a.share_detail.tw \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E article \u003E div:nth-child(6) \u003E div.share-top-new \u003E div \u003E div \u003E a.share_detail.gplus \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E article \u003E div:nth-child(6) \u003E div.share-top-new \u003E div \u003E div \u003E a.share_detail.comm.komentar \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.bacajuga \u003E div.list_berita \u003E article \u003E a \u003E div \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#comment \u003E div.dk_action.reaction \u003E a.dk_balas \u003E span:nth-child(2)"
    },{
      "function":"_eq",
      "arg0":["macro",58],
      "arg1":"selm se_fb"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.newsfeed \u003E ul \u003E li \u003E article \u003E div.desc_nhl \u003E a \u003E h2"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.detail_content \u003E div.newsfeed \u003E ul \u003E li \u003E article \u003E div.ratio4_3.box_img.fl.mr10 \u003E div.img_con.lqd.imgLiquid_bgSize.imgLiquid_ready \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div \u003E ul \u003E li \u003E article \u003E a \u003E div.ratio1_1.box_img.fl.mr10 \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(13) \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(19) \u003E ul \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h2 \u003E span"
    },{
      "function":"_cn",
      "arg0":["macro",52],
      "arg1":"sport.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Sepakbola"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"hot.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"news.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"inet.detik.com"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"div.tengah ul.list_berita_1 li h3 a"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"finance.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"oto.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"travel.detik.com"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#detikcontent \u003E div \u003E #equalize \u003E div \u003E div \u003E ul \u003E li \u003E h3 \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"food.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"health.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"wolipop.detik.com"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#newsfeed-container \u003E article \u003E a \u003E div.text \u003E h2"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"m.detik.com"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#newsfeed-container \u003E article \u003E a \u003E div.ratio1_1.box_img \u003E div \u003E img"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"div.tengah ul.list_berita_1 li img"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"m.detik.com\/news\/"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"kanal_detik"
    },{
      "function":"_eq",
      "arg0":["macro",63],
      "arg1":"\/news"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(24) \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(21) \u003E ul \u003E li \u003E article \u003E a \u003E div.ratio1_1.box_img.fl.mr10 \u003E div"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(25) \u003E ul \u003E li \u003E article \u003E a \u003E div.title_ln \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div:nth-child(25) \u003E div \u003E a"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.sticky \u003E  div.boxlr.mb15.mpop \u003E div#box-pop2 \u003E ul \u003E li \u003E article \u003E a \u003E div.title_lnf \u003E h2 \u003E span"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.sticky.is_stuck \u003E  div.boxlr.mb15.mpop \u003E div#box-pop2 \u003E div.a-center \u003E a.btn_more"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.r_content \u003E div.sticky.is_stuck \u003E div.boxlr.mb15.mpop \u003E div#box-com2 \u003E ul \u003E li \u003E article \u003E a \u003E div.title_lnf \u003E h2"
    },{
      "function":"_eq",
      "arg0":["macro",55],
      "arg1":"Formula 1"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"body \u003E div.container \u003E div.content \u003E div.l_content \u003E div.boxlr.mb15.mpop \u003E div#box-pop \u003E ul \u003E li \u003E article \u003E a"
    },{
      "function":"_eq",
      "arg0":["macro",59],
      "arg1":"gaTriggerPageview"
    },{
      "function":"_re",
      "arg0":["macro",15],
      "arg1":"gaTriggerPageview"
    },{
      "function":"_re",
      "arg0":["macro",53],
      "arg1":"(.*)[0-9]{1,7}(.*)"
    },{
      "function":"_eq",
      "arg0":["macro",2],
      "arg1":"true"
    },{
      "function":"_eq",
      "arg0":["macro",3],
      "arg1":"true"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"20detik"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"livestreaming"
    },{
      "function":"_eq",
      "arg0":["macro",52],
      "arg1":"20.detik.com"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"gtm.dom"
    },{
      "function":"_css",
      "arg0":["macro",58],
      "arg1":"framebardc_ga"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"gtm.linkClick"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_442($|,)))"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"scrollTracking"
    },{
      "function":"_re",
      "arg0":["macro",53],
      "arg1":"\\.(pdf|xlsx|png|docx|js|gif|css|html|php)$"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_490($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",15],
      "arg1":"livestreaming20d"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"breakingnews20detik"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"submit_search_gadget"
    },{
      "function":"_eq",
      "arg0":["macro",79],
      "arg1":"undefined"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"customSearch"
    },{
      "function":"_cn",
      "arg0":["macro",51],
      "arg1":"autocompletegadget"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"gtm_button_detikcom"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"gtm_button_20detik"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"gtm_button_pasangmata"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"gtm_button_cnn indonesia"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#poin \u003E g \u003E image"
    },{
      "function":"_eq",
      "arg0":["macro",58],
      "arg1":"btn btn_blue gtm_btn_trivia_mulai"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#gtm_btn_trivia \u003E span.content.lqd.imgLiquid_bgSize.imgLiquid_ready"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"gtm_btn_menu_game"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#gtm_btn_dribble \u003E span.content.lqd.imgLiquid_bgSize.imgLiquid_ready"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#gtm_btn_penalty \u003E span.content.lqd.imgLiquid_bgSize.imgLiquid_ready"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"gtm_btn_petunjuk_permainan"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#content \u003E div \u003E div.text_center.mt10 \u003E a"
    },{
      "function":"_cn",
      "arg0":["macro",53],
      "arg1":"?"
    },{
      "function":"_re",
      "arg0":["macro",15],
      "arg1":"livetranstv"
    },{
      "function":"_re",
      "arg0":["macro",15],
      "arg1":"livetrans7"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"imagetranstv"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"imagetrans7"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"imagecnntv"
    },{
      "function":"_eq",
      "arg0":["macro",51],
      "arg1":"imagecnbctv"
    },{
      "function":"_eq",
      "arg0":["macro",58],
      "arg1":"livetv"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"EventVirtualPageview"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"VirtualPageview"
    },{
      "function":"_eq",
      "arg0":["macro",59],
      "arg1":"usersession"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"usersession"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_cb_mostPopular_list.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_597($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp.*_cb_mos.*opular.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_598($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_cb_mostcommented_list.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_599($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp.*_cb_lifestyle.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_600($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_cb_lifestyle_list.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_601($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_wm_cnn.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_602($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_wm_cnbc.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_603($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_cb_detikx_logo.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_604($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp.*_cb_.*x_.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_605($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_cb_videoTerpopuler_more.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_606($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_cb_videoTerpopuler_hl.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_607($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp_cb_videoTerpopuler_list.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_608($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*wp.*_videoRekomendasi.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_612($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp.*_btn_artikelLainya.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_613($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp.*_cb_fokus.*"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_614($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp.*_cb_video$"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_615($|,)))"
    },{
      "function":"_re",
      "arg0":["macro",57],
      "arg1":".*tag_from=wp.*_cb_video_more"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_616($|,)))"
    },{
      "function":"_cn",
      "arg0":["macro",59],
      "arg1":"panel tracking"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"panel tracking"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#run.btn"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"#preview.btn"
    },{
      "function":"_css",
      "arg0":["macro",0],
      "arg1":"div.zakat-hasil \u003E div \u003E a"
    },{
      "function":"_cn",
      "arg0":["macro",57],
      "arg1":"www.bawaberkah.org\/campaign\/zakat"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_630($|,)))"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"fb"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"icon-tw"
    },{
      "function":"_cn",
      "arg0":["macro",58],
      "arg1":"icon-in"
    },{
      "function":"_re",
      "arg0":["macro",53],
      "arg1":".*"
    },{
      "function":"_gt",
      "arg0":["macro",8],
      "arg1":"4"
    },{
      "function":"_eq",
      "arg0":["macro",8],
      "arg1":"false"
    },{
      "function":"_eq",
      "arg0":["macro",97],
      "arg1":"null"
    },{
      "function":"_eq",
      "arg0":["macro",15],
      "arg1":"gtm.scrollDepth"
    },{
      "function":"_re",
      "arg0":["macro",72],
      "arg1":"(^$|((^|,)2264312_565($|,)))"
    }],
  "rules":[
    [["if",0],["add",0,268]],
    [["if",1],["add",0,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629,630,631,632,633,634,635,636,637,638,639,640,641,642,643,644,645,646,647,648,649,652,656]],
    [["if",2,3,5],["unless",4],["add",1]],
    [["if",3,5,6],["unless",4],["add",2]],
    [["if",3,5,7],["unless",4],["add",3]],
    [["if",3,5,8],["unless",4],["add",4]],
    [["if",3,5,9],["unless",4],["add",5]],
    [["if",3,5,10],["unless",4],["add",6]],
    [["if",3,5,11],["unless",4],["add",7]],
    [["if",3,5,12],["unless",4],["add",8]],
    [["if",3,5,13],["unless",4],["add",9]],
    [["if",3,5,14],["unless",4],["add",10]],
    [["if",3,5,15],["unless",4],["add",11]],
    [["if",3,5,16],["unless",4],["add",12]],
    [["if",3,5,17],["unless",4],["add",13]],
    [["if",3,5,18],["unless",4],["add",14]],
    [["if",3,5,19],["unless",4],["add",15]],
    [["if",3,5,20],["unless",4],["add",16]],
    [["if",3,5,21],["unless",4],["add",17]],
    [["if",3,5,22],["unless",4],["add",18]],
    [["if",3,5,23],["unless",4],["add",19]],
    [["if",3,5,24],["unless",4],["add",20]],
    [["if",3,5,25],["unless",4],["add",21]],
    [["if",3,5,26],["unless",4],["add",22]],
    [["if",3,5,27],["unless",4],["add",23]],
    [["if",3,5,28],["unless",4],["add",24]],
    [["if",3,5,29],["unless",4],["add",25]],
    [["if",3,5,30],["unless",4],["add",26]],
    [["if",3,5,31],["unless",4],["add",27]],
    [["if",3,5,32],["unless",4],["add",28]],
    [["if",3,5,33],["unless",4],["add",29]],
    [["if",3,5,34],["unless",4],["add",30]],
    [["if",3,5,35],["unless",4],["add",31]],
    [["if",3,5,36],["unless",4],["add",32]],
    [["if",3,5,37],["unless",4],["add",33]],
    [["if",3,5,38],["unless",4],["add",34]],
    [["if",3,5,39],["unless",4],["add",35]],
    [["if",3,5,40],["unless",4],["add",36]],
    [["if",3,5,41],["unless",4],["add",37]],
    [["if",3,5,42],["unless",4],["add",38]],
    [["if",3,5,43],["unless",4],["add",39]],
    [["if",3,5,44],["unless",4],["add",40]],
    [["if",3,5,45],["unless",4],["add",41]],
    [["if",3,5,46],["unless",4],["add",42]],
    [["if",3,5,47],["unless",4],["add",43]],
    [["if",3,5,48],["unless",4],["add",44]],
    [["if",3,5,49],["unless",4],["add",45]],
    [["if",3,5,50],["unless",4],["add",46]],
    [["if",3,5,51],["unless",4],["add",47]],
    [["if",3,5,52],["unless",4],["add",48]],
    [["if",3,5,53],["unless",4],["add",49]],
    [["if",3,5,54],["unless",4],["add",50]],
    [["if",3,5,55],["unless",4],["add",51]],
    [["if",3,5,56],["unless",4],["add",52]],
    [["if",3,5,57,58],["unless",4],["add",53]],
    [["if",3,5,59],["unless",4],["add",54]],
    [["if",3,5,60],["unless",4],["add",55]],
    [["if",3,5,61],["unless",4],["add",56]],
    [["if",3,5,62],["unless",4],["add",57]],
    [["if",3,5,63],["unless",4],["add",58]],
    [["if",3,5,64],["unless",4],["add",59]],
    [["if",3,5,65],["unless",4],["add",60]],
    [["if",3,5,66],["unless",4],["add",61]],
    [["if",3,5,67],["unless",4],["add",62]],
    [["if",3,5,68],["unless",4],["add",63]],
    [["if",3,5,69],["unless",4],["add",64]],
    [["if",3,5,70],["unless",4],["add",65]],
    [["if",3,5,71],["unless",4],["add",66]],
    [["if",3,5,72,73],["unless",4],["add",67]],
    [["if",3,5,74],["unless",4],["add",68]],
    [["if",3,5,75],["unless",4],["add",69]],
    [["if",3,5,76],["unless",4],["add",70]],
    [["if",3,5,77],["unless",4],["add",71]],
    [["if",3,5,78],["unless",4],["add",72]],
    [["if",3,5,79],["unless",4],["add",73]],
    [["if",3,5,80],["unless",4],["add",74]],
    [["if",3,5,81],["unless",4],["add",75]],
    [["if",3,5,82],["unless",4],["add",76]],
    [["if",3,5,83],["unless",4],["add",77]],
    [["if",3,5,84],["unless",4],["add",78]],
    [["if",85,86],["add",79,233]],
    [["if",3,5,87],["unless",4],["add",80]],
    [["if",3,5,88],["unless",4],["add",81]],
    [["if",3,5,89],["unless",4],["add",82]],
    [["if",3,5,90],["unless",4],["add",83]],
    [["if",3,5,91],["unless",4],["add",84]],
    [["if",3,5,92],["unless",4],["add",85]],
    [["if",3,5,93],["unless",4],["add",86]],
    [["if",3,5,94],["unless",4],["add",87]],
    [["if",3,5,95],["unless",4],["add",88]],
    [["if",3,5,96],["unless",4],["add",89]],
    [["if",3,5,97],["unless",4],["add",90]],
    [["if",3,5,98],["unless",4],["add",91]],
    [["if",3,5,99],["unless",4],["add",92]],
    [["if",3,5,100],["unless",4],["add",93]],
    [["if",3,5,101],["unless",4],["add",94]],
    [["if",3,5,102],["unless",4],["add",95]],
    [["if",3,5,103],["unless",4],["add",96]],
    [["if",3,5,104],["unless",4],["add",97]],
    [["if",3,5,105],["unless",4],["add",98]],
    [["if",3,5,106],["unless",4],["add",99]],
    [["if",3,5,107],["unless",4],["add",100]],
    [["if",3,5,108,109],["unless",110],["add",101]],
    [["if",3,5,111],["unless",4],["add",102]],
    [["if",3,5,112,113],["unless",4],["add",103]],
    [["if",3,5,114],["unless",4],["add",104]],
    [["if",3,5,115],["unless",4],["add",105]],
    [["if",3,5,116],["unless",4],["add",106]],
    [["if",3,5,117],["unless",4],["add",107]],
    [["if",3,5,118],["unless",4],["add",108]],
    [["if",3,5,119],["unless",4],["add",109]],
    [["if",3,5,120],["unless",4],["add",110]],
    [["if",3,5,121],["unless",4],["add",111]],
    [["if",3,5,122],["unless",4],["add",112]],
    [["if",3,5,123,124],["unless",4],["add",113]],
    [["if",3,5,125],["unless",4],["add",114]],
    [["if",3,5,126],["unless",4],["add",115]],
    [["if",3,4,5,127],["add",116]],
    [["if",2,3,4,5],["add",117]],
    [["if",3,4,5,6],["add",118]],
    [["if",3,4,5,35],["add",119]],
    [["if",3,4,5,128],["add",120]],
    [["if",3,4,5,129],["add",121]],
    [["if",3,4,5,130],["add",122]],
    [["if",3,4,5,131],["add",123]],
    [["if",5,126,132],["unless",4],["add",124]],
    [["if",2,5,132],["unless",4],["add",125]],
    [["if",5,12,132],["unless",4],["add",126]],
    [["if",5,56,132],["unless",4],["add",127]],
    [["if",3,4,5,133],["add",128]],
    [["if",3,4,5,134],["add",129]],
    [["if",3,4,5,135],["add",130]],
    [["if",3,4,5,136],["add",131]],
    [["if",3,4,5,137],["add",132]],
    [["if",3,4,5,138],["add",133]],
    [["if",3,4,5,139],["add",134]],
    [["if",3,4,5,140],["add",135]],
    [["if",3,4,5,141],["add",136]],
    [["if",3,4,5,142],["add",137]],
    [["if",3,4,5,143],["add",138]],
    [["if",3,4,5,144],["add",139]],
    [["if",3,4,5,145],["add",140]],
    [["if",3,4,5,146],["add",141]],
    [["if",3,4,5,147],["add",142]],
    [["if",4,5,148,149],["add",143]],
    [["if",3,4,5,28],["add",144]],
    [["if",3,4,5,23],["add",145]],
    [["if",3,4,5,29],["add",146]],
    [["if",3,4,5,32],["add",147]],
    [["if",3,4,5,8],["add",148]],
    [["if",5,28,132],["unless",4],["add",149]],
    [["if",5,23,132],["unless",4],["add",150]],
    [["if",5,29,132],["unless",4],["add",151]],
    [["if",5,32,132],["unless",4],["add",152]],
    [["if",5,7,132],["unless",4],["add",153]],
    [["if",5,8,132],["unless",4],["add",154]],
    [["if",5,15,132],["unless",4],["add",155]],
    [["if",5,22,132],["unless",4],["add",156]],
    [["if",5,20,132],["unless",4],["add",157]],
    [["if",5,16,132],["unless",4],["add",158]],
    [["if",5,10,132],["unless",4],["add",159]],
    [["if",5,11,132],["unless",4],["add",160]],
    [["if",5,14,132],["unless",4],["add",161]],
    [["if",5,25,132],["unless",4],["add",162]],
    [["if",5,18,132],["unless",4],["add",163]],
    [["if",5,19,132],["unless",4],["add",164]],
    [["if",5,27,132],["unless",4],["add",165]],
    [["if",5,33,132],["unless",4],["add",166]],
    [["if",5,30,132],["unless",4],["add",167]],
    [["if",5,24,132],["unless",4],["add",168]],
    [["if",5,21,132],["unless",4],["add",169]],
    [["if",5,31,132],["unless",4],["add",170]],
    [["if",5,132,150],["unless",4],["add",171]],
    [["if",5,26,132],["unless",4],["add",172]],
    [["if",5,17,132],["unless",4],["add",173]],
    [["if",5,56,151],["unless",4],["add",174]],
    [["if",5,56,152],["unless",4],["add",175]],
    [["if",5,56,153],["unless",4],["add",176]],
    [["if",5,154,155],["unless",4],["add",177]],
    [["if",5,56,156],["unless",4],["add",178]],
    [["if",5,56,157],["unless",4],["add",179]],
    [["if",5,158,159],["unless",4],["add",180]],
    [["if",5,56,160],["unless",4],["add",181]],
    [["if",5,56,161],["unless",4],["add",182]],
    [["if",5,162,163],["unless",4],["add",183]],
    [["if",5,163,164],["unless",4],["add",184]],
    [["if",5,155,165],["unless",4],["add",185]],
    [["if",4,5,162,166],["add",186]],
    [["if",5,163,167,168],["add",187]],
    [["if",3,4,5,169],["add",188]],
    [["if",3,4,5,170],["add",189]],
    [["if",3,4,5,171],["add",190]],
    [["if",3,4,5,172],["add",191]],
    [["if",3,4,5,173],["add",192]],
    [["if",3,4,5,174],["add",193]],
    [["if",3,4,5,175],["add",194]],
    [["if",3,4,5,7],["add",195]],
    [["if",3,4,5,10],["add",196]],
    [["if",3,4,5,11],["add",197]],
    [["if",3,4,5,12],["add",198]],
    [["if",3,4,5,13],["add",199]],
    [["if",3,4,5,14],["add",200]],
    [["if",3,4,5,15],["add",201]],
    [["if",3,4,5,16],["add",202]],
    [["if",3,4,5,17],["add",203]],
    [["if",3,4,5,18],["add",204]],
    [["if",3,4,5,19],["add",205]],
    [["if",3,4,5,20],["add",206]],
    [["if",3,4,5,21],["add",207]],
    [["if",3,4,5,22],["add",208]],
    [["if",3,4,5,24],["add",209]],
    [["if",3,4,5,25],["add",210]],
    [["if",3,4,5,26],["add",211]],
    [["if",3,4,5,27],["add",212]],
    [["if",3,4,5,30],["add",213]],
    [["if",3,4,5,31],["add",214]],
    [["if",3,4,5,33],["add",215]],
    [["if",3,4,5,60],["add",216]],
    [["if",3,4,5,34],["add",217]],
    [["if",3,4,5,36],["add",218]],
    [["if",3,4,5,37],["add",219]],
    [["if",3,4,5,176],["add",220]],
    [["if",3,4,5,9],["add",221]],
    [["if",3,4,5,39],["add",222]],
    [["if",3,4,5,40],["add",223]],
    [["if",3,4,5,41],["add",224]],
    [["if",3,4,5,42],["add",225]],
    [["if",3,4,5,43],["add",226]],
    [["if",3,4,5,44],["add",227]],
    [["if",3,4,5,45],["add",228]],
    [["if",3,4,5,46],["add",229]],
    [["if",3,4,5,47],["add",230]],
    [["if",3,4,5,48],["add",231]],
    [["if",3,4,5,49],["add",232]],
    [["if",3,5,177],["unless",4],["add",234]],
    [["if",178,179],["add",235]],
    [["if",5,180,181],["add",236,651]],
    [["if",5,180,182],["add",237,650]],
    [["if",183],["add",238]],
    [["if",184],["add",239]],
    [["if",185,186],["add",240,300]],
    [["if",187,188,189],["add",241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267]],
    [["if",190],["add",269]],
    [["if",188,191,192],["add",270]],
    [["if",193],["add",271]],
    [["if",194],["add",272]],
    [["if",5,195],["add",273]],
    [["if",197],["unless",196],["add",274]],
    [["if",5,198],["add",275]],
    [["if",5,199],["add",276,279,280,281]],
    [["if",5,200],["add",277,282,283,284]],
    [["if",5,201],["add",278,285,286,287]],
    [["if",5,202],["add",288,289,290]],
    [["if",5,203],["add",291]],
    [["if",5,204],["add",292]],
    [["if",5,205],["add",293]],
    [["if",5,206],["add",294]],
    [["if",5,207],["add",295]],
    [["if",5,208],["add",296]],
    [["if",5,209],["add",297]],
    [["if",5,210],["add",298]],
    [["if",1,211],["add",299]],
    [["if",212],["add",301]],
    [["if",213],["add",302]],
    [["if",5,214],["add",303]],
    [["if",5,215],["add",304]],
    [["if",5,216],["add",305]],
    [["if",5,217],["add",306]],
    [["if",5,218],["add",307]],
    [["if",219],["add",308]],
    [["if",220],["add",309]],
    [["if",221,222],["add",310]],
    [["if",188,223,224],["add",311]],
    [["if",188,225,226],["add",312]],
    [["if",188,227,228],["add",313]],
    [["if",188,229,230],["add",314]],
    [["if",188,231,232],["add",315]],
    [["if",188,233,234],["add",316]],
    [["if",188,235,236],["add",317]],
    [["if",188,237,238],["add",318]],
    [["if",188,239,240],["add",319]],
    [["if",188,241,242],["add",320]],
    [["if",188,243,244],["add",321]],
    [["if",188,245,246],["add",322]],
    [["if",188,247,248],["add",323]],
    [["if",188,249,250],["add",324]],
    [["if",188,251,252],["add",325]],
    [["if",188,253,254],["add",326]],
    [["if",188,255,256],["add",326]],
    [["if",257,258],["add",327]],
    [["if",5,259],["add",328]],
    [["if",5,260],["add",329]],
    [["if",188,261,262,263],["add",330]],
    [["if",5,264],["add",331]],
    [["if",5,265],["add",332]],
    [["if",5,266],["add",333]],
    [["if",1,267],["add",600]],
    [["if",186,268,269],["add",653]],
    [["if",186],["unless",270],["add",654,655]],
    [["if",271,272],["add",657]]]
},
"runtime":[
[],[]
]



};
var ba,ca=this||self,da=/^[\w+/_-]+[=]{0,2}$/,fa=null;var ha=function(){},ia=function(a){return"function"==typeof a},ja=function(a){return"string"==typeof a},ka=function(a){return"number"==typeof a&&!isNaN(a)},la=function(a){return"[object Array]"==Object.prototype.toString.call(Object(a))},ma=function(a,b){if(Array.prototype.indexOf){var c=a.indexOf(b);return"number"==typeof c?c:-1}for(var d=0;d<a.length;d++)if(a[d]===b)return d;return-1},na=function(a,b){if(a&&la(a))for(var c=0;c<a.length;c++)if(a[c]&&b(a[c]))return a[c]},oa=function(a,b){if(!ka(a)||
!ka(b)||a>b)a=0,b=2147483647;return Math.floor(Math.random()*(b-a+1)+a)},ra=function(a,b){for(var c=new pa,d=0;d<a.length;d++)c.set(a[d],!0);for(var e=0;e<b.length;e++)if(c.get(b[e]))return!0;return!1},sa=function(a,b){for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b(c,a[c])},ta=function(a){return Math.round(Number(a))||0},ua=function(a){return"false"==String(a).toLowerCase()?!1:!!a},va=function(a){var b=[];if(la(a))for(var c=0;c<a.length;c++)b.push(String(a[c]));return b},wa=function(a){return a?
a.replace(/^\s+|\s+$/g,""):""},xa=function(){return(new Date).getTime()},pa=function(){this.prefix="gtm.";this.values={}};pa.prototype.set=function(a,b){this.values[this.prefix+a]=b};pa.prototype.get=function(a){return this.values[this.prefix+a]};pa.prototype.contains=function(a){return void 0!==this.get(a)};
var ya=function(a,b,c){return a&&a.hasOwnProperty(b)?a[b]:c},za=function(a){var b=!1;return function(){if(!b)try{a()}catch(c){}b=!0}},Aa=function(a,b){for(var c in b)b.hasOwnProperty(c)&&(a[c]=b[c])},Ba=function(a){for(var b in a)if(a.hasOwnProperty(b))return!0;return!1},Da=function(a,b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]),c.push.apply(c,b[a[d]]||[]);return c};/*
 jQuery v1.9.1 (c) 2005, 2012 jQuery Foundation, Inc. jquery.org/license. */
var Ea=/\[object (Boolean|Number|String|Function|Array|Date|RegExp)\]/,Fa=function(a){if(null==a)return String(a);var b=Ea.exec(Object.prototype.toString.call(Object(a)));return b?b[1].toLowerCase():"object"},Ga=function(a,b){return Object.prototype.hasOwnProperty.call(Object(a),b)},Ha=function(a){if(!a||"object"!=Fa(a)||a.nodeType||a==a.window)return!1;try{if(a.constructor&&!Ga(a,"constructor")&&!Ga(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}for(var b in a);return void 0===
b||Ga(a,b)},f=function(a,b){var c=b||("array"==Fa(a)?[]:{}),d;for(d in a)if(Ga(a,d)){var e=a[d];"array"==Fa(e)?("array"!=Fa(c[d])&&(c[d]=[]),c[d]=f(e,c[d])):Ha(e)?(Ha(c[d])||(c[d]={}),c[d]=f(e,c[d])):c[d]=e}return c};var v=window,A=document,Ia=navigator,Ja=A.currentScript&&A.currentScript.src,Ka=function(a,b){var c=v[a];v[a]=void 0===c?b:c;return v[a]},La=function(a,b){b&&(a.addEventListener?a.onload=b:a.onreadystatechange=function(){a.readyState in{loaded:1,complete:1}&&(a.onreadystatechange=null,b())})},Ma=function(a,b,c){var d=A.createElement("script");d.type="text/javascript";d.async=!0;d.src=a;La(d,b);c&&(d.onerror=c);var e;if(null===fa)b:{var g=ca.document,h=g.querySelector&&g.querySelector("script[nonce]");
if(h){var k=h.nonce||h.getAttribute("nonce");if(k&&da.test(k)){fa=k;break b}}fa=""}e=fa;e&&d.setAttribute("nonce",e);var l=A.getElementsByTagName("script")[0]||A.body||A.head;l.parentNode.insertBefore(d,l);return d},Na=function(){if(Ja){var a=Ja.toLowerCase();if(0===a.indexOf("https://"))return 2;if(0===a.indexOf("http://"))return 3}return 1},Oa=function(a,b){var c=A.createElement("iframe");c.height="0";c.width="0";c.style.display="none";c.style.visibility="hidden";var d=A.body&&A.body.lastChild||
A.body||A.head;d.parentNode.insertBefore(c,d);La(c,b);void 0!==a&&(c.src=a);return c},Pa=function(a,b,c){var d=new Image(1,1);d.onload=function(){d.onload=null;b&&b()};d.onerror=function(){d.onerror=null;c&&c()};d.src=a;return d},Qa=function(a,b,c,d){a.addEventListener?a.addEventListener(b,c,!!d):a.attachEvent&&a.attachEvent("on"+b,c)},Ra=function(a,b,c){a.removeEventListener?a.removeEventListener(b,c,!1):a.detachEvent&&a.detachEvent("on"+b,c)},D=function(a){v.setTimeout(a,0)},Ua=function(a,b){return a&&
b&&a.attributes&&a.attributes[b]?a.attributes[b].value:null},Va=function(a){var b=a.innerText||a.textContent||"";b&&" "!=b&&(b=b.replace(/^[\s\xa0]+|[\s\xa0]+$/g,""));b&&(b=b.replace(/(\xa0+|\s{2,}|\n|\r\t)/g," "));return b},Wa=function(a){var b=A.createElement("div");b.innerHTML="A<div>"+a+"</div>";b=b.lastChild;for(var c=[];b.firstChild;)c.push(b.removeChild(b.firstChild));return c},Xa=function(a,b,c){c=c||100;for(var d={},e=0;e<b.length;e++)d[b[e]]=!0;for(var g=a,h=0;g&&h<=c;h++){if(d[String(g.tagName).toLowerCase()])return g;
g=g.parentElement}return null},Ya=function(a,b){var c=a[b];c&&"string"===typeof c.animVal&&(c=c.animVal);return c};var Za=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;var $a={},ab=function(a,b){$a[a]=$a[a]||[];$a[a][b]=!0},bb=function(a){for(var b=[],c=$a[a]||[],d=0;d<c.length;d++)c[d]&&(b[Math.floor(d/6)]^=1<<d%6);for(var e=0;e<b.length;e++)b[e]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_".charAt(b[e]||0);return b.join("")};var cb=/:[0-9]+$/,db=function(a,b,c){for(var d=a.split("&"),e=0;e<d.length;e++){var g=d[e].split("=");if(decodeURIComponent(g[0]).replace(/\+/g," ")===b){var h=g.slice(1).join("=");return c?h:decodeURIComponent(h).replace(/\+/g," ")}}},hb=function(a,b,c,d,e){b&&(b=String(b).toLowerCase());if("protocol"===b||"port"===b)a.protocol=fb(a.protocol)||fb(v.location.protocol);"port"===b?a.port=String(Number(a.hostname?a.port:v.location.port)||("http"==a.protocol?80:"https"==a.protocol?443:"")):"host"===b&&
(a.hostname=(a.hostname||v.location.hostname).replace(cb,"").toLowerCase());var g=b,h,k=fb(a.protocol);g&&(g=String(g).toLowerCase());switch(g){case "url_no_fragment":h=gb(a);break;case "protocol":h=k;break;case "host":h=a.hostname.replace(cb,"").toLowerCase();if(c){var l=/^www\d*\./.exec(h);l&&l[0]&&(h=h.substr(l[0].length))}break;case "port":h=String(Number(a.port)||("http"==k?80:"https"==k?443:""));break;case "path":a.pathname||a.hostname||ab("TAGGING",1);h="/"==a.pathname.substr(0,1)?a.pathname:
"/"+a.pathname;var m=h.split("/");0<=ma(d||[],m[m.length-1])&&(m[m.length-1]="");h=m.join("/");break;case "query":h=a.search.replace("?","");e&&(h=db(h,e,void 0));break;case "extension":var n=a.pathname.split(".");h=1<n.length?n[n.length-1]:"";h=h.split("/")[0];break;case "fragment":h=a.hash.replace("#","");break;default:h=a&&a.href}return h},fb=function(a){return a?a.replace(":","").toLowerCase():""},gb=function(a){var b="";if(a&&a.href){var c=a.href.indexOf("#");b=0>c?a.href:a.href.substr(0,c)}return b},
ib=function(a){var b=A.createElement("a");a&&(b.href=a);var c=b.pathname;"/"!==c[0]&&(a||ab("TAGGING",1),c="/"+c);var d=b.hostname.replace(cb,"");return{href:b.href,protocol:b.protocol,host:b.host,hostname:d,pathname:c,search:b.search,hash:b.hash,port:b.port}};var jb=function(a,b,c){for(var d=[],e=String(b||document.cookie).split(";"),g=0;g<e.length;g++){var h=e[g].split("="),k=h[0].replace(/^\s*|\s*$/g,"");if(k&&k==a){var l=h.slice(1).join("=").replace(/^\s*|\s*$/g,"");l&&c&&(l=decodeURIComponent(l));d.push(l)}}return d},mb=function(a,b,c,d){var e=kb(a,d);if(1===e.length)return e[0].id;if(0!==e.length){e=lb(e,function(g){return g.Cb},b);if(1===e.length)return e[0].id;e=lb(e,function(g){return g.Ua},c);return e[0]?e[0].id:void 0}};
function nb(a,b,c){var d=document.cookie;document.cookie=a;var e=document.cookie;return d!=e||void 0!=c&&0<=jb(b,e).indexOf(c)}
var rb=function(a,b,c,d,e,g){d=d||"auto";var h={path:c||"/"};e&&(h.expires=e);"none"!==d&&(h.domain=d);var k;a:{var l=b,m;if(void 0==l)m=a+"=deleted; expires="+(new Date(0)).toUTCString();else{g&&(l=encodeURIComponent(l));var n=l;n&&1200<n.length&&(n=n.substring(0,1200));l=n;m=a+"="+l}var q=void 0,t=void 0,p;for(p in h)if(h.hasOwnProperty(p)){var r=h[p];if(null!=r)switch(p){case "secure":r&&(m+="; secure");break;case "domain":q=r;break;default:"path"==p&&(t=r),"expires"==p&&r instanceof Date&&(r=
r.toUTCString()),m+="; "+p+"="+r}}if("auto"===q){for(var u=ob(),w=0;w<u.length;++w){var y="none"!=u[w]?u[w]:void 0;if(!qb(y,t)&&nb(m+(y?"; domain="+y:""),a,l)){k=!0;break a}}k=!1}else q&&"none"!=q&&(m+="; domain="+q),k=!qb(q,t)&&nb(m,a,l)}return k};function lb(a,b,c){for(var d=[],e=[],g,h=0;h<a.length;h++){var k=a[h],l=b(k);l===c?d.push(k):void 0===g||l<g?(e=[k],g=l):l===g&&e.push(k)}return 0<d.length?d:e}
function kb(a,b){for(var c=[],d=jb(a),e=0;e<d.length;e++){var g=d[e].split("."),h=g.shift();if(!b||-1!==b.indexOf(h)){var k=g.shift();k&&(k=k.split("-"),c.push({id:g.join("."),Cb:1*k[0]||1,Ua:1*k[1]||1}))}}return c}
var sb=/^(www\.)?google(\.com?)?(\.[a-z]{2})?$/,tb=/(^|\.)doubleclick\.net$/i,qb=function(a,b){return tb.test(document.location.hostname)||"/"===b&&sb.test(a)},ob=function(){var a=[],b=document.location.hostname.split(".");if(4===b.length){var c=b[b.length-1];if(parseInt(c,10).toString()===c)return["none"]}for(var d=b.length-2;0<=d;d--)a.push(b.slice(d).join("."));a.push("none");return a};
var ub=[],vb={"\x00":"&#0;",'"':"&quot;","&":"&amp;","'":"&#39;","<":"&lt;",">":"&gt;","\t":"&#9;","\n":"&#10;","\x0B":"&#11;","\f":"&#12;","\r":"&#13;"," ":"&#32;","-":"&#45;","/":"&#47;","=":"&#61;","`":"&#96;","\u0085":"&#133;","\u00a0":"&#160;","\u2028":"&#8232;","\u2029":"&#8233;"},wb=function(a){return vb[a]},xb=/[\x00\x22\x26\x27\x3c\x3e]/g;var Bb=/[\x00\x08-\x0d\x22\x26\x27\/\x3c-\x3e\\\x85\u2028\u2029]/g,Cb={"\x00":"\\x00","\b":"\\x08","\t":"\\t","\n":"\\n","\x0B":"\\x0b",
"\f":"\\f","\r":"\\r",'"':"\\x22","&":"\\x26","'":"\\x27","/":"\\/","<":"\\x3c","=":"\\x3d",">":"\\x3e","\\":"\\\\","\u0085":"\\x85","\u2028":"\\u2028","\u2029":"\\u2029",$:"\\x24","(":"\\x28",")":"\\x29","*":"\\x2a","+":"\\x2b",",":"\\x2c","-":"\\x2d",".":"\\x2e",":":"\\x3a","?":"\\x3f","[":"\\x5b","]":"\\x5d","^":"\\x5e","{":"\\x7b","|":"\\x7c","}":"\\x7d"},Db=function(a){return Cb[a]};
ub[8]=function(a){if(null==a)return" null ";switch(typeof a){case "boolean":case "number":return" "+a+" ";default:return"'"+String(String(a)).replace(Bb,Db)+"'"}};var Lb=/[\x00- \x22\x27-\x29\x3c\x3e\\\x7b\x7d\x7f\x85\xa0\u2028\u2029\uff01\uff03\uff04\uff06-\uff0c\uff0f\uff1a\uff1b\uff1d\uff1f\uff20\uff3b\uff3d]/g,Mb={"\x00":"%00","\u0001":"%01","\u0002":"%02","\u0003":"%03","\u0004":"%04","\u0005":"%05","\u0006":"%06","\u0007":"%07","\b":"%08","\t":"%09","\n":"%0A","\x0B":"%0B","\f":"%0C","\r":"%0D","\u000e":"%0E","\u000f":"%0F","\u0010":"%10",
"\u0011":"%11","\u0012":"%12","\u0013":"%13","\u0014":"%14","\u0015":"%15","\u0016":"%16","\u0017":"%17","\u0018":"%18","\u0019":"%19","\u001a":"%1A","\u001b":"%1B","\u001c":"%1C","\u001d":"%1D","\u001e":"%1E","\u001f":"%1F"," ":"%20",'"':"%22","'":"%27","(":"%28",")":"%29","<":"%3C",">":"%3E","\\":"%5C","{":"%7B","}":"%7D","\u007f":"%7F","\u0085":"%C2%85","\u00a0":"%C2%A0","\u2028":"%E2%80%A8","\u2029":"%E2%80%A9","\uff01":"%EF%BC%81","\uff03":"%EF%BC%83","\uff04":"%EF%BC%84","\uff06":"%EF%BC%86",
"\uff07":"%EF%BC%87","\uff08":"%EF%BC%88","\uff09":"%EF%BC%89","\uff0a":"%EF%BC%8A","\uff0b":"%EF%BC%8B","\uff0c":"%EF%BC%8C","\uff0f":"%EF%BC%8F","\uff1a":"%EF%BC%9A","\uff1b":"%EF%BC%9B","\uff1d":"%EF%BC%9D","\uff1f":"%EF%BC%9F","\uff20":"%EF%BC%A0","\uff3b":"%EF%BC%BB","\uff3d":"%EF%BC%BD"},Nb=function(a){return Mb[a]};ub[16]=function(a){return a};var Pb=[],Qb=[],Rb=[],Sb=[],Ub=[],Vb={},Wb,Xb,Yb,Zb=function(a,b){var c={};c["function"]="__"+a;for(var d in b)b.hasOwnProperty(d)&&(c["vtp_"+d]=b[d]);return c},$b=function(a,b){var c=a["function"];if(!c)throw Error("Error: No function name given for function call.");var d=!!Vb[c],e={},g;for(g in a)a.hasOwnProperty(g)&&0===g.indexOf("vtp_")&&(e[d?g:g.substr(4)]=a[g]);return d?Vb[c](e):(void 0)(c,e,b)},bc=function(a,b,c){c=c||[];var d={},e;for(e in a)a.hasOwnProperty(e)&&(d[e]=ac(a[e],b,c));return d},
cc=function(a){var b=a["function"];if(!b)throw"Error: No function name given for function call.";var c=Vb[b];return c?c.priorityOverride||0:0},ac=function(a,b,c){if(la(a)){var d;switch(a[0]){case "function_id":return a[1];case "list":d=[];for(var e=1;e<a.length;e++)d.push(ac(a[e],b,c));return d;case "macro":var g=a[1];if(c[g])return;var h=Pb[g];if(!h||b.vc(h))return;c[g]=!0;try{var k=bc(h,b,c);k.vtp_gtmEventId=b.id;d=$b(k,b);Yb&&(d=Yb.pf(d,k))}catch(w){b.Md&&b.Md(w,Number(g)),d=!1}c[g]=!1;return d;
case "map":d={};for(var l=1;l<a.length;l+=2)d[ac(a[l],b,c)]=ac(a[l+1],b,c);return d;case "template":d=[];for(var m=!1,n=1;n<a.length;n++){var q=ac(a[n],b,c);Xb&&(m=m||q===Xb.rb);d.push(q)}return Xb&&m?Xb.sf(d):d.join("");case "escape":d=ac(a[1],b,c);if(Xb&&la(a[1])&&"macro"===a[1][0]&&Xb.Uf(a))return Xb.dg(d);d=String(d);for(var t=2;t<a.length;t++)ub[a[t]]&&(d=ub[a[t]](d));return d;case "tag":var p=a[1];if(!Sb[p])throw Error("Unable to resolve tag reference "+p+".");return d={zd:a[2],index:p};case "zb":var r=
{arg0:a[2],arg1:a[3],ignore_case:a[5]};r["function"]=a[1];var u=dc(r,b,c);a[4]&&(u=!u);return u;default:throw Error("Attempting to expand unknown Value type: "+a[0]+".");}}return a},dc=function(a,b,c){try{return Wb(bc(a,b,c))}catch(d){JSON.stringify(a)}return null};var ec=function(){var a=function(b){return{toString:function(){return b}}};return{Wc:a("convert_case_to"),Xc:a("convert_false_to"),Yc:a("convert_null_to"),Zc:a("convert_true_to"),$c:a("convert_undefined_to"),fa:a("function"),Ee:a("instance_name"),Fe:a("live_only"),Ge:a("malware_disabled"),He:a("metadata"),Ng:a("original_vendor_template_id"),Ie:a("once_per_event"),pd:a("once_per_load"),qd:a("setup_tags"),rd:a("tag_id"),sd:a("teardown_tags")}}();var fc=null,ic=function(a){function b(q){for(var t=0;t<q.length;t++)d[q[t]]=!0}var c=[],d=[];fc=gc(a);for(var e=0;e<Qb.length;e++){var g=Qb[e],h=hc(g);if(h){for(var k=g.add||[],l=0;l<k.length;l++)c[k[l]]=!0;b(g.block||[])}else null===h&&b(g.block||[])}for(var m=[],n=0;n<Sb.length;n++)c[n]&&!d[n]&&(m[n]=!0);return m},hc=function(a){for(var b=a["if"]||[],c=0;c<b.length;c++){var d=fc(b[c]);if(!d)return null===d?null:!1}for(var e=a.unless||[],g=0;g<e.length;g++){var h=fc(e[g]);if(null===h)return null;
if(h)return!1}return!0},gc=function(a){var b=[];return function(c){void 0===b[c]&&(b[c]=dc(Rb[c],a));return b[c]}};/*
 Copyright (c) 2014 Derek Brans, MIT license https://github.com/krux/postscribe/blob/master/LICENSE. Portions derived from simplehtmlparser, which is licensed under the Apache License, Version 2.0 */
var yc={},zc=null,Ac=Math.random();yc.h="GTM-NG6BTJ";yc.vb="6q1";var Bc={__cl:!0,__ecl:!0,__ehl:!0,__evl:!0,__fal:!0,__fil:!0,__fsl:!0,__hl:!0,__jel:!0,__lcl:!0,__sdl:!0,__tl:!0,__ytl:!0},Cc="www.googletagmanager.com/gtm.js";var Dc=Cc,Ec=null,Fc=null,Gc=null,Hc="//www.googletagmanager.com/a?id="+yc.h+"&cv=514",Ic={},Jc={},Kc=function(){var a=zc.sequence||0;zc.sequence=a+1;return a};var G=function(a,b,c,d){return(2===Lc()||d||"http:"!=v.location.protocol?a:b)+c},Lc=function(){var a=Na(),b;if(1===a)a:{var c=Dc;c=c.toLowerCase();for(var d="https://"+c,e="http://"+c,g=1,h=A.getElementsByTagName("script"),k=0;k<h.length&&100>k;k++){var l=h[k].src;if(l){l=l.toLowerCase();if(0===l.indexOf(e)){b=3;break a}1===g&&0===l.indexOf(d)&&(g=2)}}b=g}else b=a;return b};var Mc=!1;
var Qc=function(){return"&tc="+Sb.filter(function(a){return a}).length},Zc=function(){Rc&&(v.clearTimeout(Rc),Rc=void 0);void 0===Sc||Tc[Sc]&&!Uc||(Vc[Sc]||Wc.Wf()||0>=Xc--?(ab("GTM",1),Vc[Sc]=!0):(Wc.og(),Pa(Yc()),Tc[Sc]=!0,Uc=""))},Yc=function(){var a=Sc;if(void 0===a)return"";var b=bb("GTM"),c=bb("TAGGING");return[$c,Tc[a]?"":"&es=1",ad[a],b?"&u="+b:"",c?"&ut="+c:"",Qc(),Uc,"&z=0"].join("")},bd=function(){return[Hc,"&v=3&t=t","&pid="+oa(),"&rv="+yc.vb].join("")},cd="0.005000">
Math.random(),$c=bd(),dd=function(){$c=bd()},Tc={},Uc="",Sc=void 0,ad={},Vc={},Rc=void 0,Wc=function(a,b){var c=0,d=0;return{Wf:function(){if(c<a)return!1;xa()-d>=b&&(c=0);return c>=a},og:function(){xa()-d>=b&&(c=0);c++;d=xa()}}}(2,1E3),Xc=1E3,ed=function(a,b){if(cd&&!Vc[a]&&Sc!==a){Zc();Sc=a;Uc="";var c;c=0===b.indexOf("gtm.")?encodeURIComponent(b):"*";ad[a]="&e="+c+"&eid="+a;Rc||(Rc=v.setTimeout(Zc,500))}},fd=function(a,b,c){if(cd&&!Vc[a]&&b){a!==Sc&&(Zc(),Sc=a);var d=String(b[ec.fa]||"").replace(/_/g,
"");0===d.indexOf("cvt")&&(d="cvt");var e=c+d;Uc=Uc?Uc+"."+e:"&tr="+e;Rc||(Rc=v.setTimeout(Zc,500));2022<=Yc().length&&Zc()}};var gd={},hd=new pa,id={},jd={},nd={name:"dataLayer",set:function(a,b){f(kd(a,b),id);ld()},get:function(a){return md(a,2)},reset:function(){hd=new pa;id={};ld()}},md=function(a,b){if(2!=b){var c=hd.get(a);if(cd){var d=od(a);c!==d&&ab("GTM",5)}return c}return od(a)},od=function(a,b,c){var d=a.split("."),e=!1,g=void 0;return e?g:qd(d)},qd=function(a){for(var b=id,c=0;c<a.length;c++){if(null===b)return!1;if(void 0===b)break;b=b[a[c]]}return b};
var td=function(a,b){jd.hasOwnProperty(a)||(hd.set(a,b),f(kd(a,b),id),ld())},kd=function(a,b){for(var c={},d=c,e=a.split("."),g=0;g<e.length-1;g++)d=d[e[g]]={};d[e[e.length-1]]=b;return c},ld=function(a){sa(jd,function(b,c){hd.set(b,c);f(kd(b,void 0),id);f(kd(b,c),id);a&&delete jd[b]})},ud=function(a,b,c){gd[a]=gd[a]||{};var d=1!==c?od(b):hd.get(b);"array"===Fa(d)||"object"===Fa(d)?gd[a][b]=f(d):gd[a][b]=d},vd=function(a,b){if(gd[a])return gd[a][b]};var wd=new RegExp(/^(.*\.)?(google|youtube|blogger|withgoogle)(\.com?)?(\.[a-z]{2})?\.?$/),xd={cl:["ecl"],customPixels:["nonGooglePixels"],ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],customScripts:["html","customPixels","nonGooglePixels","nonGoogleScripts","nonGoogleIframes"],nonGooglePixels:[],nonGoogleScripts:["nonGooglePixels"],nonGoogleIframes:["nonGooglePixels"]},yd={cl:["ecl"],customPixels:["customScripts","html"],
ecl:["cl"],ehl:["hl"],hl:["ehl"],html:["customScripts"],customScripts:["html"],nonGooglePixels:["customPixels","customScripts","html","nonGoogleScripts","nonGoogleIframes"],nonGoogleScripts:["customScripts","html"],nonGoogleIframes:["customScripts","html","nonGoogleScripts"]},zd="google customPixels customScripts html nonGooglePixels nonGoogleScripts nonGoogleIframes".split(" ");
var Bd=function(a){var b=md("gtm.whitelist");b&&ab("GTM",9);var c=b&&Da(va(b),xd),d=md("gtm.blacklist");d||(d=md("tagTypeBlacklist"))&&ab("GTM",3);
d?ab("GTM",8):d=[];Ad()&&(d=va(d),d.push("nonGooglePixels","nonGoogleScripts"));0<=ma(va(d),"google")&&ab("GTM",2);var e=d&&Da(va(d),yd),g={};return function(h){var k=h&&h[ec.fa];if(!k||"string"!=typeof k)return!0;k=k.replace(/^_*/,"");if(void 0!==g[k])return g[k];var l=Jc[k]||[],m=a(k);if(b){var n;if(n=m)a:{if(0>ma(c,k))if(l&&0<l.length)for(var q=0;q<l.length;q++){if(0>
ma(c,l[q])){ab("GTM",11);n=!1;break a}}else{n=!1;break a}n=!0}m=n}var t=!1;if(d){var p=0<=ma(e,k);if(p)t=p;else{var r=ra(e,l||[]);r&&ab("GTM",10);t=r}}var u=!m||t;u||!(0<=ma(l,"sandboxedScripts"))||c&&-1!==ma(c,"sandboxedScripts")||(u=ra(e,zd));return g[k]=u}},Ad=function(){return wd.test(v.location&&v.location.hostname)};var Cd={pf:function(a,b){b[ec.Wc]&&"string"===typeof a&&(a=1==b[ec.Wc]?a.toLowerCase():a.toUpperCase());b.hasOwnProperty(ec.Yc)&&null===a&&(a=b[ec.Yc]);b.hasOwnProperty(ec.$c)&&void 0===a&&(a=b[ec.$c]);b.hasOwnProperty(ec.Zc)&&!0===a&&(a=b[ec.Zc]);b.hasOwnProperty(ec.Xc)&&!1===a&&(a=b[ec.Xc]);return a}};var Dd={active:!0,isWhitelisted:function(){return!0}},Ed=function(a){var b=zc.zones;!b&&a&&(b=zc.zones=a());return b};var Fd=!1,Gd=0,Hd=[];function Id(a){if(!Fd){var b=A.createEventObject,c="complete"==A.readyState,d="interactive"==A.readyState;if(!a||"readystatechange"!=a.type||c||!b&&d){Fd=!0;for(var e=0;e<Hd.length;e++)D(Hd[e])}Hd.push=function(){for(var g=0;g<arguments.length;g++)D(arguments[g]);return 0}}}function Jd(){if(!Fd&&140>Gd){Gd++;try{A.documentElement.doScroll("left"),Id()}catch(a){v.setTimeout(Jd,50)}}}var Kd=function(a){Fd?a():Hd.push(a)};var Ld={},Md={},Nd=function(a,b,c,d){if(!Md[a]||Bc[b]||"__zone"===b)return-1;var e={};Ha(d)&&(e=f(d,e));e.id=c;e.status="timeout";return Md[a].tags.push(e)-1},Od=function(a,b,c,d){if(Md[a]){var e=Md[a].tags[b];e&&(e.status=c,e.executionTime=d)}};function Pd(a){for(var b=Ld[a]||[],c=0;c<b.length;c++)b[c]();Ld[a]={push:function(d){d(yc.h,Md[a])}}}
var Sd=function(a,b,c){Md[a]={tags:[]};ia(b)&&Qd(a,b);c&&v.setTimeout(function(){return Pd(a)},Number(c));return Rd(a)},Qd=function(a,b){Ld[a]=Ld[a]||[];Ld[a].push(za(function(){return D(function(){b(yc.h,Md[a])})}))};function Rd(a){var b=0,c=0,d=!1;return{add:function(){c++;return za(function(){b++;d&&b>=c&&Pd(a)})},We:function(){d=!0;b>=c&&Pd(a)}}};var Td=function(){function a(d){return!ka(d)||0>d?0:d}if(!zc._li&&v.performance&&v.performance.timing){var b=v.performance.timing.navigationStart,c=ka(nd.get("gtm.start"))?nd.get("gtm.start"):0;zc._li={cst:a(c-b),cbt:a(Fc-b)}}};var Xd=!1,Yd=function(){return v.GoogleAnalyticsObject&&v[v.GoogleAnalyticsObject]},Zd=!1;
var $d=function(a){v.GoogleAnalyticsObject||(v.GoogleAnalyticsObject=a||"ga");var b=v.GoogleAnalyticsObject;if(v[b])v.hasOwnProperty(b)||ab("GTM",12);else{var c=function(){c.q=c.q||[];c.q.push(arguments)};c.l=Number(new Date);v[b]=c}Td();return v[b]},ae=function(a,b,c,d){b=String(b).replace(/\s+/g,"").split(",");var e=Yd();e(a+"require","linker");e(a+"linker:autoLink",b,c,d)};
var ce=function(){},be=function(){return v.GoogleAnalyticsObject||"ga"},de=!1;var ke=function(a){};function je(a,b){a.containerId=yc.h;var c={type:"GENERIC",value:a};b.length&&(c.trace=b);return c};function le(a,b,c,d){var e=Sb[a],g=me(a,b,c,d);if(!g)return null;var h=ac(e[ec.qd],c,[]);if(h&&h.length){var k=h[0];g=le(k.index,{J:g,O:1===k.zd?b.terminate:g,terminate:b.terminate},c,d)}return g}
function me(a,b,c,d){function e(){if(g[ec.Ge])k();else{var w=bc(g,c,[]),y=Nd(c.id,String(g[ec.fa]),Number(g[ec.rd]),w[ec.He]),x=!1;w.vtp_gtmOnSuccess=function(){if(!x){x=!0;var B=xa()-C;fd(c.id,Sb[a],"5");Od(c.id,y,"success",B);h()}};w.vtp_gtmOnFailure=function(){if(!x){x=!0;var B=xa()-C;fd(c.id,Sb[a],"6");Od(c.id,y,"failure",B);k()}};w.vtp_gtmTagId=g.tag_id;
w.vtp_gtmEventId=c.id;fd(c.id,g,"1");var z=function(B){var E=xa()-C;ke(B);fd(c.id,g,"7");Od(c.id,y,"exception",E);x||(x=!0,k())};var C=xa();try{$b(w,c)}catch(B){z(B)}}}var g=Sb[a],h=b.J,k=b.O,l=b.terminate;if(c.vc(g))return null;var m=ac(g[ec.sd],c,[]);if(m&&m.length){var n=m[0],q=le(n.index,{J:h,O:k,terminate:l},c,d);if(!q)return null;h=q;k=2===n.zd?l:q}if(g[ec.pd]||g[ec.Ie]){var t=g[ec.pd]?Ub:c.zg,p=h,r=k;if(!t[a]){e=za(e);var u=ne(a,t,e);h=u.J;k=u.O}return function(){t[a](p,r)}}return e}
function ne(a,b,c){var d=[],e=[];b[a]=oe(d,e,c);return{J:function(){b[a]=pe;for(var g=0;g<d.length;g++)d[g]()},O:function(){b[a]=qe;for(var g=0;g<e.length;g++)e[g]()}}}function oe(a,b,c){return function(d,e){a.push(d);b.push(e);c()}}function pe(a){a()}function qe(a,b){b()};var te=function(a,b){for(var c=[],d=0;d<Sb.length;d++)if(a.Ta[d]){var e=Sb[d];var g=b.add();try{var h=le(d,{J:g,O:g,terminate:g},a,d);h?c.push({be:d,Ud:cc(e),Af:h}):(re(d,a),g())}catch(l){g()}}b.We();c.sort(se);for(var k=0;k<c.length;k++)c[k].Af();return 0<c.length};function se(a,b){var c,d=b.Ud,e=a.Ud;c=d>e?1:d<e?-1:0;var g;if(0!==c)g=c;else{var h=a.be,k=b.be;g=h>k?1:h<k?-1:0}return g}
function re(a,b){if(!cd)return;var c=function(d){var e=b.vc(Sb[d])?"3":"4",g=ac(Sb[d][ec.qd],b,[]);g&&g.length&&c(g[0].index);fd(b.id,Sb[d],e);var h=ac(Sb[d][ec.sd],b,[]);h&&h.length&&c(h[0].index)};c(a);}
var ue=!1,ve=function(a,b,c,d,e){if("gtm.js"==b){if(ue)return!1;ue=!0}ed(a,b);var g=Sd(a,d,e);ud(a,"event",1);ud(a,"ecommerce",1);ud(a,"gtm");var h={id:a,name:b,vc:Bd(c),Ta:[],zg:[],Md:function(n){ab("GTM",6);ke(n)}};h.Ta=ic(h);var k=te(h,g);"gtm.js"!==b&&"gtm.sync"!==b||ce();if(!k)return k;for(var l=0;l<h.Ta.length;l++)if(h.Ta[l]){var m=
Sb[l];if(m&&!Bc[String(m[ec.fa])])return!0}return!1};var H={Rb:"event_callback",Tb:"event_timeout"};var xe={};var ye=/[A-Z]+/,ze=/\s/,Ae=function(a){if(ja(a)&&(a=wa(a),!ze.test(a))){var b=a.indexOf("-");if(!(0>b)){var c=a.substring(0,b);if(ye.test(c)){for(var d=a.substring(b+1).split("/"),e=0;e<d.length;e++)if(!d[e])return;return{id:a,prefix:c,containerId:c+"-"+d[0],ia:d}}}}},Ce=function(a){for(var b={},c=0;c<a.length;++c){var d=Ae(a[c]);d&&(b[d.id]=d)}Be(b);var e=[];sa(b,function(g,h){e.push(h)});return e};
function Be(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];"AW"===d.prefix&&d.ia[1]&&b.push(d.containerId)}for(var e=0;e<b.length;++e)delete a[b[e]]};var De=null,Ee={},Fe={},Ge,He=function(a,b){var c={event:a};b&&(c.eventModel=f(b),b[H.Rb]&&(c.eventCallback=b[H.Rb]),b[H.Tb]&&(c.eventTimeout=b[H.Tb]));return c};
var Oe={config:function(a){},event:function(a){var b=a[1];if(ja(b)&&!(3<a.length)){var c;
if(2<a.length){if(!Ha(a[2]))return;c=a[2]}var d=He(b,c);return d}},js:function(a){if(2==a.length&&a[1].getTime)return{event:"gtm.js","gtm.start":a[1].getTime()}},policy:function(a){if(3===a.length){var b=a[1],c=a[2];xe[b]||(xe[b]=[]);xe[b].push(c)}},set:function(a){var b;2==a.length&&Ha(a[1])?b=f(a[1]):3==a.length&&ja(a[1])&&(b={},b[a[1]]=a[2]);if(b)return b.eventModel=f(b),b.event="gtag.set",b._clear=!0,b}},Pe={policy:!0};var Qe=function(){var a=!1;return a};var Se=function(a){return Re?A.querySelectorAll(a):null},Te=function(a,b){if(!Re)return null;if(Element.prototype.closest)try{return a.closest(b)}catch(e){return null}var c=Element.prototype.matches||Element.prototype.webkitMatchesSelector||Element.prototype.mozMatchesSelector||Element.prototype.msMatchesSelector||Element.prototype.oMatchesSelector,d=a;if(!A.documentElement.contains(d))return null;do{try{if(c.call(d,b))return d}catch(e){break}d=d.parentElement||d.parentNode}while(null!==d&&1===d.nodeType);
return null},Ue=!1;if(A.querySelectorAll)try{var Ve=A.querySelectorAll(":root");Ve&&1==Ve.length&&Ve[0]==A.documentElement&&(Ue=!0)}catch(a){}var Re=Ue;var bf=function(a){if(af(a))return a;this.Gg=a};bf.prototype.Hf=function(){return this.Gg};var af=function(a){return!a||"object"!==Fa(a)||Ha(a)?!1:"getUntrustedUpdateValue"in a};bf.prototype.getUntrustedUpdateValue=bf.prototype.Hf;var cf=!1,df=[];function ef(){if(!cf){cf=!0;for(var a=0;a<df.length;a++)D(df[a])}}var ff=function(a){cf?D(a):df.push(a)};var gf=[],hf=!1,jf=function(a){return v["dataLayer"].push(a)},kf=function(a){var b=zc["dataLayer"],c=b?b.subscribers:1,d=0;return function(){++d===c&&a()}},mf=function(a){var b=a._clear;sa(a,function(g,h){"_clear"!==g&&(b&&td(g,void 0),td(g,h))});Ec||(Ec=a["gtm.start"]);var c=a.event;if(!c)return!1;var d=a["gtm.uniqueEventId"];d||(d=Kc(),a["gtm.uniqueEventId"]=d,td("gtm.uniqueEventId",d));Gc=c;var e=lf(a);
Gc=null;switch(c){case "gtag.set":e&&ab("GTM",18);break;case "gtm.init":ab("GTM",19),e&&ab("GTM",20)}return e};function lf(a){var b=a.event,c=a["gtm.uniqueEventId"],d,e=zc.zones;d=e?e.checkState(yc.h,c):Dd;return d.active?ve(c,b,d.isWhitelisted,a.eventCallback,a.eventTimeout)?!0:!1:!1}
var nf=function(){for(var a=!1;!hf&&0<gf.length;){hf=!0;delete id.eventModel;ld();var b=gf.shift();if(null!=b){var c=af(b);if(c){var d=b;b=af(d)?d.getUntrustedUpdateValue():void 0;for(var e=["gtm.whitelist","gtm.blacklist","tagTypeBlacklist"],g=0;g<e.length;g++){var h=e[g],k=md(h,1);if(la(k)||Ha(k))k=f(k);jd[h]=k}}try{if(ia(b))try{b.call(nd)}catch(u){}else if(la(b)){var l=b;if(ja(l[0])){var m=
l[0].split("."),n=m.pop(),q=l.slice(1),t=md(m.join("."),2);if(void 0!==t&&null!==t)try{t[n].apply(t,q)}catch(u){}}}else{var p=b;if(p&&("[object Arguments]"==Object.prototype.toString.call(p)||Object.prototype.hasOwnProperty.call(p,"callee"))){a:{if(b.length&&ja(b[0])){var r=Oe[b[0]];if(r&&(!c||!Pe[b[0]])){b=r(b);break a}}b=void 0}if(!b){hf=!1;continue}}a=mf(b)||a}}finally{c&&ld(!0)}}hf=!1}
return!a},of=function(){var a=nf();try{var b=yc.h,c=v["dataLayer"].hide;if(c&&void 0!==c[b]&&c.end){c[b]=!1;var d=!0,e;for(e in c)if(c.hasOwnProperty(e)&&!0===c[e]){d=!1;break}d&&(c.end(),c.end=null)}}catch(g){}return a},pf=function(){var a=Ka("dataLayer",[]),b=Ka("google_tag_manager",{});b=b["dataLayer"]=b["dataLayer"]||{};Kd(function(){b.gtmDom||(b.gtmDom=!0,a.push({event:"gtm.dom"}))});ff(function(){b.gtmLoad||(b.gtmLoad=!0,a.push({event:"gtm.load"}))});b.subscribers=(b.subscribers||
0)+1;var c=a.push;a.push=function(){var d;if(0<zc.SANDBOXED_JS_SEMAPHORE){d=[];for(var e=0;e<arguments.length;e++)d[e]=new bf(arguments[e])}else d=[].slice.call(arguments,0);var g=c.apply(a,d);gf.push.apply(gf,d);if(300<this.length)for(ab("GTM",4);300<this.length;)this.shift();var h="boolean"!==typeof g||g;return nf()&&h};gf.push.apply(gf,a.slice(0));D(of)};var qf;var Mf={};Mf.rb=new String("undefined");
var Nf=function(a){this.resolve=function(b){for(var c=[],d=0;d<a.length;d++)c.push(a[d]===Mf.rb?b:a[d]);return c.join("")}};Nf.prototype.toString=function(){return this.resolve("undefined")};Nf.prototype.valueOf=Nf.prototype.toString;Mf.Je=Nf;Mf.cc={};Mf.sf=function(a){return new Nf(a)};var Of={};Mf.pg=function(a,b){var c=Kc();Of[c]=[a,b];return c};Mf.xd=function(a){var b=a?0:1;return function(c){var d=Of[c];if(d&&"function"===typeof d[b])d[b]();Of[c]=void 0}};Mf.Uf=function(a){for(var b=!1,c=!1,
d=2;d<a.length;d++)b=b||8===a[d],c=c||16===a[d];return b&&c};Mf.dg=function(a){if(a===Mf.rb)return a;var b=Kc();Mf.cc[b]=a;return'google_tag_manager["'+yc.h+'"].macro('+b+")"};Mf.Yf=function(a,b,c){a instanceof Mf.Je&&(a=a.resolve(Mf.pg(b,c)),b=ha);return{sc:a,J:b}};var Pf=function(a,b,c){function d(g,h){var k=g[h];return k}var e={event:b,"gtm.element":a,"gtm.elementClasses":d(a,"className"),"gtm.elementId":a["for"]||Ua(a,"id")||"","gtm.elementTarget":a.formTarget||d(a,"target")||""};c&&(e["gtm.triggers"]=c.join(","));e["gtm.elementUrl"]=(a.attributes&&a.attributes.formaction?a.formAction:"")||a.action||d(a,"href")||a.src||a.code||a.codebase||
"";return e},Qf=function(a){zc.hasOwnProperty("autoEventsSettings")||(zc.autoEventsSettings={});var b=zc.autoEventsSettings;b.hasOwnProperty(a)||(b[a]={});return b[a]},Rf=function(a,b,c){Qf(a)[b]=c},Sf=function(a,b,c,d){var e=Qf(a),g=ya(e,b,d);e[b]=c(g)},Tf=function(a,b,c){var d=Qf(a);return ya(d,b,c)};var Uf=function(){for(var a=Ia.userAgent+(A.cookie||"")+(A.referrer||""),b=a.length,c=v.history.length;0<c;)a+=c--^b++;var d=1,e,g,h;if(a)for(d=0,g=a.length-1;0<=g;g--)h=a.charCodeAt(g),d=(d<<6&268435455)+h+(h<<14),e=d&266338304,d=0!=e?d^e>>21:d;return[Math.round(2147483647*Math.random())^d&2147483647,Math.round(xa()/1E3)].join(".")},Xf=function(a,b,c,d){var e=Vf(b);return mb(a,e,Wf(c),d)},Vf=function(a){if(!a)return 1;a=0===a.indexOf(".")?a.substr(1):a;return a.split(".").length},Wf=function(a){if(!a||
"/"===a)return 1;"/"!==a[0]&&(a="/"+a);"/"!==a[a.length-1]&&(a+="/");return a.split("/").length-1};function Yf(a,b){var c=""+Vf(a),d=Wf(b);1<d&&(c+="-"+d);return c};var Zf=["1"],$f={},dg=function(a,b,c,d){var e=ag(a);$f[e]||bg(e,b,c)||(cg(e,Uf(),b,c,d),bg(e,b,c))};function cg(a,b,c,d,e){var g;g=["1",Yf(d,c),b].join(".");rb(a,g,c,d,0==e?void 0:new Date(xa()+1E3*(void 0==e?7776E3:e)))}function bg(a,b,c){var d=Xf(a,b,c,Zf);d&&($f[a]=d);return d}function ag(a){return(a||"_gcl")+"_au"};var eg=function(){for(var a=[],b=A.cookie.split(";"),c=/^\s*_gac_(UA-\d+-\d+)=\s*(.+?)\s*$/,d=0;d<b.length;d++){var e=b[d].match(c);e&&a.push({Pc:e[1],value:e[2]})}var g={};if(!a||!a.length)return g;for(var h=0;h<a.length;h++){var k=a[h].value.split(".");"1"==k[0]&&3==k.length&&k[1]&&(g[a[h].Pc]||(g[a[h].Pc]=[]),g[a[h].Pc].push({timestamp:k[1],Ef:k[2]}))}return g};function fg(){for(var a=gg,b={},c=0;c<a.length;++c)b[a[c]]=c;return b}function hg(){var a="ABCDEFGHIJKLMNOPQRSTUVWXYZ";a+=a.toLowerCase()+"0123456789-_";return a+"."}
var gg,ig,jg=function(a){gg=gg||hg();ig=ig||fg();for(var b=[],c=0;c<a.length;c+=3){var d=c+1<a.length,e=c+2<a.length,g=a.charCodeAt(c),h=d?a.charCodeAt(c+1):0,k=e?a.charCodeAt(c+2):0,l=g>>2,m=(g&3)<<4|h>>4,n=(h&15)<<2|k>>6,q=k&63;e||(q=64,d||(n=64));b.push(gg[l],gg[m],gg[n],gg[q])}return b.join("")},kg=function(a){function b(l){for(;d<a.length;){var m=a.charAt(d++),n=ig[m];if(null!=n)return n;if(!/^[\s\xa0]*$/.test(m))throw Error("Unknown base64 encoding at char: "+m);}return l}gg=gg||hg();ig=ig||
fg();for(var c="",d=0;;){var e=b(-1),g=b(0),h=b(64),k=b(64);if(64===k&&-1===e)return c;c+=String.fromCharCode(e<<2|g>>4);64!=h&&(c+=String.fromCharCode(g<<4&240|h>>2),64!=k&&(c+=String.fromCharCode(h<<6&192|k)))}};var lg;function mg(a,b){if(!a||b===A.location.hostname)return!1;for(var c=0;c<a.length;c++)if(a[c]instanceof RegExp){if(a[c].test(b))return!0}else if(0<=b.indexOf(a[c]))return!0;return!1}
var qg=function(){var a=ng,b=og,c=pg(),d=function(h){a(h.target||h.srcElement||{})},e=function(h){b(h.target||h.srcElement||{})};if(!c.init){Qa(A,"mousedown",d);Qa(A,"keyup",d);Qa(A,"submit",e);var g=HTMLFormElement.prototype.submit;HTMLFormElement.prototype.submit=function(){b(this);g.call(this)};c.init=!0}},pg=function(){var a=Ka("google_tag_data",{}),b=a.gl;b&&b.decorators||(b={decorators:[]},a.gl=b);return b};var rg=/(.*?)\*(.*?)\*(.*)/,sg=/^https?:\/\/([^\/]*?)\.?cdn\.ampproject\.org\/?(.*)/,tg=/^(?:www\.|m\.|amp\.)+/,ug=/([^?#]+)(\?[^#]*)?(#.*)?/,vg=/(.*?)(^|&)_gl=([^&]*)&?(.*)/,xg=function(a){var b=[],c;for(c in a)if(a.hasOwnProperty(c)){var d=a[c];void 0!==d&&d===d&&null!==d&&"[object Object]"!==d.toString()&&(b.push(c),b.push(jg(String(d))))}var e=b.join("*");return["1",wg(e),e].join("*")},wg=function(a,b){var c=[window.navigator.userAgent,(new Date).getTimezoneOffset(),window.navigator.userLanguage||
window.navigator.language,Math.floor((new Date).getTime()/60/1E3)-(void 0===b?0:b),a].join("*"),d;if(!(d=lg)){for(var e=Array(256),g=0;256>g;g++){for(var h=g,k=0;8>k;k++)h=h&1?h>>>1^3988292384:h>>>1;e[g]=h}d=e}lg=d;for(var l=4294967295,m=0;m<c.length;m++)l=l>>>8^lg[(l^c.charCodeAt(m))&255];return((l^-1)>>>0).toString(36)},zg=function(){return function(a){var b=ib(v.location.href),c=b.search.replace("?",""),d=db(c,"_gl",!0)||"";a.query=yg(d)||{};var e=hb(b,"fragment").match(vg);a.fragment=yg(e&&e[3]||
"")||{}}},yg=function(a){var b;b=void 0===b?3:b;try{if(a){var c;a:{for(var d=a,e=0;3>e;++e){var g=rg.exec(d);if(g){c=g;break a}d=decodeURIComponent(d)}c=void 0}var h=c;if(h&&"1"===h[1]){var k=h[3],l;a:{for(var m=h[2],n=0;n<b;++n)if(m===wg(k,n)){l=!0;break a}l=!1}if(l){for(var q={},t=k?k.split("*"):[],p=0;p<t.length;p+=2)q[t[p]]=kg(t[p+1]);return q}}}}catch(r){}};
function Ag(a,b,c){function d(m){var n=m,q=vg.exec(n),t=n;if(q){var p=q[2],r=q[4];t=q[1];r&&(t=t+p+r)}m=t;var u=m.charAt(m.length-1);m&&"&"!==u&&(m+="&");return m+l}c=void 0===c?!1:c;var e=ug.exec(b);if(!e)return"";var g=e[1],h=e[2]||"",k=e[3]||"",l="_gl="+a;c?k="#"+d(k.substring(1)):h="?"+d(h.substring(1));return""+g+h+k}
function Bg(a,b,c){for(var d={},e={},g=pg().decorators,h=0;h<g.length;++h){var k=g[h];(!c||k.forms)&&mg(k.domains,b)&&(k.fragment?Aa(e,k.callback()):Aa(d,k.callback()))}if(Ba(d)){var l=xg(d);if(c){if(a&&a.action){var m=(a.method||"").toLowerCase();if("get"===m){for(var n=a.childNodes||[],q=!1,t=0;t<n.length;t++){var p=n[t];if("_gl"===p.name){p.setAttribute("value",l);q=!0;break}}if(!q){var r=A.createElement("input");r.setAttribute("type","hidden");r.setAttribute("name","_gl");r.setAttribute("value",
l);a.appendChild(r)}}else if("post"===m){var u=Ag(l,a.action);Za.test(u)&&(a.action=u)}}}else Cg(l,a,!1)}if(!c&&Ba(e)){var w=xg(e);Cg(w,a,!0)}}function Cg(a,b,c){if(b.href){var d=Ag(a,b.href,void 0===c?!1:c);Za.test(d)&&(b.href=d)}}
var ng=function(a){try{var b;a:{for(var c=a,d=100;c&&0<d;){if(c.href&&c.nodeName.match(/^a(?:rea)?$/i)){b=c;break a}c=c.parentNode;d--}b=null}var e=b;if(e){var g=e.protocol;"http:"!==g&&"https:"!==g||Bg(e,e.hostname,!1)}}catch(h){}},og=function(a){try{if(a.action){var b=hb(ib(a.action),"host");Bg(a,b,!0)}}catch(c){}},Dg=function(a,b,c,d){qg();var e={callback:a,domains:b,fragment:"fragment"===c,forms:!!d};pg().decorators.push(e)},Eg=function(){var a=A.location.hostname,b=sg.exec(A.referrer);if(!b)return!1;
var c=b[2],d=b[1],e="";if(c){var g=c.split("/"),h=g[1];e="s"===h?decodeURIComponent(g[2]):decodeURIComponent(h)}else if(d){if(0===d.indexOf("xn--"))return!1;e=d.replace(/-/g,".").replace(/\.\./g,"-")}return a.replace(tg,"")===e.replace(tg,"")},Fg=function(a,b){return!1===a?!1:a||b||Eg()};var Gg={};var Hg=/^\w+$/,Ig=/^[\w-]+$/,Jg=/^~?[\w-]+$/,Kg={aw:"_aw",dc:"_dc",gf:"_gf",ha:"_ha"};function Lg(a){return a&&"string"==typeof a&&a.match(Hg)?a:"_gcl"}var Ng=function(){var a=ib(v.location.href),b=hb(a,"query",!1,void 0,"gclid"),c=hb(a,"query",!1,void 0,"gclsrc"),d=hb(a,"query",!1,void 0,"dclid");if(!b||!c){var e=a.hash.replace("#","");b=b||db(e,"gclid",void 0);c=c||db(e,"gclsrc",void 0)}return Mg(b,c,d)};
function Mg(a,b,c){var d={},e=function(g,h){d[h]||(d[h]=[]);d[h].push(g)};if(void 0!==a&&a.match(Ig))switch(b){case void 0:e(a,"aw");break;case "aw.ds":e(a,"aw");e(a,"dc");break;case "ds":e(a,"dc");break;case "3p.ds":(void 0==Gg.gtm_3pds?0:Gg.gtm_3pds)&&e(a,"dc");break;case "gf":e(a,"gf");break;case "ha":e(a,"ha")}c&&e(c,"dc");return d}
function Og(a,b,c){function d(q,t){var p=Pg(q,e);p&&rb(p,t,h,g,l,!0)}b=b||{};var e=Lg(b.prefix),g=b.domain||"auto",h=b.path||"/",k=void 0==b.Nd?7776E3:b.Nd;c=c||xa();var l=0==k?void 0:new Date(c+1E3*k),m=Math.round(c/1E3),n=function(q){return["GCL",m,q].join(".")};a.aw&&(!0===b.mh?d("aw",n("~"+a.aw[0])):d("aw",n(a.aw[0])));a.dc&&d("dc",n(a.dc[0]));a.gf&&d("gf",n(a.gf[0]));a.ha&&d("ha",n(a.ha[0]))}
var Pg=function(a,b){var c=Kg[a];if(void 0!==c)return b+c},Qg=function(a){var b=a.split(".");return 3!==b.length||"GCL"!==b[0]?0:1E3*(Number(b[1])||0)};function Rg(a){var b=a.split(".");if(3==b.length&&"GCL"==b[0]&&b[1])return b[2]}
var Sg=function(a,b,c,d,e){if(la(b)){var g=Lg(e);Dg(function(){for(var h={},k=0;k<a.length;++k){var l=Pg(a[k],g);if(l){var m=jb(l,A.cookie);m.length&&(h[l]=m.sort()[m.length-1])}}return h},b,c,d)}},Tg=function(a){return a.filter(function(b){return Jg.test(b)})},Ug=function(a){for(var b=["aw","dc"],c=Lg(a&&a.prefix),d={},e=0;e<b.length;e++)Kg[b[e]]&&(d[b[e]]=Kg[b[e]]);sa(d,function(g,h){var k=jb(c+h,A.cookie);if(k.length){var l=k[0],m=Qg(l),n={};n[g]=[Rg(l)];Og(n,a,m)}})};var Vg=/^\d+\.fls\.doubleclick\.net$/;function Wg(a){var b=ib(v.location.href),c=hb(b,"host",!1);if(c&&c.match(Vg)){var d=hb(b,"path").split(a+"=");if(1<d.length)return d[1].split(";")[0].split("?")[0]}}
function Xg(a,b){if("aw"==a||"dc"==a){var c=Wg("gcl"+a);if(c)return c.split(".")}var d=Lg(b);if("_gcl"==d){var e;e=Ng()[a]||[];if(0<e.length)return e}var g=Pg(a,d),h;if(g){var k=[];if(A.cookie){var l=jb(g,A.cookie);if(l&&0!=l.length){for(var m=0;m<l.length;m++){var n=Rg(l[m]);n&&-1===ma(k,n)&&k.push(n)}h=Tg(k)}else h=k}else h=k}else h=[];return h}
var Yg=function(){var a=Wg("gac");if(a)return decodeURIComponent(a);var b=eg(),c=[];sa(b,function(d,e){for(var g=[],h=0;h<e.length;h++)g.push(e[h].Ef);g=Tg(g);g.length&&c.push(d+":"+g.join(","))});return c.join(";")},$g=function(a,b,c,d,e){dg(b,c,d,e);var g=$f[ag(b)],h=Ng().dc||[],k=!1;if(g&&0<h.length){var l=zc.joined_au=zc.joined_au||{},m=b||"_gcl";if(!l[m])for(var n=0;n<h.length;n++){var q="https://adservice.google.com/ddm/regclk",t=q=q+"?gclid="+h[n]+"&auiddc="+g;Ia.sendBeacon&&Ia.sendBeacon(t)||Pa(t);k=l[m]=
!0}}null==a&&(a=k);if(a&&g){var p=ag(b),r=$f[p];r&&cg(p,r,c,d,e)}};var ah;if(3===yc.vb.length)ah="g";else{var bh="G";ah=bh}
var ch={"":"n",UA:"u",AW:"a",DC:"d",G:"e",GF:"f",HA:"h",GTM:ah,OPT:"o"},dh=function(a){var b=yc.h.split("-"),c=b[0].toUpperCase(),d=ch[c]||"i",e=a&&"GTM"===c?b[1]:"OPT"===c?b[1]:"",g;if(3===yc.vb.length){var h=void 0;g="2"+(h||"w")}else g=
"";return g+d+yc.vb+e};var ih=["input","select","textarea"],jh=["button","hidden","image","reset","submit"],kh=function(a){var b=a.tagName.toLowerCase();return!na(ih,function(c){return c===b})||"input"===b&&na(jh,function(c){return c===a.type.toLowerCase()})?!1:!0},lh=function(a){return a.form?a.form.tagName?a.form:A.getElementById(a.form):Xa(a,["form"],100)},mh=function(a,b,c){if(!a.elements)return 0;for(var d=b.getAttribute(c),e=0,g=1;e<a.elements.length;e++){var h=a.elements[e];if(kh(h)){if(h.getAttribute(c)===d)return g;
g++}}return 0};var ph=!!v.MutationObserver,qh=void 0,rh=function(a){if(!qh){var b=function(){var c=A.body;if(c)if(ph)(new MutationObserver(function(){for(var e=0;e<qh.length;e++)D(qh[e])})).observe(c,{childList:!0,subtree:!0});else{var d=!1;Qa(c,"DOMNodeInserted",function(){d||(d=!0,D(function(){d=!1;for(var e=0;e<qh.length;e++)D(qh[e])}))})}};qh=[];A.body?b():D(b)}qh.push(a)};
var Jh=function(){var a=A.body,b=A.documentElement||a&&a.parentElement,c,d;if(A.compatMode&&"BackCompat"!==A.compatMode)c=b?b.clientHeight:0,d=b?b.clientWidth:0;else{var e=function(g,h){return g&&h?Math.min(g,h):Math.max(g,h)};ab("GTM",7);c=e(b?b.clientHeight:0,a?a.clientHeight:0);d=e(b?b.clientWidth:0,a?a.clientWidth:0)}return{width:d,height:c}},Kh=function(a){var b=Jh(),c=b.height,d=b.width,e=a.getBoundingClientRect(),g=e.bottom-e.top,h=e.right-e.left;return g&&h?(1-Math.min((Math.max(0-e.left,
0)+Math.max(e.right-d,0))/h,1))*(1-Math.min((Math.max(0-e.top,0)+Math.max(e.bottom-c,0))/g,1)):0},Lh=function(a){if(A.hidden)return!0;var b=a.getBoundingClientRect();if(b.top==b.bottom||b.left==b.right||!v.getComputedStyle)return!0;var c=v.getComputedStyle(a,null);if("hidden"===c.visibility)return!0;for(var d=a,e=c;d;){if("none"===e.display)return!0;var g=e.opacity,h=e.filter;if(h){var k=h.indexOf("opacity(");0<=k&&(h=h.substring(k+8,h.indexOf(")",k)),"%"==h.charAt(h.length-1)&&(h=h.substring(0,h.length-
1)),g=Math.min(h,g))}if(void 0!==g&&0>=g)return!0;(d=d.parentElement)&&(e=v.getComputedStyle(d,null))}return!1};var Th=v.clearTimeout,Uh=v.setTimeout,K=function(a,b,c){if(Qe()){b&&D(b)}else return Ma(a,b,c)},Vh=function(){return new Date},Wh=function(){return v.location.href},Xh=function(a){return hb(ib(a),"fragment")},Yh=function(a){return gb(ib(a))},Zh=null;
var $h=function(a,b){return md(a,b||2)},ai=function(a,b,c){b&&(a.eventCallback=b,c&&(a.eventTimeout=c));return jf(a)},bi=function(a,b){v[a]=b},L=function(a,b,c){b&&(void 0===v[a]||c&&!v[a])&&(v[a]=b);return v[a]},ci=function(a,b,c){return jb(a,b,void 0===c?!0:!!c)},di=function(a,b,c,d){var e={prefix:a,path:b,domain:c,Nd:d},g=Ng();Og(g,e);Ug(e)},ei=function(a,b,c,d,e){var g=zg(),h=pg();h.data||(h.data={query:{},fragment:{}},g(h.data));var k={},l=h.data;l&&
(Aa(k,l.query),Aa(k,l.fragment));for(var m=Lg(b),n=0;n<a.length;++n){var q=a[n];if(void 0!==Kg[q]){var t=Pg(q,m),p=k[t];if(p){var r=Math.min(Qg(p),xa()),u;b:{for(var w=r,y=jb(t,A.cookie),x=0;x<y.length;++x)if(Qg(y[x])>w){u=!0;break b}u=!1}u||rb(t,p,c,d,0==e?void 0:new Date(r+1E3*(null==e?7776E3:e)),!0)}}}var z={prefix:b,path:c,domain:d};Og(Mg(k.gclid,k.gclsrc),z);},fi=function(a,b,c,d,e){Sg(a,b,c,d,e);},
gi=function(a,b){if(Qe()){b&&D(b)}else Oa(a,b)},hi=function(a){return!!Tf(a,"init",!1)},ii=function(a){Rf(a,"init",!0)},ji=function(a,b,c){var d=(void 0===c?0:c)?"www.googletagmanager.com/gtag/js":Dc;d+="?id="+encodeURIComponent(a)+"&l=dataLayer";b&&sa(b,function(e,g){g&&(d+="&"+e+"="+encodeURIComponent(g))});K(G("https://","http://",d))},ki=function(a,b){var c=a[b];return c};
var mi=Mf.Yf;var ni=new pa,oi=function(a,b){function c(h){var k=ib(h),l=hb(k,"protocol"),m=hb(k,"host",!0),n=hb(k,"port"),q=hb(k,"path").toLowerCase().replace(/\/$/,"");if(void 0===l||"http"==l&&"80"==n||"https"==l&&"443"==n)l="web",n="default";return[l,m,n,q]}for(var d=c(String(a)),e=c(String(b)),g=0;g<d.length;g++)if(d[g]!==e[g])return!1;return!0},pi=function(a){var b=a.arg0,c=a.arg1;if(a.any_of&&la(c)){for(var d=0;d<c.length;d++)if(pi({"function":a["function"],arg0:b,arg1:c[d]}))return!0;return!1}switch(a["function"]){case "_cn":return 0<=
String(b).indexOf(String(c));case "_css":var e;a:{if(b){var g=["matches","webkitMatchesSelector","mozMatchesSelector","msMatchesSelector","oMatchesSelector"];try{for(var h=0;h<g.length;h++)if(b[g[h]]){e=b[g[h]](c);break a}}catch(u){}}e=!1}return e;case "_ew":var k,l;k=String(b);l=String(c);var m=k.length-l.length;return 0<=m&&k.indexOf(l,m)==m;case "_eq":return String(b)==String(c);case "_ge":return Number(b)>=Number(c);case "_gt":return Number(b)>Number(c);case "_lc":var n;n=String(b).split(",");
return 0<=ma(n,String(c));case "_le":return Number(b)<=Number(c);case "_lt":return Number(b)<Number(c);case "_re":var q;var t=a.ignore_case?"i":void 0;try{var p=String(c)+t,r=ni.get(p);r||(r=new RegExp(c,t),ni.set(p,r));q=r.test(b)}catch(u){q=!1}return q;case "_sw":return 0==String(b).indexOf(String(c));case "_um":return oi(b,c)}return!1};var ri=function(a,b){var c=function(){};c.prototype=a.prototype;var d=new c;a.apply(d,Array.prototype.slice.call(arguments,1));return d};var si={},ti=encodeURI,Y=encodeURIComponent,ui=Pa;var vi=function(a,b){if(!a)return!1;var c=hb(ib(a),"host");if(!c)return!1;for(var d=0;b&&d<b.length;d++){var e=b[d]&&b[d].toLowerCase();if(e){var g=c.length-e.length;0<g&&"."!=e.charAt(0)&&(g--,e="."+e);if(0<=g&&c.indexOf(e,g)==g)return!0}}return!1};
var wi=function(a,b,c){for(var d={},e=!1,g=0;a&&g<a.length;g++)a[g]&&a[g].hasOwnProperty(b)&&a[g].hasOwnProperty(c)&&(d[a[g][b]]=a[g][c],e=!0);return e?d:null};si.Vf=function(){var a=!1;return a};var xi=function(){var a=!1;return a};var Xi=function(){var a=v.gaGlobal=v.gaGlobal||{};a.hid=a.hid||oa();return a.hid};var oj=function(a,b,c,d){this.n=a;this.t=b;this.p=c;this.d=d},pj=function(){this.c=1;this.e=[];this.p=null};function qj(a){var b=zc,c=b.gss=b.gss||{};return c[a]=c[a]||new pj}var rj=function(a,b){qj(a).p=b},sj=function(a){var b=qj(a),c=b.p;if(c){var d=b.e,e=[];b.e=[];var g=function(h){for(var k=0;k<h.length;k++)try{var l=h[k];l.d?(l.d=!1,e.push(l)):c(l.n,l.t,l.p)}catch(m){}};g(d);g(e)}};var Cj=window,Dj=document,Ej=function(a){var b=Cj._gaUserPrefs;if(b&&b.ioo&&b.ioo()||a&&!0===Cj["ga-disable-"+a])return!0;try{var c=Cj.external;if(c&&c._gaUserPrefs&&"oo"==c._gaUserPrefs)return!0}catch(g){}for(var d=jb("AMP_TOKEN",Dj.cookie,!0),e=0;e<d.length;e++)if("$OPT_OUT"==d[e])return!0;return Dj.getElementById("__gaOptOutExtension")?!0:!1};var Lj=function(a,b,c){Kj(a);var d=Math.floor(xa()/1E3);qj(a).e.push(new oj(b,d,c,void 0));sj(a)},Mj=function(a,b,c){Kj(a);var d=Math.floor(xa()/1E3);qj(a).e.push(new oj(b,d,c,!0))},Kj=function(a){if(1===qj(a).c&&(qj(a).c=2,!Qe())){var b=encodeURIComponent(a);Ma(("http:"!=v.location.protocol?"https:":"http:")+("//www.googletagmanager.com/gtag/js?id="+b+"&l=dataLayer&cx=c"))}},Oj=function(a,b){},Nj=function(a,
b){},Pj=function(a){return"_"===a.charAt(0)},Qj=function(a){sa(a,function(c){Pj(c)&&delete a[c]});var b=a[H.qb]||{};sa(b,function(c){Pj(c)&&delete b[c]})};var Z={a:{}};
Z.a.sdl=["google"],function(){function a(){return!!(Object.keys(l("horiz.pix")).length||Object.keys(l("horiz.pct")).length||Object.keys(l("vert.pix")).length||Object.keys(l("vert.pct")).length)}function b(x){for(var z=[],C=x.split(","),B=0;B<C.length;B++){var E=Number(C[B]);if(isNaN(E))return[];n.test(C[B])||z.push(E)}return z}function c(){var x=0,z=0;return function(){var C=Jh(),B=C.height;x=Math.max(u.scrollLeft+C.width,x);z=Math.max(u.scrollTop+B,z);return{vf:x,wf:z}}}function d(){p=L("self");
r=p.document;u=r.scrollingElement||r.body&&r.body.parentNode;y=c()}function e(x,z,C,B){var E=l(z),F={},P;for(P in E){F.na=P;if(E.hasOwnProperty(F.na)){var U=Number(F.na);x<U||(ai({event:"gtm.scrollDepth","gtm.scrollThreshold":U,"gtm.scrollUnits":C.toLowerCase(),"gtm.scrollDirection":B,"gtm.triggers":E[F.na].join(",")}),Sf("sdl",z,function(R){return function(T){delete T[R.na];return T}}(F),{}))}F={na:F.na}}}function g(){var x=y(),z=x.vf,C=x.wf,B=z/u.scrollWidth*100,E=C/u.scrollHeight*100;e(z,"horiz.pix",
q.tb,t.od);e(B,"horiz.pct",q.sb,t.od);e(C,"vert.pix",q.tb,t.td);e(E,"vert.pct",q.sb,t.td);Rf("sdl","pending",!1)}function h(){var x=250,z=!1;r.scrollingElement&&r.documentElement&&p.addEventListener&&(x=50,z=!0);var C=0,B=!1,E=function(){B?C=Uh(E,x):(C=0,g(),hi("sdl")&&!a()&&(Ra(p,"scroll",F),Ra(p,"resize",F),Rf("sdl","init",!1)));B=!1},F=function(){z&&y();C?B=!0:(C=Uh(E,x),Rf("sdl","pending",!0))};return F}function k(x,z,C){if(z){var B=b(String(x));Sf("sdl",C,function(E){for(var F=0;F<B.length;F++){var P=
String(B[F]);E.hasOwnProperty(P)||(E[P]=[]);E[P].push(z)}return E},{})}}function l(x){return Tf("sdl",x,{})}function m(x){D(x.vtp_gtmOnSuccess);var z=x.vtp_uniqueTriggerId,C=x.vtp_horizontalThresholdsPixels,B=x.vtp_horizontalThresholdsPercent,E=x.vtp_verticalThresholdUnits,F=x.vtp_verticalThresholdsPixels,P=x.vtp_verticalThresholdsPercent;switch(x.vtp_horizontalThresholdUnits){case q.tb:k(C,z,"horiz.pix");break;case q.sb:k(B,z,"horiz.pct")}switch(E){case q.tb:k(F,z,"vert.pix");break;case q.sb:k(P,
z,"vert.pct")}hi("sdl")?Tf("sdl","pending")||(w||(d(),w=!0),D(function(){return g()})):(d(),w=!0,u&&(ii("sdl"),Rf("sdl","pending",!0),D(function(){g();if(a()){var U=h();Qa(p,"scroll",U);Qa(p,"resize",U)}else Rf("sdl","init",!1)})))}var n=/^\s*$/,q={sb:"PERCENT",tb:"PIXELS"},t={td:"vertical",od:"horizontal"},p,r,u,w=!1,y;(function(x){Z.__sdl=x;Z.__sdl.b="sdl";Z.__sdl.g=!0;Z.__sdl.priorityOverride=0})(function(x){x.vtp_triggerStartOption?m(x):ff(function(){m(x)})})}();

Z.a.jsm=["customScripts"],function(){(function(a){Z.__jsm=a;Z.__jsm.b="jsm";Z.__jsm.g=!0;Z.__jsm.priorityOverride=0})(function(a){if(void 0!==a.vtp_javascript){var b=a.vtp_javascript;try{var c=L("google_tag_manager");return c&&c.e&&c.e(b)}catch(d){}}})}();Z.a.c=["google"],function(){(function(a){Z.__c=a;Z.__c.b="c";Z.__c.g=!0;Z.__c.priorityOverride=0})(function(a){return a.vtp_value})}();
Z.a.d=["google"],function(){(function(a){Z.__d=a;Z.__d.b="d";Z.__d.g=!0;Z.__d.priorityOverride=0})(function(a){var b=null,c=null,d=a.vtp_attributeName;if("CSS"==a.vtp_selectorType){var e=Se(a.vtp_elementSelector);e&&0<e.length&&(b=e[0])}else b=A.getElementById(a.vtp_elementId);b&&(c=d?Ua(b,d):Va(b));return wa(String(b&&c))})}();
Z.a.e=["google"],function(){(function(a){Z.__e=a;Z.__e.b="e";Z.__e.g=!0;Z.__e.priorityOverride=0})(function(a){return String(vd(a.vtp_gtmEventId,"event"))})}();
Z.a.f=["google"],function(){(function(a){Z.__f=a;Z.__f.b="f";Z.__f.g=!0;Z.__f.priorityOverride=0})(function(a){var b=$h("gtm.referrer",1)||A.referrer;return b?a.vtp_component&&"URL"!=a.vtp_component?hb(ib(String(b)),a.vtp_component,a.vtp_stripWww,a.vtp_defaultPages,a.vtp_queryKey):Yh(String(b)):String(b)})}();
Z.a.cl=["google"],function(){function a(b){var c=b.target;if(c){var d=Pf(c,"gtm.click");ai(d)}}(function(b){Z.__cl=b;Z.__cl.b="cl";Z.__cl.g=!0;Z.__cl.priorityOverride=0})(function(b){if(!hi("cl")){var c=L("document");Qa(c,"click",a,!0);ii("cl")}D(b.vtp_gtmOnSuccess)})}();Z.a.k=["google"],function(){(function(a){Z.__k=a;Z.__k.b="k";Z.__k.g=!0;Z.__k.priorityOverride=0})(function(a){return ci(a.vtp_name,$h("gtm.cookie",1),!!a.vtp_decodeCookie)[0]})}();


Z.a.u=["google"],function(){var a=function(b){return{toString:function(){return b}}};(function(b){Z.__u=b;Z.__u.b="u";Z.__u.g=!0;Z.__u.priorityOverride=0})(function(b){var c;c=(c=b.vtp_customUrlSource?b.vtp_customUrlSource:$h("gtm.url",1))||Wh();var d=b[a("vtp_component")];if(!d||"URL"==d)return Yh(String(c));var e=ib(String(c)),g;if("QUERY"===d)a:{var h=b[a("vtp_multiQueryKeys").toString()],k=b[a("vtp_queryKey").toString()]||"",l=b[a("vtp_ignoreEmptyQueryParam").toString()],m;m=h?la(k)?k:String(k).replace(/\s+/g,
"").split(","):[String(k)];for(var n=0;n<m.length;n++){var q=hb(e,"QUERY",void 0,void 0,m[n]);if(void 0!=q&&(!l||""!==q)){g=q;break a}}g=void 0}else g=hb(e,d,"HOST"==d?b[a("vtp_stripWww")]:void 0,"PATH"==d?b[a("vtp_defaultPages")]:void 0,void 0);return g})}();
Z.a.v=["google"],function(){(function(a){Z.__v=a;Z.__v.b="v";Z.__v.g=!0;Z.__v.priorityOverride=0})(function(a){var b=a.vtp_name;if(!b||!b.replace)return!1;var c=$h(b.replace(/\\\./g,"."),a.vtp_dataLayerVersion||1);return void 0!==c?c:a.vtp_defaultValue})}();
Z.a.ua=["google"],function(){var a,b=function(c){var d={},e={},g={},h={},k={},l=void 0;if(c.vtp_gaSettings){var m=c.vtp_gaSettings;f(wi(m.vtp_fieldsToSet,"fieldName","value"),e);f(wi(m.vtp_contentGroup,"index","group"),g);f(wi(m.vtp_dimension,"index","dimension"),h);f(wi(m.vtp_metric,"index","metric"),k);c.vtp_gaSettings=null;m.vtp_fieldsToSet=void 0;m.vtp_contentGroup=void 0;m.vtp_dimension=void 0;m.vtp_metric=void 0;var n=f(m);c=f(c,n)}f(wi(c.vtp_fieldsToSet,"fieldName","value"),e);f(wi(c.vtp_contentGroup,
"index","group"),g);f(wi(c.vtp_dimension,"index","dimension"),h);f(wi(c.vtp_metric,"index","metric"),k);var q=$d(c.vtp_functionName);if(ia(q)){var t="",p="";c.vtp_setTrackerName&&"string"==typeof c.vtp_trackerName?""!==c.vtp_trackerName&&(p=c.vtp_trackerName,t=p+"."):(p="gtm"+Kc(),t=p+".");var r={name:!0,clientId:!0,sampleRate:!0,siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,
legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},u={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0,allowAdFeatures:!0},w=function(V){var N=[].slice.call(arguments,0);N[0]=t+N[0];q.apply(window,N)},y=function(V,N){return void 0===N?N:V(N)},x=function(V,N){if(N)for(var aa in N)N.hasOwnProperty(aa)&&w("set",V+aa,N[aa])},z=function(){},C=function(V,N,aa){var qa=0;if(V)for(var Ca in V)if(V.hasOwnProperty(Ca)&&(aa&&r[Ca]||!aa&&void 0===r[Ca])){var Sa=u[Ca]?ua(V[Ca]):V[Ca];"anonymizeIp"!=Ca||Sa||(Sa=void 0);N[Ca]=Sa;qa++}return qa},B={name:p};C(e,B,!0);(function(){})();
q("create",c.vtp_trackingId||d.trackingId,B);w("set","&gtm",dh(!0));c.vtp_enableRecaptcha&&w("require","recaptcha","recaptcha.js");(function(V,N){void 0!==c[N]&&w("set",V,c[N])})("nonInteraction","vtp_nonInteraction");x("contentGroup",g);x("dimension",h);x("metric",k);var E={};C(e,E,!1)&&w("set",E);var F;c.vtp_enableLinkId&&w("require",
"linkid","linkid.js");w("set","hitCallback",function(){var V=e&&e.hitCallback;ia(V)&&V();c.vtp_gtmOnSuccess()});if("TRACK_EVENT"==c.vtp_trackType){c.vtp_enableEcommerce&&(w("require","ec","ec.js"),z());var P={hitType:"event",eventCategory:String(c.vtp_eventCategory||d.category),eventAction:String(c.vtp_eventAction||d.action),eventLabel:y(String,c.vtp_eventLabel||d.label),eventValue:y(ta,c.vtp_eventValue||d.value)};C(F,P,!1);w("send",
P);}else if("TRACK_SOCIAL"==c.vtp_trackType){}else if("TRACK_TRANSACTION"==c.vtp_trackType){}else if("TRACK_TIMING"==
c.vtp_trackType){}else if("DECORATE_LINK"==c.vtp_trackType){}else if("DECORATE_FORM"==c.vtp_trackType){}else if("TRACK_DATA"==c.vtp_trackType){}else{c.vtp_enableEcommerce&&(w("require","ec","ec.js"),z());if(c.vtp_doubleClick||"DISPLAY_FEATURES"==c.vtp_advertisingFeaturesType){var S=
"_dc_gtm_"+String(c.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");w("require","displayfeatures",void 0,{cookieName:S})}if("DISPLAY_FEATURES_WITH_REMARKETING_LISTS"==c.vtp_advertisingFeaturesType){var X="_dc_gtm_"+String(c.vtp_trackingId).replace(/[^A-Za-z0-9-]/g,"");w("require","adfeatures",{cookieName:X})}F?w("send","pageview",F):w("send","pageview");c.vtp_autoLinkDomains&&ae(t,c.vtp_autoLinkDomains,!!c.vtp_useHashAutoLink,!!c.vtp_decorateFormsAutoLink);
}if(!a){var W=c.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js";c.vtp_useInternalVersion&&!c.vtp_useDebugVersion&&(W="internal/"+W);a=!0;var ea=G("https:","http:","//www.google-analytics.com/"+W,e&&e.forceSSL);K(ea,function(){var V=Yd();V&&V.loaded||c.vtp_gtmOnFailure();},
c.vtp_gtmOnFailure)}}else D(c.vtp_gtmOnFailure)};Z.__ua=b;Z.__ua.b="ua";Z.__ua.g=!0;Z.__ua.priorityOverride=0}();



Z.a.opt=["google"],function(){var a,b=function(c){var d={};if(c.vtp_gaSettings){var e=c.vtp_gaSettings;f(wi(e.vtp_fieldsToSet,"fieldName","value"),d);c.vtp_gaSettings=null;e.vtp_fieldsToSet=void 0;var g=f(e);c=f(c,g)||{}}f(wi(c.vtp_fieldsToSet,"fieldName","value"),d);var h=$d(c.vtp_functionName);if(ia(h)){h.r=!0;var k="",l="";c.vtp_setTrackerName&&"string"===typeof c.vtp_trackerName?""!==c.vtp_trackerName&&(l=c.vtp_trackerName,k=l+"."):(l="gtm"+Kc(),k=l+".");var m={name:!0,clientId:!0,sampleRate:!0,
siteSpeedSampleRate:!0,alwaysSendReferrer:!0,allowAnchor:!0,allowLinker:!0,cookieName:!0,cookieDomain:!0,cookieExpires:!0,cookiePath:!0,cookieUpdate:!0,legacyCookieDomain:!0,legacyHistoryImport:!0,storage:!0,useAmpClientId:!0,storeGac:!0},n={allowAnchor:!0,allowLinker:!0,alwaysSendReferrer:!0,anonymizeIp:!0,cookieUpdate:!0,exFatal:!0,forceSSL:!0,javaEnabled:!0,legacyHistoryImport:!0,nonInteraction:!0,useAmpClientId:!0,useBeacon:!0,storeGac:!0},q=function(y,x,z){var C=0,B;for(B in y)if(y.hasOwnProperty(B)&&
(z&&m[B]||!z&&void 0===m[B])){var E=n[B]?ua(y[B]):y[B];"anonymizeIp"!==B||E||(E=void 0);x[B]=E;C++}return C},t={name:l};q(d,t,!0);var p={"&gtm":dh(!0)};q(d,p,!1);var r=encodeURI(G("https:","http:","//www.google-analytics.com/"+(c.vtp_useDebugVersion?"u/analytics_debug.js":"analytics.js"),!!d.forceSSL));h("create",c.vtp_trackingId,t);h(k+"set",p);h(k+"require",c.vtp_optimizeContainerId,{dataLayer:"dataLayer"});h(c.vtp_gtmOnSuccess);h(k+"require","render");a||(a=!0,K(r,function(){return Yd().loaded||
c.vtp_gtmOnFailure()},c.vtp_gtmOnFailure));var u=L("dataLayer"),w=u&&u.hide;w&&w.end&&(w[c.vtp_optimizeContainerId]=!0)}else D(c.vtp_gtmOnFailure)};Z.__opt=b;Z.__opt.b="opt";Z.__opt.g=!0;Z.__opt.priorityOverride=0}();

Z.a.aev=["google"],function(){function a(p,r){var u=!1,w;var y=vd(p,"gtm");if(!y)return;u=!0;w=y[r];u||(w=$h("gtm."+r,1));return w}function b(p,r,u,w){w||(w="element");var y=p+"."+r,x;if(n.hasOwnProperty(y))x=n[y];else{var z=a(p,w);if(z&&(x=u(z),n[y]=x,q.push(y),35<q.length)){var C=q.shift();delete n[C]}}return x}function c(p,r,u){var w=a(p,t[r]);return void 0!==w?w:u}function d(p,r){if(!p)return!1;
var u=e(Wh());la(r)||(r=String(r||"").replace(/\s+/g,"").split(","));for(var w=[u],y=0;y<r.length;y++)if(r[y]instanceof RegExp){if(r[y].test(p))return!1}else{var x=r[y];if(0!=x.length){if(0<=e(p).indexOf(x))return!1;w.push(e(x))}}return!vi(p,w)}function e(p){m.test(p)||(p="http://"+p);return hb(ib(p),"HOST",!0)}function g(p,r,u){switch(p){case "SUBMIT_TEXT":return b(r,"FORM."+p,h,"formSubmitElement")||u;case "LENGTH":return b(r,"FORM."+p,k)||u;case "INTERACTED_FIELD_ID":return l(r,"id",u);case "INTERACTED_FIELD_NAME":return l(r,
"name",u);case "INTERACTED_FIELD_TYPE":return l(r,"type",u);case "INTERACTED_FIELD_POSITION":return a(r,"interactedFormFieldPosition")||u;default:return u}}function h(p){switch(p.tagName.toLowerCase()){case "input":return Ua(p,"value");case "button":return Va(p);default:return null}}function k(p){if("form"===p.tagName.toLowerCase()&&p.elements){for(var r=0,u=0;u<p.elements.length;u++)kh(p.elements[u])&&r++;return r}}function l(p,r,u){var w=a(p,"interactedFormField");return w&&Ua(w,r)||u}var m=/^https?:\/\//i,
n={},q=[],t={ATTRIBUTE:"elementAttribute",CLASSES:"elementClasses",ELEMENT:"element",ID:"elementId",HISTORY_CHANGE_SOURCE:"historyChangeSource",HISTORY_NEW_STATE:"newHistoryState",HISTORY_NEW_URL_FRAGMENT:"newUrlFragment",HISTORY_OLD_STATE:"oldHistoryState",HISTORY_OLD_URL_FRAGMENT:"oldUrlFragment",TARGET:"elementTarget"};(function(p){Z.__aev=p;Z.__aev.b="aev";Z.__aev.g=!0;Z.__aev.priorityOverride=0})(function(p){var r=p.vtp_gtmEventId,u=p.vtp_defaultValue,w=p.vtp_varType;switch(w){case "TAG_NAME":var y=
a(r,"element");return y&&y.tagName||u;case "TEXT":return b(r,w,Va)||u;case "URL":var x;a:{var z=String(a(r,"elementUrl")||u||""),C=ib(z),B=String(p.vtp_component||"URL");switch(B){case "URL":x=z;break a;case "IS_OUTBOUND":x=d(z,p.vtp_affiliatedDomains);break a;default:x=hb(C,B,p.vtp_stripWww,p.vtp_defaultPages,p.vtp_queryKey)}}return x;case "ATTRIBUTE":var E;if(void 0===p.vtp_attribute)E=c(r,w,u);else{var F=p.vtp_attribute,P=a(r,"element");E=P&&Ua(P,F)||u||""}return E;case "MD":var U=p.vtp_mdValue,
R=b(r,"MD",Fh);return U&&R?Ih(R,U)||u:R||u;case "FORM":return g(String(p.vtp_component||"SUBMIT_TEXT"),r,u);default:return c(r,w,u)}})}();
Z.a.gas=["google"],function(){(function(a){Z.__gas=a;Z.__gas.b="gas";Z.__gas.g=!0;Z.__gas.priorityOverride=0})(function(a){var b=f(a),c=b;c[ec.fa]=null;c[ec.Ee]=null;var d=b=c;d.vtp_fieldsToSet=d.vtp_fieldsToSet||[];var e=d.vtp_cookieDomain;void 0!==e&&(d.vtp_fieldsToSet.push({fieldName:"cookieDomain",value:e}),delete d.vtp_cookieDomain);return b})}();

Z.a.remm=["google"],function(){(function(a){Z.__remm=a;Z.__remm.b="remm";Z.__remm.g=!0;Z.__remm.priorityOverride=0})(function(a){for(var b=a.vtp_input,c=a.vtp_map||[],d=a.vtp_fullMatch,e=a.vtp_ignoreCase?"gi":"g",g=0;g<c.length;g++){var h=c[g].key||"";d&&(h="^"+h+"$");var k=new RegExp(h,e);if(k.test(b)){var l=c[g].value;a.vtp_replaceAfterMatch&&(l=String(b).replace(k,l));return l}}return a.vtp_defaultValue})}();




Z.a.paused=[],function(){(function(a){Z.__paused=a;Z.__paused.b="paused";Z.__paused.g=!0;Z.__paused.priorityOverride=0})(function(a){D(a.vtp_gtmOnFailure)})}();

Z.a.html=["customScripts"],function(){function a(d,e,g,h){return function(){try{if(0<e.length){var k=e.shift(),l=a(d,e,g,h);if("SCRIPT"==String(k.nodeName).toUpperCase()&&"text/gtmscript"==k.type){var m=A.createElement("script");m.async=!1;m.type="text/javascript";m.id=k.id;m.text=k.text||k.textContent||k.innerHTML||"";k.charset&&(m.charset=k.charset);var n=k.getAttribute("data-gtmsrc");n&&(m.src=n,La(m,l));d.insertBefore(m,null);n||l()}else if(k.innerHTML&&0<=k.innerHTML.toLowerCase().indexOf("<script")){for(var q=
[];k.firstChild;)q.push(k.removeChild(k.firstChild));d.insertBefore(k,null);a(k,q,l,h)()}else d.insertBefore(k,null),l()}else g()}catch(t){D(h)}}}var c=function(d){if(A.body){var e=
d.vtp_gtmOnFailure,g=mi(d.vtp_html,d.vtp_gtmOnSuccess,e),h=g.sc,k=g.J;if(d.vtp_useIframe){}else d.vtp_supportDocumentWrite?b(h,k,e):a(A.body,Wa(h),k,e)()}else Uh(function(){c(d)},
200)};Z.__html=c;Z.__html.b="html";Z.__html.g=!0;Z.__html.priorityOverride=0}();
Z.a.dbg=["google"],function(){(function(a){Z.__dbg=a;Z.__dbg.b="dbg";Z.__dbg.g=!0;Z.__dbg.priorityOverride=0})(function(){return!1})}();




Z.a.lcl=[],function(){function a(){var c=L("document"),d=0,e=function(g){var h=g.target;if(h&&3!==g.which&&(!g.timeStamp||g.timeStamp!==d)){d=g.timeStamp;h=Xa(h,["a","area"],100);if(!h)return g.returnValue;var k=g.defaultPrevented||!1===g.returnValue,l=Tf("lcl",k?"nv.mwt":"mwt",0),m;m=k?Tf("lcl","nv.ids",[]):Tf("lcl","ids",[]);if(m.length){var n=Pf(h,"gtm.linkClick",m);if(b(g,h,c)&&!k&&l&&h.href){var q=L((ki(h,"target")||"_self").substring(1)),t=!0;if(ai(n,kf(function(){t&&q&&(q.location.href=ki(h,
"href"))}),l))t=!1;else return g.preventDefault&&g.preventDefault(),g.returnValue=!1}else ai(n,function(){},l||2E3);return!0}}};Qa(c,"click",e,!1);Qa(c,"auxclick",e,!1)}function b(c,d,e){if(2===c.which||c.ctrlKey||c.shiftKey||c.altKey||c.metaKey)return!1;var g=ki(d,"href"),h=g.indexOf("#"),k=ki(d,"target");if(k&&"_self"!==k&&"_parent"!==k&&"_top"!==k||0===h)return!1;if(0<h){var l=Yh(g),m=Yh(e.location);return l!==m}return!0}(function(c){Z.__lcl=c;Z.__lcl.b="lcl";Z.__lcl.g=!0;Z.__lcl.priorityOverride=
0})(function(c){var d=void 0===c.vtp_waitForTags?!0:c.vtp_waitForTags,e=void 0===c.vtp_checkValidation?!0:c.vtp_checkValidation,g=Number(c.vtp_waitForTagsTimeout);if(!g||0>=g)g=2E3;var h=c.vtp_uniqueTriggerId||"0";if(d){var k=function(m){return Math.max(g,m)};Sf("lcl","mwt",k,0);e||Sf("lcl","nv.mwt",k,0)}var l=function(m){m.push(h);return m};Sf("lcl","ids",l,[]);e||Sf("lcl","nv.ids",l,[]);hi("lcl")||(a(),ii("lcl"));D(c.vtp_gtmOnSuccess)})}();

var Rj={};Rj.macro=function(a){if(Mf.cc.hasOwnProperty(a))return Mf.cc[a]},Rj.onHtmlSuccess=Mf.xd(!0),Rj.onHtmlFailure=Mf.xd(!1);Rj.dataLayer=nd;Rj.callback=function(a){Ic.hasOwnProperty(a)&&ia(Ic[a])&&Ic[a]();delete Ic[a]};Rj.af=function(){zc[yc.h]=Rj;Aa(Jc,Z.a);Xb=Xb||Mf;Yb=Cd};
Rj.Qf=function(){Gg.gtm_3pds=!0;zc=v.google_tag_manager=v.google_tag_manager||{};if(zc[yc.h]){var a=zc.zones;a&&a.unregisterChild(yc.h)}else{for(var b=data.resource||{},c=b.macros||[],d=0;d<c.length;d++)Pb.push(c[d]);for(var e=b.tags||[],g=0;g<e.length;g++)Sb.push(e[g]);for(var h=b.predicates||[],
k=0;k<h.length;k++)Rb.push(h[k]);for(var l=b.rules||[],m=0;m<l.length;m++){for(var n=l[m],q={},t=0;t<n.length;t++)q[n[t][0]]=Array.prototype.slice.call(n[t],1);Qb.push(q)}Vb=Z;Wb=pi;Rj.af();pf();Fd=!1;Gd=0;if("interactive"==A.readyState&&!A.createEventObject||"complete"==A.readyState)Id();else{Qa(A,"DOMContentLoaded",Id);Qa(A,"readystatechange",Id);if(A.createEventObject&&A.documentElement.doScroll){var p=!0;try{p=!v.frameElement}catch(y){}p&&Jd()}Qa(v,"load",Id)}cf=!1;"complete"===A.readyState?ef():
Qa(v,"load",ef);a:{if(!cd)break a;v.setInterval(dd,864E5);}
Fc=(new Date).getTime();}};(0,Rj.Qf)();

})()
