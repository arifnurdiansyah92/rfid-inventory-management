/**
 * embed generator
 */

(function ($) {
    var oembed = {
        loopPasta:0,
        classPasta:null,
        maxwidth:500,
        countPasta:[],
        classBody:'.itp_bodycontent',
        to:3000,
        init: function(el, k)
        {
            var cls = el.attr('class');
                media = cls.split(' ');
                target = el.attr('href');
            if (typeof media[1] != 'undefined' && media[1] !== null) {
                var act = '_'+media[1];
                try{
                    if($(this.classBody).width() > 0){
                        this.maxwidth = $(this.classBody).width();
                    }
                    var arg = {
                        'url':target,
                        'maxwidth':this.maxwidth,
                        'class':media[1],
                    }
                    this[act](el, arg, k);
                }catch(e){
                    console.log('GAMBAS:NOT FOUND');
                }
            }
            return;
        },
        _instagram: function(e, arg)
        {
            var param = {
                'url':`https://www.detik.com/api/instagram/?url=${arg.url}`,
                'dataType':'json',
                'style':'margin:20px 0 0 0',
                'exist': {
                    'tag':'iframe.instagram-media',
                    'get':'height'
                }
            }
            this.__request(e, param);
        },
        _twitter: function(e, arg)
        {
            var param = {
                'url':'https://publish.twitter.com/oembed?maxwidth='+arg.maxwidth+'&url='+arg.url,
                'dataType':'jsonp',
                'style':'margin:20px 0 0 0'
            }
            this.__request(e, param);
        },
        _youtube: function(e, arg)
        {
            var i = this.getUrlVars(arg.url);
            var args = {
                'url': 'https://www.youtube.com/embed/'+i,
                'style':'border:0;',
                'class':arg.class,
                'exist': {
                    'tag':'iframe[src*="youtube.com"]',
                    'get':'height'
                }
            }
            this.__video(e, args);
        },
        _video20detik: function(e, arg, k)
        {
            var _ic = 'iframe.video20detik_'+k;
            var _c = '.video20detik_'+k;
            
            var scroll_ap = true;
            var bumper = ''
            if(oembed.checkMobile()){
                bumper = 'width: 100px !important;'
            }
            $(e).removeClass('video20detik').addClass('video20detik_'+k);
            $(e).html(`<div class="bumper20detik_${k}" align="center"><img style="${bumper}" src="https://cdn.detik.net.id/assets/images/load.gif"/></div>`);
            $(window).bind('scroll load', function(){
                var exist = $('body').find(_ic).length;
                if(oembed.inview('.video20detik_'+k))
                {
                    if(exist == 0)
                    {
                        var args = {
                            'url': arg.url+'?smartautoplay=true',
                            'style':'border:0;text-align:left;visibility:hidden;',
                            'class':arg.class+'_'+k,
                            'exist': {
                                'tag':'iframe[src*="20.detik.com"]',
                                'get':'height'
                            },
                            'add_attr': 'allowfullscreen allow="autoplay"'
                        }
                        oembed.__video(e, args, k);
                    }
                }
                if(exist != 0)
                {
                    if(scroll_ap == false && oembed.inview(_c) == false)
                    {
                        scroll_ap = true;
                    }
                    window.addEventListener('message', function (event) {
                        if (event.data.scroll == 'disable')
                        {
                            scroll_ap = false;
                        }
                    }, false);
                    if(scroll_ap)
                    {
                        var i = document.querySelectorAll(_ic)[0];
                        i.contentWindow.postMessage({
                            'player': (oembed.inview('.video20detik_'+k)) ? 'in' : 'out',
                            'type': 'scroll'
                        }, "*");
                    }
                }
            })
        },
        _pasangmata: function(e, arg)
        {
            var args = {
                'cls':arg.class+this.loopPasta,
                'style':'width:'+(this.maxwidth-parseInt(10))+'px;overflow:hidden;display: block;border:0;'
            }
            var iframe = '<iframe scrolling="no" style="'+args.style+'" class="'+args.cls+'" src="'+arg.url+'?w='+(this.maxwidth-parseInt(20))+'&loop='+this.loopPasta+'"></iframe>';
            $(e).replaceWith('<div class="sisip_embed_sosmed">'+iframe+'</div>');
            this.countPasta.push(arg.class+this.loopPasta);
            if(oembed.checkMobile()){
                oembed.existTag({
                    'tag':'iframe.'+args.cls,
                    'get':'height'
                });
            }
            this.loopPasta++;
        },
        __video: function(e, param, k)
        {
            var style;
            var new_attr;
            var w = $(this.classBody).width();
            var h = (w*9/16)+parseInt(0);
            if(typeof(param.style) != "undefined" && param.style !== null) {
                var style = 'style='+param.style+'';
            }
            if(typeof(param.add_attr) != "undefined" && param.add_attr !== null) {
                var new_attr = param.add_attr;
            }
            var iframe = '<iframe scrolling="no" '+style+' '+new_attr+' src="'+param.url+'" width="'+w+'px" height="'+h+'px" class="'+param.class+'"></iframe>';
            
            if(param.class == 'video20detik_'+k)
            {
                iframe = '<div class="sisip_video_ds">'+iframe+'</div>';
                $('<div class="sisip_embed_sosmed">'+iframe+'</div>').appendTo(e);
                window.addEventListener('message', function (event) {
                    if (event.data.videois == 'play') {
                        $('body').find('div.bumper20detik_'+k).remove();
                        $('iframe.video20detik_'+k).css('visibility','visible');
                    }
                }, false);
            }
            else
            {
                $(e).replaceWith('<div class="sisip_embed_sosmed">'+iframe+'</div>');
            }

            if(oembed.checkMobile()){
                if (typeof param.exist != 'undefined' && param.exist !== null) {
                    oembed.existTag(param.exist);
                }
            }
        },
        __request: function(e, param)
        {
            $.ajax({
                url: param.url,
                method: "GET",
                dataType: param.dataType
            }).done(function(response) {
                $(e).replaceWith('<div class="sisip_embed_sosmed" style="'+param.style+'">'+response.html+'</div>');
                if(oembed.checkMobile()){
                    if (typeof param.exist != 'undefined' && param.exist !== null) {
                        oembed.existTag(param.exist);
                    }
                }
            });
        },
        checkMobile: function()
        {
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                console.log('GAMBAS:MOBILE');
                return true;
            }
            return false;
        },
        existTag: function(param)
        {
            setTimeout(function(){
                if($(param.tag).length > 0){
                    var res = $(param.tag).attr(param.get);
                    if (typeof res != 'undefined' && res !== 0) {
                        $(param.tag).css(param.get,res);
                    }
                }
            }, this.to)
        },
        getUrlVars: function(uri)
        {
            var vars = [], hash;
            var hashes = uri.slice(uri.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars['v'];
        },
        inview: function(elem)
        {
            var elm = document.querySelector(elem);
            var rect = elm.getBoundingClientRect();
            var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
            return !(rect.bottom < 200 || rect.top - viewHeight >= -200);
        }
    }

    $('a.embed').each(function(k){
        oembed.init($(this), k);
    });

    if(oembed.countPasta.length > 0){
        $.each(oembed.countPasta, function( index, value ) {
            window.addEventListener('message', function (event) {
                if (event.data.dtk == value) {
                    var data = event.data;
                    document.getElementsByClassName(value)[0].height = (data.height)+parseInt(20);
                }
            }, false);
        });
    }

    if($(".itp_bodycontent iframe[src*='facebook.com']").length > 0){
        $(".itp_bodycontent iframe[src*='facebook.com']").css({
            'width':'100%',
            'height':$(".itp_bodycontent iframe[src*='facebook.com']").attr('height')
        });
    }

    if($(".itp_bodycontent iframe[src*='20.detik.com']").length > 0){
        $(".itp_bodycontent iframe[src*='20.detik.com']").css({
            'width':'100%',
            'height':($('.itp_bodycontent').width()*9/16)+parseInt(0)
        });
    }

})(jQuery);

$(window).bind("load", function() {
    var frames, b, source, iframe20d, msg, article_id;
    article_id = $('meta[name=articleid]').attr("content");
    frames = document.getElementsByTagName("iframe");
    msg = article_id;

    for (b = 0; b < frames.length; b++) {
        source = frames[b].src;
        if(source.includes('detik.com')){
            iframe20d = frames[b].contentWindow;
            iframe20d.postMessage(msg, '*');
        }
    }
});
