/**
 * Mei 21, 2015
 */

var mood = {
	baseurl	: 'https://mood.detik.com/display/',
	url		: 'https://mood.detik.com/display/',
	img_url	: 'https://images.detik.com/community/moodrating/',
	parent_url : '',
	target : 'mood_box',
	idkanal : 0,
	idnews : 0,
	idsubkanal : 0,
	idfokus : 0,
	idmicrosite : 0,
	appid : 0,
	title : '',
	article_url : '',
	date : '',
	socket : '',
	cssinhead : function(url) {
	    var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('link');

	    link.rel  = 'stylesheet';
	    link.type = 'text/css';
	    link.href = url;
	    link.media = 'all';

	    head.appendChild(link);
	},
	jsinhead : function(url, cont, attr) {
		var head = document.getElementsByTagName('head')[0];
		var script = document.createElement('script');
		script.type = 'text/javascript';
		if(url != null)
			script.src = url;
		if(cont != null)
			script.innerHTML = cont;
		if(typeof(attr) != 'undefined')
			script.setAttribute(attr.atname, attr.atval);

		head.appendChild(script);
	},
	mandatoryfile : function(obj) {
		this.cssinhead(this.baseurl+'css/style_2.css?v2.3');

		if(typeof(obj) != 'undefined')
			if(typeof(obj.css) != 'undefined' && obj.css != '')
				mood.cssinhead(obj.css);

	},
	firstresize : function (){
		setTimeout(function(){
			var bheight = this.cur_height = $('html').height();

			$.postMessage(
				'height:'+bheight,
	       		mood.parent_url,
	       		parent
	         );
		}, 410);
	},
	resizeparent : function (){
		setTimeout(function(){
			var bheight = $('html').height();

			while (bheight != this.cur_height) {
				this.cur_height = bheight;
				bheight = $('html').height();
			}

			$.postMessage(
				'height:'+bheight,
	       		mood.parent_url,
	       		parent
			);
		}, 400);
	},
	parentLayout : function (){
		$.postMessage(
			'login',
       		mood.parent_url,
       		parent
         );
	},
	setProperties : function (obj) {
		if (typeof obj.idkanal !== 'undefined')
			mood.idkanal = obj.idkanal;
		if (typeof obj.idnews !== 'undefined')
			mood.idnews = obj.idnews;
		if (typeof obj.title !== 'undefined')

		if (typeof obj.idsubkanal !== 'undefined')
			mood.idsubkanal = obj.idsubkanal;
		if (typeof obj.idfokus !== 'undefined')
			mood.idfokus = obj.idfokus;
		if (typeof obj.idmicrosite !== 'undefined')
			mood.idmicrosite = obj.idmicrosite;
		if (typeof obj.appid !== 'undefined')
			mood.appid = obj.appid;
		if (typeof obj.date !== 'undefined')
			mood.date = obj.date;

		if (typeof obj.title !== 'undefined')
			mood.title = obj.title;
		if (typeof obj.article_url !== 'undefined')
			mood.article_url = obj.article_url;
		if (typeof obj.article_url !== 'undefined')
			mood.parent_url = obj.article_url;
	},
	listenerResize : function () {
		window.onresize = function() {

		}
	},
	listeningCall : function (idkanal, idnews) {
		// var ws = new WebSocket('wss://push.detik.com/ws/moodrating'+idkanal+idnews);
		//
		// ws.onmessage = function(data) {
		// 	var value	= JSON.parse(data.data);
		// 	mood.show(value, 1);
		// }
	},
	init : function(obj) {
		mood.setProperties(obj);
		mood.mandatoryfile();

		var urlget = '';

		$.ajax({
			url			: 'https://mood.detik.com/api/init?idkanal='+mood.idkanal+'&idnews='+mood.idnews+
							'&idsubkanal='+mood.idsubkanal+'&idfokus='+mood.idfokus+'&idmicrosite='+mood.idmicrosite+'&news_url='+mood.parent_url,
			type		: "GET",
			dataType	:'json',
			beforeSend	: function() {
			},
			success	: function(data){
				if (data.error.flag > 0)
					alert(data.error.msg);
				else {
					profile.isVoted = data.info.user_voted;
					profile.getData(data.profile);
					mood.getTemplate(data);

					// langsung munculkan hasil
					if (data.info.user_voted > 0)
						mood.show(data, 2, data.info.user_rated);
					else
						mood.show(data);

					// mood.listeningCall(mood.idkanal, mood.idnews);
				}
			},
			error	: function (e) {
				setTimeout(function(){
					var errInfo = $('<div/>').html('Gagal memuat mood rating. Silahkan coba beberapa saat lagi')
									.css({'background-color': '#FFF8C5', 'border': '1px solid #E0E904',
											'color': '#AA2828', 'padding': '10px'});
					$('#'+mood.target).html(errInfo);

				},5000);
			}
		});

	},
	getTemplate : function (data) {
		var reaction_	= $('<div/>').addClass('reaction_');

		var title	= $('<div/>').addClass('title'),  // **
		ask			= $('<span/>').addClass('ask rot').html('Bagaimana reaksi Anda tentang artikel ini?'),
		ico_d		= $('<span/>').addClass('ico_d rot'),
		img_arrdown	= $('<img/>').attr({src:mood.img_url+'arrow_down2.png'}).click(function(){
							mood.panelShowHide($(this));
						}),

		clearfix	= $('<div/>').addClass('clearfix');
		$(title).append(ask).append(ico_d.append(img_arrdown)).append(clearfix);

		var title_act	= $('<div/>').addClass('title_act terhibur');  // **
		var clearfix_2	= $('<div/>').addClass('clearfix');  // *

		var reaction_1	= $('<div/>').addClass('reaction_1'); // **

		if (data.option.length > 0) {
			$(data.option).each(function(key, value){
				var tip_senang	= $('<a/>').addClass('tip').attr({'data-tip':value.name, 'data-seq':value.id}).css({'background': value.color})
									.click(function(){ mood.submit($(this))}), // ***
				box_img_senang	= $('<div/>').addClass('ratio1_1 box_img'),
				img_con_senang	= $('<div/>').addClass('img_con'),
				img_senang		= $('<img/>').addClass('satu').attr({src:mood.img_url+value.image_default}),
				img_senang_dua	= $('<img/>').addClass('dua').attr({src:mood.img_url+value.image_gif+'?a=1'}),
				info_senang		= $('<span/>').addClass('info').html(value.label).css({'background': value.color}),
				nilai_senang	= $('<div/>').addClass('nilai').attr({style:'height:'+value.total_percent+'%'}),
				nilai2_senang	= $('<div/>').addClass('nilai2').html(value.total_percent+'%');
				$(box_img_senang).append(img_con_senang.append(img_senang).append(img_senang_dua));
				$(tip_senang).append(box_img_senang).append(info_senang).append(nilai_senang).append(nilai2_senang);

				$(reaction_1).append(tip_senang);
			})
		}

		var clearfix_3	= $('<div/>').addClass('clearfix');  // **

		$(reaction_).append(title).append(title_act).append(clearfix_2).append(reaction_1).append(clearfix_3);

		$('#'+mood.target).html(reaction_);
	},
	submit : function(elm) {
		if (profile.islogin == 0) {
			goLogin();
			console.log('need login');

			return
		}

		var choice	= $(elm).attr('data-tip').toLowerCase(),
		idicon		= $(elm).attr('data-seq'),
		label		=  $(elm).find('.info').html();

		var entry	= {idkanal:mood.idkanal, idnews:mood.idnews, prf_id:profile.id, prf_name:profile.name, prf_email:profile.email, choice:choice, idicon:idicon, idsubkanal: mood.idsubkanal,
						idfokus: mood.idfokus, idmicrosite: mood.idmicrosite, appid: appid, title: mood.title, url:mood.article_url, userAgent: navigator.userAgent, date:mood.date}

		$.ajax({
			url			: 'https://mood.detik.com/api/init/vote',
			type		: "POST",
			data		: entry,
			dataType	:'json',
			beforeSend	: function() {
			},
			success	: function(data){
				if (data.error.flag > 0)
					alert(data.error.msg);
				else {

					profile.isVoted = 1;
					mood.show(data, 2, idicon);
					// mood.listeningCall(mood.idkanal, mood.idnews);
				}
			},
			error	: function (e) {
				setTimeout(function(){
					var errInfo = $('<div/>').html('Gagal memuat mood rating. Silahkan coba beberapa saat lagi')
									.css({'background-color': '#FFF8C5', 'border': '1px solid #E0E904',
											'color': '#AA2828', 'padding': '10px'});
					$('#'+mood.target).html(errInfo);

				},5000);
			}
		});

	},
	show : function (data, update, choice) {
		// update : 0 init, 1 lister, 2 submit
		var selected = '';
		if (profile.isVoted > 0)
			selected = 'selected';

		if (typeof elm === 'undefined')
			elm = $('.senang');
		if (typeof label === 'undefined')
			label = '';
		if (typeof update === 'undefined')
			update = 0;

		var sel_color = '#000'
		if(typeof(data.info.color) != 'undefined')
			sel_color = data.info.color;

		$.each(data.option, function( key, val ) {
			if (update == 1) {
				$('.reaction_1').find('a[data-seq='+val.id+']').find('.nilai').css({'height':val.total_percent+'%'});
				$('.reaction_1').find('a[data-seq='+val.id+']').find('.nilai2').html(val.total_percent+'%');
			}
			else {
				$('.reaction_1').find('a[data-seq='+val.id+']').addClass(selected).find('.nilai').attr({'style':'height:'+val.total_percent+'%'}).show(500);
				$('.reaction_1').find('a[data-seq='+val.id+']').find('.nilai2').html(val.total_percent+'%').show(500);
			}
		});

		if (parseInt(data.info.posted) > 0 && profile.isVoted > 0) {
			// $(elm).closest('.reaction_').find('.title').hide(500);
			$('.title').hide(500);
			var pembaca_merasa = mood.createWording(data.info);
			// console.log(data.info); alert(pembaca_merasa);
			// $(elm).closest('.reaction_').find('.title_act').html(pembaca_merasa).show(500);

			/* 2 line below, disable by yuz for change color default */
			// var def_label = ['senang','terhibur','inspirasi','rata','ganggu','sedih','takut','marah','terbagi']
			// if(def_label.indexOf(data.info.most_rated_label.toLowerCase()) < 0)
				$('.title_act').attr({'style':'background-color:'+sel_color})

			$('.title_act').html(pembaca_merasa).show(500);
		}

		$('.title_act').removeClass('senang terhibur inspirasi rata ganggu sedih takut marah terbagi');
		$('.title_act').addClass(data.info.most_rated);

		if (update == 0 || update == 2) {
			if (update == 2)
				var choice_label = choice;
			else
				choice_label = data.info.user_rated;

			var check_list = $('<img/>').addClass('check').attr({src:'https://mood.detik.com/display/images/default/check.png'});
			if(typeof(choice_label) != 'undefined') {
				if(choice_label > 0) {
					$('.reaction_1').find('a[data-seq='+choice_label+']').addClass('set pilih').prepend(check_list);
					$('.reaction_1').find('a[data-seq='+choice_label+']').find('.satu').hide();
					$('.reaction_1').find('a[data-seq='+choice_label+']').find('.dua').show().css({display:'inline'});
				}
			}
		}

		if (profile.isVoted > 0) { // disable box saat sudah vote
			$('.reaction_1').find('a.tip').unbind('click');
		}

		mood.resizeparent();
	},
	createWording : function(info) {
		var result = '';

		if (info.posted > 0) {
			if (typeof(info.wording) != 'undefined') {
				result = info.wording
			}
		}

		return result;
	},
	panelShowHide : function(img) {
		if ($('.reaction_1').is(":visible")) {
			$('.reaction_1').hide(300);
			$(img).closest('.ico_d').removeClass('rot');
			$(img).closest('.ask').removeClass('rot');
		}
		else {
			$('.reaction_1').show(300);
			$(img).closest('.ico_d').addClass('rot');
			$(img).closest('.ask').addClass('rot');
		}
		mood.resizeparent();
	},
	goLogin : function () {

		mood.socket.postMessage("gologin");
	},
	log : function(msg) {
		console.log(msg);
	}
};

var colour = {
	senang : '#ff4800',
	terhibur : '#bbac1b',
	inspirasi : '#bbac1b',
	rata : '#ffa329',
	ganggu : '#0aa3bc',
	sedih : '#0aa3bc',
	takut : '#0aa3bc',
	marah : '#845f34'
}

var profile	= {
	islogin	: 0,
	id : '',
	name : '',
	fullname : '',
	image : '',
	isVoted : 0,
	getData : function (data) {
		if (data != '') {
			profile.islogin = 1;
			profile.id = data.prf_id;
			profile.name = data.prf_name;
			profile.email = data.prf_email;
			profile.fullname = data.prf_fullname;
			profile.image = data.prf_image;
		}
	}
}
