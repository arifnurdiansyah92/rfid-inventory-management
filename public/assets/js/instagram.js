$('#instafeed').instahistory({
    get: '@disparbudkrwkab_', // or @username
    imageSize: 240,
    limit: 8,
    template: '<div class="col"><a href="{{link}}" type="_blank"><img src="{{image}}" class="img-responsive"><span class="info"></span></a></div>'
});