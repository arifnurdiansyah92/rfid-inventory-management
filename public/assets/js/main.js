
$(document).ready(function() {

  $('.menu-utama').affix({
      // offset: {top: $('.abovebrandnav').height() + 190}
      offset: {top: 190}
  }).on('affix.bs.affix', function () {
      $('.menu-utama').removeClass("animated fadeInUp2");
      $('.menu-utama .topnav').removeClass("animated topnav2top");
      $('.menu-utama .topnav .nav>li>a').removeClass("animated topnav-a");
      $('.menu-utama .logo').removeClass("animated logo2top");
      $('.menu-utama').addClass("animated02 fadeInDown2");
      $('.menu-utama .topnav').addClass("animated07 fadeFromRight");
      $('.menu-utama .logo').addClass("animated07 fadeFromLeft");
  }).on('affix-top.bs.affix', function () {
      $('.menu-utama').removeClass("animated02 fadeInDown2");
      $('.menu-utama .topnav').removeClass("animated07 fadeFromRight");
      $('.menu-utama .logo').removeClass("animated07 fadeFromLeft");
      $('.menu-utama').addClass("animated fadeInUp2");
      $('.menu-utama .topnav').addClass("animated topnav2top");
      $('.menu-utama .topnav .nav>li>a').addClass("animated topnav-a");
      $('.menu-utama .logo').addClass("animated logo2top");
  });


  $('.topnav .nav > li.dropdown-hover').hover(function(){
    $('ul.dropdown-menu.root', this).stop(true, true).addClass('animated03 fadeInDown');
    $(this).addClass('open');
  }, function() {
    $('ul.dropdown-menu.root', this).stop(true, true).removeClass('animated03 fadeInDown');
    $(this).removeClass('open');
  });

});


// Config Options
    var slideout = new Slideout({
      'panel': document.getElementById('wrapper'),
      'menu': document.getElementById('menu-m'),
      'side': 'right',
      'padding': 256,
      'tolerance': 70
    });

    slideout.enableTouch();

    // Toggle button
    document.querySelector('.toggle-button').addEventListener('click', function() {
        slideout.toggle();
    });

    $('.m-link').on('click', function() {
      slideout.close();
    });

    // Close menu
    // document.querySelector('.menu').addEventListener('click', function(eve) {
    //       if (eve.target.nodeName === 'A') { slideout.close(); }
    // });

