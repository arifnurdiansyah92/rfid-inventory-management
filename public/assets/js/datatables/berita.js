"use strict";
var KTDatatablesBasicScrollable = function() {

	var initTable1 = function() {
		var table = $('#kt_table_1');

		// begin first table
		table.DataTable({
			scrollY: '50vh',
			scrollX: true,
			scrollCollapse: true,
			order: [[ 0, "desc" ]],
			columnDefs: [
				{
					targets: -1,
					title: 'Aksi',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
						<a href="/admin/berita/${data}" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                          <i class="la la-edit"></i>
                        </a>
                        <a href="/admin/berita/${data}/delete" onclick="return confirm('Yakin ingin menghapus data?');" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Hapus">
                          <i class="la la-trash"></i>
                        </a>`;
					},
                },
                {
					targets: 0,
					render: function(data, type, full, meta) {
						return moment(data).format('DD/MM/YYYY');
					},
                },
                {
					targets: 1,
					render: function(data, type, full, meta) {
						return `<img src="/uploads/${data}" class="img-thumbnail" style='height:50px;'>`;
					},
				},
				{
					targets: 3,
					render: function(data, type, full, meta) {
						var status = {
							0: {'title': 'Draft', 'class': ' kt-badge--warning'},
							1: {'title': 'Published', 'class': 'kt-badge--success'},
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-badge ' + status[data].class + ' kt-badge--inline kt-badge--pill">' + status[data].title + '</span>';
					},
				},
			],
		});
	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
			// initTable2();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesBasicScrollable.init();
});