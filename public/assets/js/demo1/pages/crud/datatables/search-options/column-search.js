"use strict";
var KTDatatablesSearchOptionsColumnSearch = function() {

	$.fn.dataTable.Api.register('column().title()', function() {
		return $(this.header()).text().trim();
	});

	var initTable1 = function() {

		// begin first table
		var table = $('#kt_table_1').DataTable({
			responsive: true,

			// Pagination settings
			dom: `<'row'<'col-sm-12'tr>>
			<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
			// read more: https://datatables.net/examples/basic_init/dom.html

			lengthMenu: [5, 10, 25, 50],

			pageLength: 10,

			language: {
				'lengthMenu': 'Display _MENU_',
			},

			searchDelay: 500,
			processing: false,
			serverSide: false,
			ajax: {
				url: './fakedata/stakeholder.json',
				type: 'GET'
			},
			columns: [
				{data: 'FullName'},
				{data: 'PreferredName'},
				{data: 'Corporation'},
				{data: 'PersonalPhoneNumber'},
				{data: 'BusinessPhoneNumber'},
				{data: 'PersonalEmailAddress'},
				{data: 'BusinessEmailAddress'},
                {data: 'Position'},
                {data: 'Level'},
				{data: 'Actions', responsivePriority: -1},
			],
			initComplete: function() {
				var thisTable = this;
				var rowFilter = $('<tr class="filter"></tr>').appendTo($(table.table().header()));
                this.api().columns().every(function() {
					var column = this;
					var input;

					switch (column.title()) {
						case 'Position':
							var status = {
								1: {'title': 'Manager', 'class': 'kt-badge--brand'},
								2: {'title': 'CEO', 'class': ' kt-badge--danger'},
							};
							input = $(`<select class="form-control form-control-sm form-filter kt-input" title="Select" data-col-index="` + column.index() + `">
										<option value="">Select</option></select>`);
							column.data().unique().sort().each(function(d, j) {
								$(input).append('<option value="' + d + '">' + status[d].title + '</option>');
							});
							break;
                        case 'Level':
							var level = {
								1: {'title': 'Senior', 'class': 'kt-badge--brand'},
								2: {'title': 'Middle', 'class': ' kt-badge--danger'},
                                3: {'title': 'Intermediate', 'class': ' kt-badge--danger'},
							};
							input = $(`<select class="form-control form-control-sm form-filter kt-input" title="Select" data-col-index="` + column.index() + `">
										<option value="">Select</option></select>`);
							column.data().unique().sort().each(function(d, j) {
								$(input).append('<option value="' + d + '">' + level[d].title + '</option>');
							});
							break;
						case 'Actions':
							var search = $(`<button class="btn btn-brand kt-btn btn-sm kt-btn--icon">
							  <span>
							    <i class="la la-search"></i>
							    <span>Search</span>
							  </span>
							</button>`);

							var reset = $(`<button class="btn btn-secondary kt-btn btn-sm kt-btn--icon">
							  <span>
							    <i class="la la-close"></i>
							    <span>Reset</span>
							  </span>
							</button>`);

							$('<th>').append(search).append(reset).appendTo(rowFilter);

							$(search).on('click', function(e) {
								e.preventDefault();
								var params = {};
								$(rowFilter).find('.kt-input').each(function() {
									var i = $(this).data('col-index');
									if (params[i]) {
										params[i] += '|' + $(this).val();
									}
									else {
										params[i] = $(this).val();
									}
								});
								$.each(params, function(i, val) {
									// apply search params to datatable
									table.column(i).search(val ? val : '', false, false);
								});
								table.table().draw();
							});

							$(reset).on('click', function(e) {
								e.preventDefault();
								$(rowFilter).find('.kt-input').each(function(i) {
									$(this).val('');
									table.column($(this).data('col-index')).search('', false, false);
								});
								table.table().draw();
							});
							break;
					}

					if (column.title() !== 'Actions') {
						$(input).appendTo($('<th>').appendTo(rowFilter));
					}
				});
				// hide search column for responsive table
				var hideSearchColumnResponsive = function () {
                    thisTable.api().columns().every(function () {
                        var column = this
                        if(column.responsiveHidden()) {
                            $(rowFilter).find('th').eq(column.index()).show();
                        } else {
                            $(rowFilter).find('th').eq(column.index()).hide();
                        }
                    })
                };

				// init on datatable load
				hideSearchColumnResponsive();
				// recheck on window resize
				window.onresize = hideSearchColumnResponsive;

				$('#kt_datepicker_1,#kt_datepicker_2').datepicker();
			},
			columnDefs: [
				{
					targets: -1,
					title: 'Actions',
					orderable: false,
					render: function(data, type, full, meta) {
						return `
                        <span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="true">
                              <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="demo1/profile.html"><i class="la la-edit"></i> Detail</a>
                            </div>
                        </span>
                        `;
					},
				},
				{
					targets: 5,
					width: '150px',
				},
				{
					targets: 7,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Manager', 'state': 'danger'},
							2: {'title': 'CEO', 'state': 'primary'},
						
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
                {
					targets: 8,
					render: function(data, type, full, meta) {
						var status = {
							1: {'title': 'Senior', 'state': 'danger'},
							2: {'title': 'Middle', 'state': 'primary'},
                            3: {'title': 'Intermediate', 'state': 'primary'},
						
						};
						if (typeof status[data] === 'undefined') {
							return data;
						}
						return '<span class="kt-font-bold kt-font-' + status[data].state + '">' + status[data].title + '</span>';
					},
				},
			],
		});

	};

	return {

		//main function to initiate the module
		init: function() {
			initTable1();
		},

	};

}();

jQuery(document).ready(function() {
	KTDatatablesSearchOptionsColumnSearch.init();
});