"use strict";
var datatableEmail = function() {
    if ($('#kt_datatable_email').length === 0) {
        return;
    }
    var datatable = $('#kt_datatable_email').KTDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    method: 'GET',
                    url: './fakedata/email.json'
                }
            },
            pageSize: 10,
            saveState: {
                cookie: false,
                webstorage: true
            },
        },
        layout: {
            theme: 'default',
            class: '',
            scroll: true,
            height: 500,
            footer: false
        },
        sortable: true,
        filterable: false,
        pagination: true,
        columns: [{
            field: "Subject",
            title: "Subject",
            autoHide: false,
            //width: 100
        },{
            field: "From",
            title: "From",
            autoHide: false,
            width: 200
        },{
            field: "Status",
            title: "Status",
            autoHide: false,
            width: 100,
            template: function(row) {
                    var status = {
                        1: {
                            'title': 'Unread',
                            'class': ' btn-label-danger'
                        },
                        2: {
                            'title': 'Read',
                            'class': ' btn-label-success'
                        },                                             
                    };
                    return '<span class="btn btn-bold btn-sm btn-font-sm ' + status[row.Status].class + '">' + status[row.Status].title + '</span>';
                }
            },{
            field: "Actions",
            width: 80,
            title: "Actions",
            sortable: false,
            autoHide: false,
            overflow: 'visible',
            template: function() {
                return '\
                    <div class="dropdown">\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">\
                            <i class="flaticon-more-1"></i>\
                        </a>\
                        <div class="dropdown-menu dropdown-menu-right">\
                            <ul class="kt-nav">\
                                <li class="kt-nav__item">\
                                    <a href="javascript:;" class="kt-nav__link">\
                                        <i class="kt-nav__link-icon flaticon2-expand"></i>\
                                        <span class="kt-nav__link-text">Detail</span>\
                                    </a>\
                                </li>\
                            </ul>\
                        </div>\
                    </div>\
                ';
            }
        }]
    });
}

jQuery(document).ready(function() {
	datatableEmail();
});