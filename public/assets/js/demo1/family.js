"use strict";
var datatableFamily = function() {
    if ($('#kt_datatable_family').length === 0) {
        return;
    }
    var datatable = $('#kt_datatable_family').KTDatatable({
        data: {
            type: 'remote',
            source: {
                read: {
                    method: 'GET',
                    url: './fakedata/family.json'
                }
            },
            pageSize: 10,
            saveState: {
                cookie: false,
                webstorage: true
            },
        },
        layout: {
            theme: 'default',
            class: '',
            scroll: true,
            height: 500,
            footer: false
        },
        sortable: true,
        filterable: false,
        pagination: true,
        columns: [{
            field: "NameOfSpouse",
            title: "Name Of Spouse",
            autoHide: false,
            
        },{
            field: "NameOfDescendant",
            title: "Name Of Descendant",
            autoHide: false,
        
        },{
            field: "HobbiesAndInterest",
            title: "Hobbies And Interest",
            autoHide: false,
            width: 100
        },{
            field: "SentimentAndRemarks",
            title: "Sentiment And Remarks",
            autoHide: false,
            width: 100
        },{
            field: "Actions",
            width: 80,
            title: "Actions",
            sortable: false,
            autoHide: false,
            overflow: 'visible',
            template: function() {
                return '\
                    <div class="dropdown">\
                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown">\
                            <i class="flaticon-more-1"></i>\
                        </a>\
                        <div class="dropdown-menu dropdown-menu-right">\
                            <ul class="kt-nav">\
                                <li class="kt-nav__item">\
                                    <a href="#" class="kt-nav__link">\
                                        <i class="kt-nav__link-icon flaticon2-expand"></i>\
                                        <span class="kt-nav__link-text">Detail</span>\
                                    </a>\
                                </li>\
                                <li class="kt-nav__item">\
                                    <a href="#" class="kt-nav__link">\
                                        <i class="kt-nav__link-icon flaticon2-edit"></i>\
                                        <span class="kt-nav__link-text">Edit</span>\
                                    </a>\
                                </li>\
                            </ul>\
                        </div>\
                    </div>\
                ';
            }
        }]
    });
}

jQuery(document).ready(function() {
	datatableFamily();
});