@extends('layouts.master')
@section('title','Checkin')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div id="search-sbg" class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body">
            <form class="form-inline">
                <div class="form-group mb-2">
                    <label for="staticEmail2" class="sr-only">Email</label>
                    <input type="text" readonly class="form-control-plaintext" id="staticEmail2"
                        value="Masukkan nomor SBG">
                </div>
                <div class="form-group mx-sm-3 mb-2">
                    <label for="inputPassword2" class="sr-only">Nomor SBG</label>
                    <input id="sbg" type="text" class="form-control" id="inputPassword2" placeholder="Nomor SBG">
                </div>
                <button type="button" onclick="checksbg()" class="btn mb-2"
                    style="background-color:#00ab4e !important;color:#ffffff">Periksa</button>
            </form>
        </div>
    </div>
    <div id="form-wizard" class="d-none">
        <ul>
            <li><a href="#step-1">Detail BG</a></li>
            <li><a href="#step-2">Ambil Foto</a></li>
            <li><a href="#step-3">Label</a></li>
            <li><a href="#step-4">Summary</a></li>
        </ul>

        <div>
            <div id="step-1" class="">
                Step Content
            </div>
            <div id="step-2" class="">
                Step Content
            </div>
            <div id="step-3" class="">
                Step Content
            </div>
            <div id="step-4" class="">
                Step Content
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    function checksbg(){
        let sbg = $("#sbg").val();
        $.ajax({
            url: "{{ url('/api/check_sbg') }}/" + sbg,
            type: "GET",
            success: function(response){
                if(response.status == 2){
                    swal.fire("", "Sudah Lunas!");
                }else if(response.status == 1){
                    $("#form-wizard").removeClass("d-none");
                    $("#search-sbg").addClass("d-none");
                }
            },
            error: function(response){
                swal.fire("", "Data tidak ditemukan!");
            } 
        })
    }
    $(document).ready(function() {
        $('#form-wizard').smartWizard({
            lang: { 
                next: 'Next', 
                previous: 'Previous'
            },
            theme: 'arrows'
        });
        $("#form-wizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection) {
            if(stepNumber == "3"){
                $(".btn-toolbar").html(`
                    <div class="btn-group mr-2 sw-btn-group" role="group"><button class="btn btn-secondary sw-btn-prev" type="button" onclick="resetForm()">Cancel</button><button class="btn btn-secondary sw-btn-next" type="button">Simpan</button></div>
                `);
            }
        });
    });
    function resetForm(){
        $("#form-wizard").smartWizard("reset");
    }
</script>
@endsection