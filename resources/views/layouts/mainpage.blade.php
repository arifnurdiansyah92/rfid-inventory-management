<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/owl.carousel.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/owl.theme.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/fonts/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/fullcalendar390.min.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/fullcalendar.print.css') }}" media="print" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/scheduler.min.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/style.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/responsive.css') }}" />

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body data-spy="scroll" data-target=".dest-nav">

    <section id="header" class="hidden-xs hidden-sm">
        <div class="container-fluid menu-utama">

            <a href="{{ url('/') }}" class="logo">
                <h2 class="ttl-short">RFID Inventory Management</h2>
                <img src="{{ asset('/assets/img/logo-kab-karawang.png') }}" class="img-responsive">
                <h2>Karawang</h2>
                <h5 class="ttl-long x">Dinas Pariwisata dan Kebudayaan<br><span>Pemerintah Kabupaten Karawang </span>
                </h5>
            </a>

            @include('layouts.partials.topnav')
        </div>
    </section>

    <!-- mobile menu -->
    <nav id="menu-m" class="visible-xs visible-sm">
        <ul class="nav">
            <li class="active"><a href="#">About</a></li>
            <li><a href="#">Destinations</a></li>
            <li><a href="#">Interests</a></li>
            <li><a href="#">Facts</a></li>
            <li><a href="#">Media Center</a></li>
            <!-- <li class="divider-vertical"></li> -->
            <li><a href="#">Map</a></li>
        </ul>
        <p>&nbsp;</p>
    </nav>

    <header id="header-m" class="visible-xs visible-sm" data-spy="affix" data-offset-top="50">
        <div class="mheader">
            <img src="{{ asset('/assets/img/dm.png') }}" class="img-responsive">
        </div>

        <button type="button" class="toggle-button navbar-toggle" data-toggle="button" aria-pressed="false"
            autocomplete="off">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </header>


    <div id="wrapper">

        @yield('konten')
        @include('layouts.partials.footer')

    </div><!-- ./wrapper -->


    <script src="{{ asset('/assets/js/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets/js/slideout.min.js') }}"></script>
    <script src="{{ asset('/assets/js/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('/assets/js/main.js') }}"></script>
    <script src="{{ asset('/assets/js/moment.min.js') }}"></script>
    <script src="{{ asset('/assets/js/fullcalendar390.min.js') }}"></script>
    <script src="{{ asset('/assets/js/scheduler.min.js') }}"></script>
    <script src="{{ asset('/assets/js/id.js') }}"></script>
    <script src="{{ asset('/assets/js/instahistory.js') }}"></script>
    <script src="{{ asset('/assets/js/instagram.js') }}"></script>
	<script src="{{ asset('/assets/vendors/general/summernote/dist/summernote.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/js/summernote-pagebreak.js') }}"></script>
    @yield('scripts')

</body>

</html>