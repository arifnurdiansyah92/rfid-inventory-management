<section id="useful-info">
    <div class="container   ">
        <div class="section-h">
            <h2>Informasi bermanfaat Lainnya</h2>
        </div>

        <div class="row">
            @foreach($berita as $item)
            <div class="col-sm-4 col-md-4">
                <a href="{{ route('main.berita.detail',$item->id) }}" class="box-imgdesc">
                    <div class="imgboard">
                        <img src="{{ asset($item->thumbnail_img) }}" class="img-responsive">
                    </div>
                    <div class="desc">
                        <h4>{{ $item->judul }}</h4>
                        {!! $item->konten !!}
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>