<div class="topnav">
        <ul class="nav">
            <li class="dropdown dropdown-hover">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tentang kami</a>
                <ul class="dropdown-menu root">
                    <li><a href="#">Pariwisata Indonesia</a></li>
                    <li><a href="#">Penghargaan</a></li>
                    <li><a href="{{ route('main.kontak') }}">Hubungi kami</a></li>
                </ul>
            </li>
            <li class="dropdown dropdown-hover">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tujuan Wisata</a>
                <ul class="dropdown-menu root">
                    <li>
                        <a href="{{ route('main.pariwisata','curug-bandung') }}">
                            <div class="thumbox">
                                <img src="{{ asset('/assets/img/dests/thumb/th-curug-bandung.jpg') }}" class="img-responsive">
                            </div>
                            <span>Curug Bandung</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('main.pariwisata','pantai-tanjung-baru') }}">
                            <div class="thumbox">
                                <img src="{{ asset('/assets/img/dests/thumb/th-pantai-tanjung-baru.jpg') }}" class="img-responsive">
                            </div>
                            <span>Pantai Tanjung Baru</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('main.pariwisata','danau-cipule') }}">
                            <div class="thumbox">
                                <img src="{{ asset('/assets/img/dests/thumb/th-situ-cipule.jpg') }}" class="img-responsive">
                            </div>
                            <span>Danau Cipule</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('main.pariwisata','candi-jiwa') }}">
                            <div class="thumbox">
                                <img src="{{ asset('/assets/img/dests/thumb/th-candi-jiwa.jpg') }}" class="img-responsive">
                            </div>
                            <span>Candi Jiwa</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('main.pariwisata','green-canyon') }}">
                            <div class="thumbox">
                                <img src="{{ asset('/assets/img/dests/thumb/th-green-canyon.jpg') }}" class="img-responsive">
                            </div>
                            <span>Green Canyon</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <div class="thumbox">
                                <img src="{{ asset('/assets/img/dests/thumb/th-explore.jpg') }}" class="img-responsive">
                            </div>
                            <span>Lainnya...</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li><a href="#">Seni & Budaya</a></li>
            <li><a href="{{ route('main.event') }}">Event</a></li>
            <li><a href="{{ route('main.berita') }}">Berita</a></li>
            <li><a href="{{ route('main.kontak') }}">Kontak</a></li>
            <!-- <li class="dropdown dropdown-hover">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown">Media Center</a>
      <ul class="dropdown-menu root">
        <li><a href="#">Photo</a></li>
        <li><a href="#">Video</a></li>
        <li><a href="#">Press</a></li>
      </ul>
    </li>
    <li><a href="#">Maps</a></li> -->
        </ul>
    </div>
    <!-- </div> -->