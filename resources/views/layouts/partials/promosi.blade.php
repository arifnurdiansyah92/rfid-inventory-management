<div class="col-sm-3 col-md-3">
        <div class="sidenav">

            <a href="#" class="side-box-imgdesc">
                <div class="imgboard">
                    <img src="{{ asset('/assets/img/slides/karawang_pantai-tanjung-pakis.png') }}" class="img-responsive">
                </div>
                <div class="desc">
                    <h5>Banner promosi</h5>
                    <p>Duis dolor est, tincidunt vel enim sit amet, venenatis euismod neque. Duis
                        eleifend ligula id tortor finibus faucibus. Donec et quam pulvinar, blandit
                        tortor. ...</p>
                </div>
            </a>

            <ul>
                <h4>Tujuan Wisata Populer:</h4>
                <li><a href="curug-bandung.html">Curug Bandung</a></li>
                <li><a href="curug-bandung.html">Pantai Tanjung Baru</a></li>
                <li><a href="curug-bandung.html">Danau Cipule</a></li>
                <li><a href="curug-bandung.html">Candi Jiwa</a></li>
                <li><a href="curug-bandung.html">Pantai Sedari</a></li>
                <li><a href="curug-bandung.html">Bukit Kembar</a></li>
                <li><a href="curug-bandung.html">Pantai Samudera Baru</a></li>
                <li><a href="curug-bandung.html">Green Canyon</a></li>
                <li><a href="curug-bandung.html">Taruma Leisure Waterpark</a></li>
            </ul>
        </div>
    </div>