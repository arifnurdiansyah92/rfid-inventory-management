<footer>
    <div class="container-fluid footer-list">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-8">
                    <ul>
                        <h4>Tujuan Wisata</h4>
                        <li><a href="{{ route('main.pariwisata','curug-bandung') }}">Curug Bandung</a></li>
                        <li><a href="{{ route('main.pariwisata','pantai-tanjung-baru') }}">Pantai Tanjung Baru</a></li>
                        <li><a href="{{ route('main.pariwisata','danau-cipuler') }}">Danau Cipule</a></li>
                        <li><a href="{{ route('main.pariwisata','candi-jiwa') }}">Candi Jiwa</a></li>
                        <li><a href="{{ route('main.pariwisata','pantai-sedari') }}">Pantai Sedari</a></li>
                        <li><a href="{{ route('main.pariwisata','bukit-kembar') }}">Bukit Kembar</a></li>
                        <li><a href="{{ route('main.pariwisata','pantai-samudera-baru') }}">Pantai Samudera Baru</a></li>
                        <li><a href="{{ route('main.pariwisata','green-canyon') }}">Green Canyon</a></li>
                        <li><a href="{{ route('main.pariwisata','taruma-leisure-waterpark') }}">Taruma Leisure Waterpark</a></li>
                    </ul>

                    <ul>
                        <h4>Interes</h4>
                        <li><a href="#">Petualangan</a></li>
                        <li><a href="#">Wisata Keluarga</a></li>
                        <li><a href="#">Festival & events</a></li>
                        <li><a href="#">Makanan & Minuman</a></li>
                        <li><a href="#">Pantai</a></li>
                        <li><a href="#">Wisata Alam</a></li>
                        <li><a href="#">Diving & snorkeling</a></li>
                    </ul>

                    <ul>
                        <h4>Media Center</h4>
                        <li><a href="#">Foto</a></li>
                        <li><a href="#">Video</a></li>
                        <li><a href="#">Press</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 col-md-4 footer-form">
                    <h4>Berlangganan Tabloid Disbudpar</h4>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                    <form>
                        <input class="form-control" type="email" name="" maxlength="100">
                        <button type="submit" class="btn">Daftar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid footer-cp">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <a href="#" class="footer-logo">
                        <h2>Pariwisata<br><span>Karawang</span></h2>
                    </a>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="{{ route('main.kontak') }}">Contact us</a></li>
                        <span>&copy; 2016 Disparbud Kab. Karawang, Jawa Barat - Indonesia</span>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</footer>