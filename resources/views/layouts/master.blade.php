<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->

<head>
	<!--begin::Base Path (base relative path for assets of this page) -->
	<base href="../">
	<!--end::Base Path -->
	<meta charset="utf-8" />
	<title>@yield('title')</title>
	<meta name="description" content="Updates and statistics">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--begin::Fonts -->
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
				google: {
					"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
				},
				active: function() {
					sessionStorage.fonts = true;
				}
			});
	</script>

	<!--end::Fonts -->

	<!--begin::Page Vendors Styles(used by this page) -->
	<link href="{{ asset('/assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
		type="text/css" />
	<!-- <link href="./assets/vendors/custom/fullcalendar/fullcalendar.bundle.css" rel="stylesheet" type="text/css" /> -->

	<!--end::Page Vendors Styles -->

	<!--begin:: Global Mandatory Vendors -->
	<link href="{{ asset('/assets/vendors/general/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet"
		type="text/css" />

	<!--end:: Global Mandatory Vendors -->

	<!--begin:: Global Optional Vendors -->
	<link href="{{ asset('/assets/vendors/general/tether/dist/css/tether.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}"
		rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css') }}"
		rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-timepicker/css/bootstrap-timepicker.css') }}"
		rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css') }}"
		rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-select/dist/css/bootstrap-select.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css') }}"
		rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/select2/dist/css/select2.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/nouislider/distribute/nouislider.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/owl.carousel/dist/assets/owl.carousel.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/owl.carousel/dist/assets/owl.theme.default.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/dropzone/dist/dropzone.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/summernote/dist/summernote.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/bootstrap-markdown/css/bootstrap-markdown.min.css') }}"
		rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/animate.css/animate.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/toastr/build/toastr.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/morris.js/morris.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/general/sweetalert2/dist/sweetalert2.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/socicon/css/socicon.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/custom/vendors/line-awesome/css/line-awesome.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/custom/vendors/flaticon/flaticon.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/vendors/custom/vendors/flaticon2/flaticon.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/vendors/general/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ asset('/assets/css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/css/smart_wizard_theme_arrows.css') }}" rel="stylesheet" type="text/css" />
	<!--end:: Global Optional Vendors -->

	<!--begin::Global Theme Styles(used by all pages) -->
	<link href="{{ asset('/assets/css/demo1/style.bundle.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/css/demo1/style.custom.css') }}" rel="stylesheet" type="text/css" />

	<!--end::Global Theme Styles -->

	<!--begin::Layout Skins(used by all pages) -->
	<link href="{{ asset('/assets/css/demo1/skins/header/base/light-g.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/css/demo1/skins/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/css/demo1/skins/brand/green.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('/assets/css/demo1/skins/aside/green.css') }}" rel="stylesheet" type="text/css" />

	<!--end::Layout Skins -->
	<link rel="apple-touch-icon-precomposed" sizes="57x57"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-57x57.png') }}" />
	<link rel="apple-touch-icon-precomposed" sizes="114x114"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-114x114.png') }}" />
	<link rel="apple-touch-icon-precomposed" sizes="72x72"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-72x72.png') }}" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-144x144.png') }}" />
	<link rel="apple-touch-icon-precomposed" sizes="60x60"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-60x60.png') }}" />
	<link rel="apple-touch-icon-precomposed" sizes="120x120"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-120x120.png') }}" />
	<link rel="apple-touch-icon-precomposed" sizes="76x76"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-76x76.png') }}" />
	<link rel="apple-touch-icon-precomposed" sizes="152x152"
		href="{{ asset('/assets/img/favicon/apple-touch-icon-152x152.png') }}" />
	<link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon/favicon-196x196.png') }}" sizes="196x196" />
	<link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon/favicon-96x96.png') }}" sizes="96x96" />
	<link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon/favicon-32x32.png') }}" sizes="32x32" />
	<link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon/favicon-16x16.png') }}" sizes="16x16" />
	<link rel="icon" type="image/png" href="{{ asset('/assets/img/favicon/favicon-128.png') }}" sizes="128x128" />
	<meta name="application-name" content="&nbsp;" />
	<meta name="msapplication-TileColor" content="#FFFFFF" />
	<meta name="msapplication-TileImage" content="mstile-144x144.png" />
	<meta name="msapplication-square70x70logo" content="mstile-70x70.png" />
	<meta name="msapplication-square150x150logo" content="mstile-150x150.png" />
	<meta name="msapplication-wide310x150logo" content="mstile-310x150.png" />
	<meta name="msapplication-square310x310logo" content="mstile-310x310.png" />

	<style>
		.kt-aside {
			background-color: #00ab4e !important;
		}

		#kt_header {
			background-color: #00ab4e !important;
		}

		.kt-aside-menu .kt-menu__nav>.kt-menu__item>.kt-menu__link .kt-menu__link-icon i {
			color: #00ab4e !important;
			font-size: 1.6rem;
		}

		.kt-aside-menu {
			background-color: #f1f1f1 !important;
		}

		#kt_aside_menu_wrapper {
			background-color: #f1f1f1 !important;
		}

		.kt-aside-menu .kt-menu__nav>.kt-menu__item.kt-menu__item--active>.kt-menu__heading,
		.kt-aside-menu .kt-menu__nav>.kt-menu__item.kt-menu__item--active>.kt-menu__link {
			background-color: #f1f1f1 !important;
		}

		.kt-aside-menu .kt-menu__nav>.kt-menu__item.kt-menu__item--active>.kt-menu__heading,
		.kt-aside-menu .kt-menu__nav>.kt-menu__item.kt-menu__item--active>.kt-menu__link {
			background-color: #f1f1f1 !important;
		}

		.kt-aside-menu .kt-menu__nav>.kt-menu__item.kt-menu__item--active>.kt-menu__heading .kt-menu__link-text,
		.kt-aside-menu .kt-menu__nav>.kt-menu__item.kt-menu__item--active>.kt-menu__link .kt-menu__link-text {
			color: #00ab4e !important;
		}

		#content {
			background-color: #ffffff;
		}
	</style>
	@yield('styles')
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body
	class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

	<!-- begin:: Page -->

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
		<div class="kt-header-mobile__logo">
			<a href="{{ url('/') }}">
				<img alt="Logo" src="./assets/media/gojek-logo-white.png" class="img-responsive" />
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toggler kt-header-mobile__toggler--left"
				id="kt_aside_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
					class="flaticon-more"></i></button>
		</div>
	</div>

	<!-- end:: Header Mobile -->
	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

			<!-- begin:: Aside -->
			<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
			<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
				id="kt_aside">
				@include('layouts.partials.sidebar')
			</div>

			<!-- end:: Aside -->
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

				<!-- begin:: Header -->
				<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

					<!-- begin:: Header Menu -->
					<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i
							class="la la-close"></i></button>
					<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
						<div id="kt_header_menu"
							class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
							<ul class="kt-menu__nav ">

							</ul>
						</div>
					</div>

					<!-- end:: Header Menu -->

					<!-- begin:: Header Topbar -->
					<div class="kt-header__topbar">

						<!--begin: Search -->

						<!-- <div class="kt-quick-search kt-quick-search--inline" id="kt_quick_search_inline">
									<form method="get" class="kt-quick-search__form">
										<div class="input-group">
											<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
											<input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
										</div>
									</form>
									<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
									</div>
								</div> -->

						<!--end: Search -->
						<!--begin: Quick Actions -->
						<!--end: Quick Actions -->
					</div>

					<!-- end:: Header Topbar -->
				</div>
				<div id="content" class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
					@yield('content')
				</div>

				{{-- <!-- begin:: Footer -->
				<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
					<div class="kt-footer__copyright">
						2019 &copy;&nbsp;<a href="#" class="kt-link">RFID Inventory Management</a>
					</div>
				</div>

				<!-- end:: Footer --> --}}
			</div>
		</div>
	</div>

	<!-- end:: Page -->

	<!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>

	<!-- end::Scrolltop -->



	<!-- begin::Global Config(global config for global JS sciprts) -->
	<script>
		var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#5d78ff",
						"dark": "#282a3c",
						"light": "#ffffff",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#36a3f7",
						"warning": "#ffb822",
						"danger": "#fd3995"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
	</script>

	<!-- end::Global Config -->

	<!--begin:: Global Mandatory Vendors -->
	<script src="{{ asset('/assets/vendors/general/jquery/dist/jquery.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/popper.js/dist/umd/popper.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/js-cookie/src/js.cookie.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/moment/min/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/tooltip.js/dist/umd/tooltip.min.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/perfect-scrollbar/dist/perfect-scrollbar.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/sticky-js/dist/sticky.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/wnumb/wNumb.js') }}" type="text/javascript"></script>

	<!--end:: Global Mandatory Vendors -->

	<!--begin:: Global Optional Vendors -->
	<script src="{{ asset('/assets/vendors/general/jquery-form/dist/jquery.form.min.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/block-ui/jquery.blockUI.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/js/vendors/bootstrap-datepicker.init.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/js/vendors/bootstrap-timepicker.init.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-daterangepicker/daterangepicker.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-maxlength/src/bootstrap-maxlength.js') }}"
		type="text/javascript"></script>
	<script
		src="{{ asset('/assets/vendors/custom/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-select/dist/js/bootstrap-select.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-switch/dist/js/bootstrap-switch.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/js/vendors/bootstrap-switch.init.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/select2/dist/js/select2.full.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/ion-rangeslider/js/ion.rangeSlider.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/typeahead.js/dist/typeahead.bundle.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/handlebars/dist/handlebars.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/inputmask/dist/jquery.inputmask.bundle.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/inputmask/dist/inputmask/inputmask.date.extensions.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/nouislider/distribute/nouislider.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/owl.carousel/dist/owl.carousel.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/autosize/dist/autosize.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/clipboard/dist/clipboard.min.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/dropzone/dist/dropzone.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/summernote/dist/summernote.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/markdown/lib/markdown.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-markdown/js/bootstrap-markdown.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/js/vendors/bootstrap-markdown.init.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/bootstrap-notify/bootstrap-notify.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/js/vendors/bootstrap-notify.init.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/jquery-validation/dist/jquery.validate.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/jquery-validation/dist/additional-methods.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/js/vendors/jquery-validation.init.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/toastr/build/toastr.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/raphael/raphael.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/morris.js/morris.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/chart.js/dist/Chart.bundle.js') }}" type="text/javascript"></script>
	<script
		src="{{ asset('/assets/vendors/custom/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/vendors/jquery-idletimer/idle-timer.min.js') }}"
		type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/waypoints/lib/jquery.waypoints.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/counterup/jquery.counterup.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/es6-promise-polyfill/promise.min.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/sweetalert2/dist/sweetalert2.min.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/custom/js/vendors/sweetalert2.init.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/jquery.repeater/src/lib.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/general/jquery.repeater/src/jquery.input.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/jquery.repeater/src/repeater.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/vendors/general/dompurify/dist/purify.js') }}" type="text/javascript"></script>

	<!--end:: Global Optional Vendors -->

	<!--begin::Global Theme Bundle(used by all pages) -->
	<script src="{{ asset('/assets/js/demo1/scripts.bundle.js') }}" type="text/javascript"></script>

	<!--end::Global Theme Bundle -->

	<!--begin::Page Vendors(used by this page) -->
	<script src="{{ asset('/assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js') }}" type="text/javascript">
	</script>
	<script src="{{ asset('/assets/js/summernote-pagebreak.js') }}"></script>
	<!--end::Page Vendors -->

	<!--begin::Page Scripts(used by this page) -->
	<script>
		$(document).ready(function() {
			$('.summernote').summernote({
				dialogsInBody: true,
				height: '300px',
				toolbar:[
					['pagebreak',['pagebreak']], // The Button
					['style',['style']],
					['font',['bold','italic','underline','clear']],
					['fontname',['fontname']],
					['color',['color']],
					['para',['ul','ol','paragraph']],
					['height',['height']],
					['table',['table']],
					['insert',['picture','link','hr']],
					['view',['fullscreen','codeview']],
					['help',['help']]
				],
			});
		});
		@if($errors->any())
			toastr.error("{{ $errors->first() }}");
		@elseif(session()->has('success'))
			toastr.success('{{ session()->get("success") }}');
		@endif
	</script>
	<script type="text/javascript" src="{{ asset('/assets/js/jquery.smartWizard.js') }}"></script>
	@yield('scripts')
	<!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>