##Tutorial Install DISPARBUD
1.  Clone project ini di `https://gitlab.com/aegisdev/disparbud.git`
2. Setelah berhasil clone, konfigurasi .env, bisa copy dari .env.example dan sesuaikan confignya.
3. Silahkan install package composer dengan `composer install` 
4. Buka terminal di root folder project dan ketik `php artisan migrate` untuk inisialisasi database.

##Author Note
Jika konfigurasi tidak update coba hapus cache dengan cmd berikut :
`php artisan config:clear` dan `php artisan cache:clear`
Kontak : an@aegis.co.id