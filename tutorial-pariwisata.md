##Tutorial Menambah Page Pariwisata
1. Membuat view file di `{root folder}\resources\views\mainpage\pariwisata`.
2. Bisa akses dengan `{url}/pariwisata/nama-file`. contoh nama file adalah `curug-bandung.blade.php` maka bisa akses dengan link `{url}/pariwisata/curug-bandung`.
3. Generate link dengan `{{ route('main.pariwisata','nama-file') }}` contoh : `{{ route('main.pariwisata','curug-bandung) }}`