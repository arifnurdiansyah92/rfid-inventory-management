<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('mainpage.login');
});
Route::post('/login',function(Request $req){
    if($req->username == "admin" && $req->password == "admin"){
        return redirect()->route('admin.dashboard');
    }else{
        return redirect()->back()->with('errors','Username / Password Salah!');
    }
})->name('auth.login');

Route::prefix('admin')->group(function(){
    Route::get('/',function(){
        return view('mainpage.admin');
    })->name("admin.dashboard");
    Route::get('/inventory',function(){
        return view('inventory.index');
    })->name('inventory.index');
    Route::get('/checkin',function(){
        return view('checkin.index');
    })->name('checkin.index');
});